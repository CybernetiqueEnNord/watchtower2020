/**
 *
 */

package com.booleanworks.ocelloid.watchtower.aruco;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.TreeMap;
import javax.imageio.ImageIO;

/**
 * @author m023271
 *
 */
public abstract class ArucoDictionary
{


  public static ArucoDictionary getArucoDictionay4()
  {
    return new AruroDictionary4();
  }

  public static ArucoDictionary getArucoDictionay5()
  {
    return new AruroDictionary5();
  }

  public static ArucoDictionary getArucoDictionay6()
  {
    return new AruroDictionary6();
  }

  public static ArucoDictionary getArucoDictionay7()
  {
    return new AruroDictionary7();
  }

  public static ArucoDictionary getArucoDictionayClassic()
  {
    return new AruroDictionaryClassic();
  }

  public abstract int getKnownArucoCount();

  public abstract int getMatrixHeight();

  public abstract int getMatrixWidth();

  public BufferedImage getRgbIntImageFromIndex(int id, int pixelpercell, boolean withmargins)
  {


    if (withmargins)
    {
      BufferedImage result = new BufferedImage(this.getMatrixWidth() + 2,
          this.getMatrixHeight() + 2, BufferedImage.TYPE_INT_RGB);



      // all black
      for (int x = 0; x < (this.getMatrixWidth() + 1); x++)
      {
        for (int y = 0; y < (this.getMatrixHeight() + 1); y++)
        {
          result.setRGB(x, y, 0x00000000);
        }
      }


      // Letting margins and filling inside.
      for (int x = 0; x < this.getMatrixWidth(); x++)
      {
        for (int y = 0; y < this.getMatrixHeight(); y++)
        {
          result.setRGB(y + 1, x + 1,
              this.getValueAtPixel(id, x, y) != 0 ? 0x00ffffff : 0x00000000);
        }
      }

      BufferedImage resultScaled = new BufferedImage((this.getMatrixWidth() + 2) * pixelpercell,
          (this.getMatrixHeight() + 2) * pixelpercell, BufferedImage.TYPE_INT_RGB);
      Graphics2D g = resultScaled.createGraphics();
      // g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
      // RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      g.drawImage(result, 0, 0, (this.getMatrixWidth() + 2) * pixelpercell,
          (this.getMatrixHeight() + 2) * pixelpercell, 0, 0, (this.getMatrixWidth() + 2),
          (this.getMatrixHeight() + 2), null);
      g.dispose();

      try
      {
        ImageIO.write(resultScaled, "png", new File("debug_border_" + id + ".png"));
      }
      catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      return resultScaled;
    }
    else
    {
      BufferedImage result = new BufferedImage(this.getMatrixWidth(), this.getMatrixHeight(),
          BufferedImage.TYPE_INT_RGB);


      // Letting margins and filling inside.
      for (int x = 0; x < this.getMatrixWidth(); x++)
      {
        for (int y = 0; y < this.getMatrixHeight(); y++)
        {
          result.setRGB(y, x, this.getValueAtPixel(id, x, y) != 0 ? 0x00ffffff : 0x00000000);
        }
      }


      BufferedImage resultScaled = new BufferedImage(this.getMatrixWidth() * pixelpercell,
          this.getMatrixHeight() * pixelpercell, BufferedImage.TYPE_INT_RGB);
      Graphics2D g = resultScaled.createGraphics();
      // g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
      // RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      g.drawImage(result, 0, 0, this.getMatrixWidth() * pixelpercell,
          this.getMatrixHeight() * pixelpercell, 0, 0, this.getMatrixWidth(),
          this.getMatrixHeight(), null);
      g.dispose();


      try
      {
        ImageIO.write(resultScaled, "png", new File("debug_" + id + ".png"));
      }
      catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }



      return resultScaled;
    }

  }

  public TreeMap<Integer, BufferedImage> getRgbIntImageInRange(int start, int end, int pixelpercell,
      boolean withmargins)
  {

    TreeMap<Integer, BufferedImage> result = new TreeMap<>();

    start = start < 0 ? 0 : start;
    start = start > (this.getKnownArucoCount() - 1) ? (this.getKnownArucoCount() - 1) : start;

    end = end < 0 ? 0 : end;
    end = end > (this.getKnownArucoCount() - 1) ? (this.getKnownArucoCount() - 1) : end;

    if (start > end)
    {
      int tmp = end;
      end = start;
      start = tmp;
    }

    for (int id = start; id < end; id++)
    {
      result.put(id, this.getRgbIntImageFromIndex(id, pixelpercell, withmargins));
    }

    return result;

  }

  public abstract int getValueAtPixel(int id, int x, int y);

}
