/**
 *
 */

package com.booleanworks.ocelloid.watchtower.aruco;

import java.util.List;
import com.booleanworks.ocelloid.watchtower.CapturedImageOccurence;
import com.booleanworks.ocelloid.watchtower.messages.FiducialDetectionOccurence;
import boofcv.alg.distort.LensDistortionNarrowFOV;
import boofcv.struct.calib.CameraPinholeBrown;

/**
 * @author vortigern
 *
 */
public abstract class AbstractArucoDetectionContext {

  protected final CameraPinholeBrown cameraPinholeBrown;
  protected final LensDistortionNarrowFOV lensDistortionNarrowFOV;
  protected final double arucoPhysicalSize;

  /**
   * @param cameraPinholeBrown
   * @param lensDistortionNarrowFOV
   * @param webcam
   */
  public AbstractArucoDetectionContext(CameraPinholeBrown cameraPinholeBrown,
      LensDistortionNarrowFOV lensDistortionNarrowFOV, double arucoPhysicalSize) {
    super();
    this.cameraPinholeBrown = cameraPinholeBrown;
    this.lensDistortionNarrowFOV = lensDistortionNarrowFOV;
    this.arucoPhysicalSize = arucoPhysicalSize;
  }

  /**
   * @return the cameraPinholeBrown
   */
  public CameraPinholeBrown getCameraPinholeBrown() {
    return this.cameraPinholeBrown;
  }

  /**
   * @return the lensDistortionNarrowFOV
   */
  public LensDistortionNarrowFOV getLensDistortionNarrowFOV() {
    return this.lensDistortionNarrowFOV;
  }



  /**
   * @return the arucoPhysicalSize
   */
  public double getArucoPhysicalSize() {
    return this.arucoPhysicalSize;
  }

  public abstract List<FiducialDetectionOccurence> detect(CapturedImageOccurence capture);



}
