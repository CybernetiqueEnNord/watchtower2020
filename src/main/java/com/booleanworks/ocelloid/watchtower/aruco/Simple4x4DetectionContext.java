/**
 *
 */

package com.booleanworks.ocelloid.watchtower.aruco;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;
import com.booleanworks.ocelloid.watchtower.CapturedImageOccurence;
import com.booleanworks.ocelloid.watchtower.Cfg;
import com.booleanworks.ocelloid.watchtower.messages.FiducialDetectionOccurence;
import boofcv.abst.fiducial.FiducialStability;
import boofcv.abst.fiducial.SquareImage_to_FiducialDetector;
import boofcv.alg.distort.LensDistortionNarrowFOV;
import boofcv.factory.fiducial.ConfigFiducialImage;
import boofcv.factory.fiducial.FactoryFiducial;
import boofcv.factory.filter.binary.ConfigThreshold;
import boofcv.factory.filter.binary.ThresholdType;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.struct.ConfigLength;
import boofcv.struct.calib.CameraPinholeBrown;
import boofcv.struct.image.GrayF32;
import georegression.struct.point.Point2D_F64;
import georegression.struct.se.Se3_F64;
import georegression.struct.shapes.Polygon2D_F64;

/**
 * @author vortigern
 *
 */
public class Simple4x4DetectionContext extends AbstractArucoDetectionContext
{

  private static final double MAX_ACCEPTABLE_POSE_UNCERTAINITY = 0.03;
  private static final double MAX_ACCEPTABLE_LOCATION_UNCERTAINITY = 11.0;
  private static final double STABILITY_CMPT_DISTURBANCE =
      Cfg.getDouble("c1.services.aruco.detector.stability.disturbance", 0.25);
  protected static final double[] minimumBlackBorderFraction =
  {0.78, 0.81, 0.83, 0.85, 0.87, 0.92};
  protected static final double[] borderWidthFraction =
  {0.08, 0.10, 0.11, 0.12, 0.15, 0.15};
  protected static final double[] maxErrorFraction =
  {0.31, 0.25, 0.24, 0.21, 0.17, 0.14};
  protected static final double[] minimumContour =
  {4, 5, 6, 7, 9, 11};
  protected static final int[] localThreshold =
  {35, 24, 21, 21, 21, 22};



  protected static final int DETECTORS_START_IDX =
      Cfg.getInt("c1.services.aruco.detector.startIdx", 0);

  protected static final int DETECTORS_STOP_IDX =
      Cfg.getInt("c1.services.aruco.detector.stopIdx", 3);

  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(Simple4x4DetectionContext.class.getName());


  protected TreeMap<Integer, SquareImage_to_FiducialDetector<GrayF32>> detectors =
      new TreeMap<Integer, SquareImage_to_FiducialDetector<GrayF32>>();

  /**
   * @param cameraPinholeBrown
   * @param lensDistortionNarrowFOV
   * @param webcam
   */
  public Simple4x4DetectionContext(CameraPinholeBrown cameraPinholeBrown,
      LensDistortionNarrowFOV lensDistortionNarrowFOV, double arucoPhysicalSize)
  {
    super(cameraPinholeBrown, lensDistortionNarrowFOV, arucoPhysicalSize);

    LOG.finer("[Simple4x4DetectionContext] creation...");

    // ArucoDictionary.getArucoDictionay4().getRgbIntImageInRange(0, 12, 60, true);

    TreeMap<Integer, BufferedImage> arucoPatterns =
        ArucoDictionary.getArucoDictionay4().getRgbIntImageInRange(0, 80,
            Cfg.getInt("c1.services.aruco.detector.pixelpercell", 4), false);


    for (int idx = DETECTORS_START_IDX; idx < DETECTORS_STOP_IDX; idx++)
    {
      ConfigFiducialImage configFiducialImage = new ConfigFiducialImage();
      configFiducialImage.minimumBlackBorderFraction =
          minimumBlackBorderFraction[idx % minimumBlackBorderFraction.length];
      configFiducialImage.borderWidthFraction =
          borderWidthFraction[idx % minimumBlackBorderFraction.length];
      configFiducialImage.maxErrorFraction =
          maxErrorFraction[idx % minimumBlackBorderFraction.length];
      // configFiducialImage.getSquareDetector().detector.minimumContour =
      // ConfigLength.fixed(minimumContour[idx % minimumBlackBorderFraction.length]);
      configFiducialImage.getSquareDetector().detector.minimumContour = ConfigLength.relative(0.15,
          (int) minimumContour[idx % minimumBlackBorderFraction.length]);

      SquareImage_to_FiducialDetector<GrayF32> newDetector = FactoryFiducial
          .squareImage(configFiducialImage, ConfigThreshold.local(ThresholdType.LOCAL_MEAN,
              localThreshold[idx % localThreshold.length]), GrayF32.class);

      // SquareImage_to_FiducialDetector<GrayF32> newDetector = FactoryFiducial.squareImage(
      // configFiducialImage, ConfigThreshold.global(ThresholdType.GLOBAL_OTSU), GrayF32.class);

      newDetector.setLensDistortion(this.getLensDistortionNarrowFOV(),
          this.getCameraPinholeBrown().getWidth(), this.getCameraPinholeBrown().getHeight());



      LOG.finer("newDetector.getAlgorithm().getTargets().size()="
          + newDetector.getAlgorithm().getTargets().size());
      if (newDetector.getAlgorithm().getTargets().isEmpty())
      {
        for (Map.Entry<Integer, BufferedImage> patternEntry : arucoPatterns.entrySet())
        {
          newDetector.addPatternImage(
              ConvertBufferedImage.convertFrom(patternEntry.getValue(), (GrayF32) null), 90.0,
              this.getArucoPhysicalSize());
        }
      }


      this.detectors.put(idx, newDetector);



    }

  }

  @Override
  public List<FiducialDetectionOccurence> detect(CapturedImageOccurence capture)
  {

    CopyOnWriteArrayList<FiducialDetectionOccurence> result1 =
        new CopyOnWriteArrayList<FiducialDetectionOccurence>();


    this.detectors.values().parallelStream().forEach(cDetector -> {

      GrayF32 gray = ConvertBufferedImage.convertFrom(capture.getImage(), (GrayF32) null);


      cDetector.detect(gray);

      for (int fidct = 0; fidct < cDetector.totalFound(); fidct++)
      {



        Polygon2D_F64 bounds = new Polygon2D_F64();
        Point2D_F64 center = new Point2D_F64();
        Se3_F64 targetToSensor = new Se3_F64();
        Point2D_F64 locationPixel = new Point2D_F64();

        cDetector.getCenter(fidct, center);
        cDetector.getBounds(fidct, bounds);

        if (cDetector.hasID())
        {
          LOG.finer("Target ID = " + cDetector.getId(fidct));
        }
        if (cDetector.hasMessage())
        {
          LOG.finer("Message   = " + cDetector.getMessage(fidct));
        }
        LOG.finer("2D Image Location = " + locationPixel);



        if (cDetector.is3D())
        {
          cDetector.getFiducialToCamera(fidct, targetToSensor);
          // LOG.finer("3D Location:");
          LOG.finer("targetToSensor = " + targetToSensor);

        }

        FiducialDetectionOccurence fidOcc = new FiducialDetectionOccurence(capture.getWebcamName(),
            capture.getTimestamp(), capture.hashCode(), "" + cDetector.getId(fidct), bounds, center,
            targetToSensor, locationPixel);

        FiducialStability stability = new FiducialStability();
        boolean computedStability =
            cDetector.computeStability(fidct, STABILITY_CMPT_DISTURBANCE, stability);
        fidOcc.setStability(stability);

        LOG.finer("Stability.location: " + stability.location);
        LOG.finer("Stability.orientation: " + stability.orientation);



        if (computedStability && stability.location < MAX_ACCEPTABLE_LOCATION_UNCERTAINITY
            && stability.orientation < MAX_ACCEPTABLE_POSE_UNCERTAINITY)
        {
          result1.add(fidOcc);
        }
        else
        {
          LOG.finer("Discarded instable Aruco detection. " + stability.location + " , "
              + stability.orientation);
        }

      }

    });



    ArrayList<FiducialDetectionOccurence> result2 = new ArrayList<FiducialDetectionOccurence>();


    TreeSet<String> foundIds = new TreeSet<>();
    result1.stream().forEach((f1) -> foundIds.add(f1.getDataAsString()));

    foundIds.stream()
        .forEach((id) -> result1.stream().filter((f1) -> f1.getDataAsString().equals(id))
            .findFirst().ifPresent((f2) -> result2.add(f2)));

    // ArrayList<FiducialDetectionOccurence> result3 = new ArrayList<FiducialDetectionOccurence>();

    // result2.removeIf((f1) -> f1.getStability().location * 0.7 > f1.getTargetToSensor().getZ()
    // / this.getArucoPhysicalSize());
    //
    // result2.removeIf((f1) -> f1.getStability().orientation * 100.0 >
    // f1.getTargetToSensor().getZ()
    // / this.getArucoPhysicalSize());


    return result1;
  }

}
