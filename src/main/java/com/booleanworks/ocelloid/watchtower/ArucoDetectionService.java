/**
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Logger;
import com.booleanworks.ocelloid.watchtower.aruco.AbstractArucoDetectionContext;
import com.booleanworks.ocelloid.watchtower.aruco.Simple4x4DetectionContext;
import com.booleanworks.ocelloid.watchtower.messages.FiducialDetectionOccurence;
import boofcv.gui.feature.VisualizeFeatures;
import boofcv.gui.feature.VisualizeShapes;
import boofcv.gui.fiducial.VisualizeFiducial;
import boofcv.io.image.UtilImageIO;

/**
 * @author vortigern
 *
 */
public class ArucoDetectionService extends Thread implements BaseService
{

  /**
   *
   */
  public ArucoDetectionService()
  {
    super();
    this.serviceStatus = EnumServiceStatus.UNKNOWN;
  }

  public static int CYCLE_DELAY = Cfg.getInt("c1.services.aruco.cycle", 1200);

  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(ArucoDetectionService.class.getName());

  private CopyOnWriteArrayList<Integer> processedImageOccurences = new CopyOnWriteArrayList<Integer>();

  private CopyOnWriteArrayList<FiducialDetectionOccurence> fiducialOccurences =
      new CopyOnWriteArrayList<FiducialDetectionOccurence>();

  /**
   * @return the fiducialOccurences
   */
  public CopyOnWriteArrayList<FiducialDetectionOccurence> getFiducialOccurences()
  {
    return this.fiducialOccurences;
  }

  /** By cam detector */
  private HashMap<String, AbstractArucoDetectionContext> detectors = new HashMap<String, AbstractArucoDetectionContext>();

  /** Our mighty executor :-) . */
  private ScheduledThreadPoolExecutor executor =
      new ScheduledThreadPoolExecutor(Math.max(2, Runtime.getRuntime().availableProcessors() - 1));


  private EnumServiceStatus serviceStatus;

  @Override
  public boolean bootstrap()
  {


    LOG.info("Starting ArucoDetectionService...");
    if (!this.isAlive())
    {
      this.setPriority(3);
      this.start();
    }

    return true;
  }



  /**
   * return the serviceStatus.
   *
   * @return the serviceStatus.
   */
  @Override
  public EnumServiceStatus getServiceStatus()
  {

    if (this.serviceStatus == null)
    {
      this.serviceStatus = EnumServiceStatus.UNKNOWN;
    }

    return this.serviceStatus;
  }


  @Override
  public boolean init()
  {
    if (this.getServiceStatus() != EnumServiceStatus.UNKNOWN)
    {
      return false;
    }

    this.serviceStatus = EnumServiceStatus.INITIALIZING;
    this.setName(ArucoDetectionService.class.getName());



    this.serviceStatus = EnumServiceStatus.READY;
    return true;
  }

  @Override
  public void run()
  {

    this.serviceStatus = EnumServiceStatus.RUNNING;

    while (this.getServiceStatus() != EnumServiceStatus.STOPPING && this.getServiceStatus() != EnumServiceStatus.FULL_FAILURE)
    {

      try
      {

        // measuring time
        long t0 = System.currentTimeMillis();



        // Getting all knwon images at this time
        ConcurrentSkipListSet<CapturedImageOccurence> images = new ConcurrentSkipListSet<CapturedImageOccurence>();
        // images.addAll(WatchTowerService.getInstance().getWebCamService().getImages());
        images.addAll(WatchTowerService.getInstance().getOpenCvCamService().getImages());


        if (Cfg.getBoolean("c1.services.aruco.onlycentercamera", false))
        {
          images.removeIf(i -> !i.getWebcamName().matches(Game2020StatusService.CENTER_CAMERA.getNameRecognitionPattern()));
        }


        ArrayList<CapturedImageOccurence> retainedImages = new ArrayList<CapturedImageOccurence>();

        HashMap<String, Integer> retainedImagesPerCam = new HashMap<String, Integer>();
        images.stream().filter((ci) -> !retainedImagesPerCam.containsKey(ci.getWebcamName()))
            .forEach((ci) -> retainedImagesPerCam.put(ci.getWebcamName(), 0));


        LOG.finer("[ArucoDetectionService]  images.size() = " + images.size());
        for (CapturedImageOccurence cImage : images)
        {

          // Ignoring already known images
          if (!this.processedImageOccurences.contains(cImage.hashCode()))
          {
            this.processedImageOccurences.add(cImage.hashCode());
            int alreadyRetainedImagesForThisCam = retainedImagesPerCam.get(cImage.getWebcamName());
            if (alreadyRetainedImagesForThisCam < 1)
            {
              LOG.finer("[ArucoDetectionService]  Retaining image from this cam.");
              retainedImagesPerCam.put(cImage.getWebcamName(), alreadyRetainedImagesForThisCam++);
              retainedImages.add(cImage);
            }
            else
            {
              LOG.finer("[ArucoDetectionService]  Already enought images from this cam.");
            }

            LOG.finer("[ArucoDetectionService]  Unknown image added.");

          }
          else
          {
            LOG.finer("[ArucoDetectionService]  Known image discarded.");
          }

        }

        LOG.finer("[ArucoDetectionService]  Retained images before processing retainedImages.size() = " + retainedImages.size());



        // If executor is busy or have a queue, give up !
        if (this.executor.getActiveCount() > 0 && this.executor.getQueue().size() > 0)
        {
          LOG.finer("[ArucoDetectionService]  executor is busy. Bypassing recognition.");
          continue;
        }

        // Processing images
        retainedImages.stream().forEach(cImage -> {


          Runnable newTask = () -> {

            LOG.finer("[ArucoDetectionService]  New detection task !");

            long t2 = System.currentTimeMillis();

            if (!this.detectors.containsKey(cImage.getWebcamName()))
            {

              LOG.finer("[ArucoDetectionService]  Detection task check point 005.");

              try
              {
                EnumWebCamType webcamType = EnumWebCamType.detect(cImage.getWebcamName());
                // EnumWebCamType webcamType = EnumWebCamType.DEFAULT;
                LOG.finer("[ArucoDetectionService]  Detection task check point 006.");

                Simple4x4DetectionContext newDetector =
                    new Simple4x4DetectionContext(webcamType.getCameraPinholeBrown(), webcamType.getLensDistortion(), 80.0);
                LOG.finer("[ArucoDetectionService]  Detection task check point 007.");
                this.detectors.put(cImage.getWebcamName(), newDetector);
              }
              catch (Exception e)
              {
                LOG.warning("[ArucoDetectionService]  Failed to created detector: " + e.getMessage());
                e.printStackTrace();
              }



              LOG.finer("[ArucoDetectionService] Added detector, this.detectors.size() = " + this.detectors.size());
            }
            else
            {
              LOG.finer("[ArucoDetectionService] No new detector needed, this.detectors.size() = " + this.detectors.size());
            }

            LOG.finer("[ArucoDetectionService]  Detection task check point 010.");

            // Making detection using dedicated per webcam detector
            List<FiducialDetectionOccurence> NewArucos = this.detectors.get(cImage.getWebcamName()).detect(cImage);
            LOG.finer("[ArucoDetectionService] NewArucos.size():" + NewArucos.size());

            LOG.finer("[ArucoDetectionService]  Detection task check point 020.");

            if (Cfg.getBoolean("c1.services.root.debug5", false))
            {

              try
              {
                ColorModel cm = cImage.getImage().getColorModel();
                boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
                WritableRaster raster = cImage.getImage().copyData(null);
                BufferedImage debugImage = new BufferedImage(cm, raster, isAlphaPremultiplied, null);

                Graphics2D g2 = debugImage.createGraphics();
                g2.setColor(new Color(50, 50, 255));
                g2.setStroke(new BasicStroke(10));

                for (FiducialDetectionOccurence fid : NewArucos)
                {
                  VisualizeFeatures.drawPoint(g2, (int) fid.getCenter().x, (int) fid.getCenter().y, new Color(255, 255, 50));
                  VisualizeShapes.drawPolygon(fid.getBounds(), true, 1.0, g2);
                  VisualizeFiducial.drawCube(fid.getTargetToSensor(),
                      this.detectors.get(cImage.getWebcamName()).getCameraPinholeBrown(), 40.0, 3, g2);
                  VisualizeFiducial.drawLabelCenter(fid.getTargetToSensor(),
                      this.detectors.get(cImage.getWebcamName()).getCameraPinholeBrown(),
                      fid.getDataAsString() + " " + fid.getStability().location, g2);


                }

                UtilImageIO.saveImage(debugImage,
                    "aruconew_" + cImage.getWebcamName().replaceAll("/", "") + "_" + cImage.getTimestamp() + ".png");
              }
              catch (Exception e)
              {
                LOG.warning("[ArucoDetectionService] Error on debugging step, " + e.getMessage());
                e.printStackTrace();
              }


            }


            // Adding results.
            this.fiducialOccurences.addAll(NewArucos);
            LOG.finer(
                "[ArucoDetectionService] After detection, this.fiducialOccurences.size() = " + this.fiducialOccurences.size());

            long t3 = System.currentTimeMillis();

            LOG.finer("[ArucoDetectionService]  Detection task check point 030.");

            long ArucoProcessingDuration = t3 - t2;

            LOG.finer("[ArucoDetectionService]  ArucoProcessingDuration = " + ArucoProcessingDuration);

            LOG.finer("[ArucoDetectionService]  Detection task check point 040.");

            if (ArucoProcessingDuration < CYCLE_DELAY)
            {
              LOG.finer("ArucoProcessingDuration=" + ArucoProcessingDuration + "ms");
            }
            else
            {
              LOG.warning("ArucoProcessingDuration=" + ArucoProcessingDuration + "ms");
              LOG.warning("[ArucoDetectionService] Added detector, this.detectors.size() = " + this.detectors.size());
              LOG.warning(
                  "[ArucoDetectionService]  Retained images before processing retainedImages.size() = " + retainedImages.size());
              LOG.warning("[ArucoDetectionService]  images.size() = " + images.size());
              LOG.warning(
                  "[ArucoDetectionService] After detection, this.fiducialOccurences.size() = " + this.fiducialOccurences.size());
            }

            LOG.finer("[ArucoDetectionService]  Detection task check point 050.");

          };

          // Parallel processing
          this.executor.execute(newTask);

          // simple run
          // newTask.run();

          LOG.finer("[ArucoDetectionService]  post Detection task check point 100.");

          cImage.incrementUsageCounter();
          this.processedImageOccurences.add(cImage.hashCode());



        });



        // Removing occurences that were removed by WebCamService
        this.processedImageOccurences.removeIf(imageId -> images.stream().noneMatch(cio -> cio.hashCode() == imageId));

        // Removing old fiducials
        this.fiducialOccurences.removeIf(fidOcc -> (System.currentTimeMillis() - fidOcc.getTimestamp()) > 3 * CYCLE_DELAY);

        long t1 = System.currentTimeMillis();



        long waitTime = CYCLE_DELAY - (t1 - t0);
        if (waitTime < 10)
        {
          LOG.severe("Warning, Aruco detection is too long !!! " + (t1 - t0) + "ms");
        }
        else
        {
          LOG.finer("Warning, Aruco detection duration = " + (t1 - t0) + "ms");
        }


        LOG.finer("[ArucoDetectionService] this.detectors.size() = " + this.detectors.size());
        LOG.finer("[ArucoDetectionService] this.fiducialOccurences.size() = " + this.fiducialOccurences.size());
        LOG.finer("[ArucoDetectionService] this.processedImageOccurences.size() = " + this.processedImageOccurences.size());

        Thread.sleep(Math.max(10, CYCLE_DELAY - (t1 - t0)));

        CYCLE_DELAY = Cfg.getInt("c1.services.aruco.cycle", 1200);

        this.setPriority(Cfg.getInt("c1.services.aruco.priority", 2));

      }
      catch (InterruptedException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }



    }

  }

  @Override
  public boolean tearDown()
  {
    this.serviceStatus = EnumServiceStatus.STOPPING;

    try
    {
      Thread.sleep(CYCLE_DELAY * 2);
    }
    catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }



    this.serviceStatus = EnumServiceStatus.STOPPED;

    return true;
  }


}
