/*
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.util.BitSet;
import java.util.stream.IntStream;

public class CappedBitSet extends BitSet
{

  /**
   *
   */
  private static final long serialVersionUID = -6105613705657926966L;

  protected int cappedSize = 0;



  /**
   *
   */
  public CappedBitSet()
  {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @param nbits
   */
  public CappedBitSet(int nbits)
  {
    super(nbits);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param nbits
   */
  public CappedBitSet(int nbits, int cappedSize)
  {
    super(nbits);
    this.setCappedSize(cappedSize);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void and(BitSet arg0)
  {
    // TODO Auto-generated method stub
    super.and(arg0);


  }

  @Override
  public void andNot(BitSet arg0)
  {
    // TODO Auto-generated method stub
    super.andNot(arg0);
  }

  @Override
  public int cardinality()
  {
    // TODO Auto-generated method stub
    return super.cardinality();
  }

  @Override
  public void clear()
  {
    // TODO Auto-generated method stub
    super.clear();
  }

  @Override
  public void clear(int arg0, int arg1)
  {
    // TODO Auto-generated method stub
    super.clear(arg0, arg1);
  }

  @Override
  public void clear(int bitIndex)
  {
    // TODO Auto-generated method stub
    super.clear(bitIndex);
  }

  @Override
  public Object clone()
  {
    // TODO Auto-generated method stub
    return super.clone();
  }

  @Override
  public boolean equals(Object arg0)
  {
    // TODO Auto-generated method stub
    return super.equals(arg0);
  }

  @Override
  public void flip(int arg0, int arg1)
  {
    // TODO Auto-generated method stub
    super.flip(arg0, arg1);
  }

  @Override
  public void flip(int bitIndex)
  {
    // TODO Auto-generated method stub
    super.flip(bitIndex);
  }

  @Override
  public BitSet get(int arg0, int arg1)
  {
    // TODO Auto-generated method stub
    return super.get(arg0, arg1);
  }

  @Override
  public boolean get(int bitIndex)
  {
    // TODO Auto-generated method stub
    return super.get(bitIndex);
  }

  @Override
  public int hashCode()
  {
    // TODO Auto-generated method stub
    return super.hashCode();
  }

  @Override
  public boolean intersects(BitSet arg0)
  {
    // TODO Auto-generated method stub
    return super.intersects(arg0);
  }

  @Override
  public boolean isEmpty()
  {
    // TODO Auto-generated method stub
    return super.isEmpty();
  }

  @Override
  public int length()
  {
    // TODO Auto-generated method stub
    return super.length();
  }

  @Override
  public int nextClearBit(int fromIndex)
  {
    // TODO Auto-generated method stub
    return super.nextClearBit(fromIndex);
  }

  @Override
  public int nextSetBit(int fromIndex)
  {
    // TODO Auto-generated method stub
    return super.nextSetBit(fromIndex);
  }

  @Override
  public void or(BitSet arg0)
  {
    // TODO Auto-generated method stub
    super.or(arg0);
  }

  @Override
  public int previousClearBit(int fromIndex)
  {
    // TODO Auto-generated method stub
    return super.previousClearBit(fromIndex);
  }

  @Override
  public int previousSetBit(int fromIndex)
  {
    // TODO Auto-generated method stub
    return super.previousSetBit(fromIndex);
  }

  @Override
  public void set(int bitIndex, boolean value)
  {
    // TODO Auto-generated method stub
    super.set(bitIndex, value);
  }

  @Override
  public void set(int fromIndex, int toIndex, boolean value)
  {
    // TODO Auto-generated method stub
    super.set(fromIndex, toIndex, value);
  }

  @Override
  public void set(int arg0, int arg1)
  {
    // TODO Auto-generated method stub
    super.set(arg0, arg1);
  }

  @Override
  public void set(int bitIndex)
  {
    // TODO Auto-generated method stub
    super.set(bitIndex);
  }

  @Override
  public int size()
  {
    // TODO Auto-generated method stub
    return super.size();
  }

  @Override
  public IntStream stream()
  {
    // TODO Auto-generated method stub
    return super.stream();
  }

  @Override
  public byte[] toByteArray()
  {
    // TODO Auto-generated method stub
    return super.toByteArray();
  }

  @Override
  public long[] toLongArray()
  {
    // TODO Auto-generated method stub
    return super.toLongArray();
  }

  @Override
  public String toString()
  {
    // TODO Auto-generated method stub
    return super.toString();
  }

  @Override
  public void xor(BitSet arg0)
  {
    // TODO Auto-generated method stub
    super.xor(arg0);
  }

  // ----------------------------------------


  public void and(CappedBitSet arg0)
  {
    // TODO Auto-generated method stub
    super.and(arg0);
  }


  public void andNot(CappedBitSet arg0)
  {
    // TODO Auto-generated method stub
    super.andNot(arg0);
  }



  public boolean intersects(CappedBitSet arg0)
  {
    // TODO Auto-generated method stub
    return super.intersects(arg0);
  }



  public void or(CappedBitSet arg0)
  {
    // TODO Auto-generated method stub
    super.or(arg0);
  }


  public void xor(CappedBitSet arg0)
  {
    // TODO Auto-generated method stub
    super.xor(arg0);
  }

  /**
   * @return the cappedSize
   */
  public int getCappedSize()
  {
    return this.cappedSize;
  }

  /**
   * @param cappedSize the cappedSize to set
   */
  public void setCappedSize(int cappedSize)
  {
    this.cappedSize = cappedSize;
  }


}
