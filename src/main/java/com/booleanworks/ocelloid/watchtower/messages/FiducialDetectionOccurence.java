/*
 *
 */

package com.booleanworks.ocelloid.watchtower.messages;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.util.Objects;
import boofcv.abst.fiducial.FiducialStability;
import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point3D_F64;
import georegression.struct.se.Se3_F64;
import georegression.struct.shapes.Polygon2D_F64;

public class FiducialDetectionOccurence implements Serializable, UdpMessage<FiducialDetectionOccurence>
{

  /**
   * Serialization id
   */
  private static final long serialVersionUID = -6172157592246143284L;
  protected final String webcamName;
  protected final long timestamp;
  protected final long imageOccurenceHash;
  protected final String dataAsString;
  // @XmlTransient
  protected final Polygon2D_F64 bounds;
  // @XmlTransient
  protected final Point2D_F64 center;
  // @XmlTransient
  protected final Se3_F64 targetToSensor;
  // @XmlTransient
  protected final Point2D_F64 locationPixel;
  // @XmlTransient
  protected FiducialStability stability;

  protected Point3D_F64 originalLocation;

  protected Point3D_F64 projectedLocation;



  /**
   * @param webcamName
   * @param timestamp
   * @param imageOccurenceHash
   * @param dataAsString
   * @param bounds
   * @param center
   * @param targetToSensor
   * @param locationPixel
   */
  public FiducialDetectionOccurence(String webcamName, long timestamp, long imageOccurenceHash, String dataAsString,
      Polygon2D_F64 bounds, Point2D_F64 center, Se3_F64 targetToSensor, Point2D_F64 locationPixel)
  {
    super();
    this.webcamName = webcamName;
    this.timestamp = timestamp;
    this.imageOccurenceHash = imageOccurenceHash;
    this.dataAsString = dataAsString;
    this.bounds = bounds;
    this.center = center;
    this.targetToSensor = targetToSensor;
    if (Objects.nonNull(this.targetToSensor))
    {
      this.originalLocation = new Point3D_F64(this.targetToSensor.getX(), this.targetToSensor.getY(), this.targetToSensor.getZ());
    }
    this.locationPixel = locationPixel;
  }

  /**
   * @return the webcamName
   */
  public String getWebcamName()
  {
    return this.webcamName;
  }

  /**
   * @return the timestamp
   */
  public long getTimestamp()
  {
    return this.timestamp;
  }

  /**
   * @return the imageOccurenceHash
   */
  public long getImageOccurenceHash()
  {
    return this.imageOccurenceHash;
  }

  /**
   * @return the dataAsString
   */
  public String getDataAsString()
  {
    return this.dataAsString;
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(this.dataAsString, this.imageOccurenceHash, this.timestamp, this.webcamName);
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
    {
      return true;
    }
    if (obj == null)
    {
      return false;
    }
    if (this.getClass() != obj.getClass())
    {
      return false;
    }
    FiducialDetectionOccurence other = (FiducialDetectionOccurence) obj;
    return Objects.equals(this.dataAsString, other.dataAsString) && this.imageOccurenceHash == other.imageOccurenceHash
        && this.timestamp == other.timestamp && Objects.equals(this.webcamName, other.webcamName);
  }

  /**
   * @return the bounds
   */
  public Polygon2D_F64 getBounds()
  {
    return this.bounds;
  }

  /**
   * @return the center
   */
  public Point2D_F64 getCenter()
  {
    return this.center;
  }

  /**
   * @return the targetToSensor
   */
  public Se3_F64 getTargetToSensor()
  {
    if (Objects.nonNull(this.targetToSensor))
    {
      this.originalLocation = new Point3D_F64(this.targetToSensor.getX(), this.targetToSensor.getY(), this.targetToSensor.getZ());
    }
    return this.targetToSensor;
  }

  /**
   * @return the locationPixel
   */
  public Point2D_F64 getLocationPixel()
  {
    return this.locationPixel;
  }

  /**
   * @return the stability
   */
  public FiducialStability getStability()
  {
    return this.stability;
  }

  /**
   * @param stability the stability to set
   */
  public void setStability(FiducialStability stability)
  {
    this.stability = stability;
  }

  @Override
  public boolean isDecodableAs(DatagramPacket packet)
  {
    if (packet == null)
    {
      return false;
    }

    if (packet.getData() != null && packet.getData().length > 4)
    {
      try (ByteArrayInputStream bais = new ByteArrayInputStream(packet.getData());
          ObjectInputStream ois = new ObjectInputStream(bais);)
      {
        Short header = ois.readShort();
        if (header == 0x0131)
        {
          return true;
        }
      }
      catch (Exception e)
      {
        return false;
      }

    }


    return false;
  }

  @Override
  public DatagramPacket encode()
  {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(baos);)
    {

      /** Providing a header (16 bits = 2 bytes) */
      oos.writeShort(0x0131);

      /** Providing tag id (16 bits = 2 bytes) */
      short tagId = -1;
      if (this.dataAsString != null && this.dataAsString.matches("[0-9]+"))
      {
        tagId = Short.parseShort(this.dataAsString);

      }
      oos.writeShort(tagId);

      /** Providing timestamp (64 bits = 4 bytes) */
      oos.writeLong(this.timestamp);



      oos.flush();
      byte[] payload = baos.toByteArray();
      DatagramPacket result = new DatagramPacket(payload, payload.length);
      return result;
    }
    catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public FiducialDetectionOccurence decode(DatagramPacket packet)
  {
    if (packet == null)
    {
      return null;
    }

    if (packet.getData() != null && packet.getData().length > 4)
    {
      try (ByteArrayInputStream bais = new ByteArrayInputStream(packet.getData());
          ObjectInputStream ois = new ObjectInputStream(bais);)
      {
        Short header = ois.readShort();
      }
      catch (Exception e)
      {
        return null;
      }

    }


    return null;
  }

  @Override
  public String getDocumentation()
  {

    return "Not documented now";
  }



  @Override
  public String toString()
  {
    return String.format(
        "FiducialDetectionOccurence [webcamName=%s, timestamp=%s, imageOccurenceHash=%s, dataAsString=%s, bounds=%s, center=%s, targetToSensor=%s, locationPixel=%s, stability=%s, originalLocation=%s, projectedLocation=%s]",
        this.webcamName, this.timestamp, this.imageOccurenceHash, this.dataAsString, this.bounds, this.center,
        this.targetToSensor, this.locationPixel, this.stability, this.originalLocation, this.projectedLocation);
  }


  public String toStringAlt1()
  {
    return String.format(
        "FiducialDetectionOccurence [webcamName=%s, timestamp=%s, imageOccurenceHash=%s, dataAsString=%s,  center=%s,  locationPixel=%s, stability=%s, originalLocation=%s, projectedLocation=%s]",
        this.webcamName, this.timestamp, this.imageOccurenceHash, this.dataAsString, this.center, this.locationPixel,
        this.stability, this.originalLocation, this.projectedLocation);
  }

  /**
   * @return the originalLocation
   */
  public Point3D_F64 getOriginalLocation()
  {
    return this.originalLocation;
  }

  /**
   * @param originalLocation the originalLocation to set
   */
  public void setOriginalLocation(Point3D_F64 originalLocation)
  {
    this.originalLocation = originalLocation;
  }

  /**
   * @return the projectedLocation
   */
  public Point3D_F64 getProjectedLocation()
  {
    return this.projectedLocation;
  }

  /**
   * @param projectedLocation the projectedLocation to set
   */
  public void setProjectedLocation(Point3D_F64 projectedLocation)
  {
    this.projectedLocation = projectedLocation;
  }



}
