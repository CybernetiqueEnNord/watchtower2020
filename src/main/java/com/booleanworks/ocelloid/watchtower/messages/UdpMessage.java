/**
 *
 */

package com.booleanworks.ocelloid.watchtower.messages;

import java.net.DatagramPacket;

/**
 * @author vortigern
 *
 */
public interface UdpMessage<T>
{

  boolean isDecodableAs(DatagramPacket packet);

  DatagramPacket encode();

  T decode(DatagramPacket packet);

  String getDocumentation();

  default void printDocumentation()
  {
    System.out.println(this.getDocumentation());
  }



}
