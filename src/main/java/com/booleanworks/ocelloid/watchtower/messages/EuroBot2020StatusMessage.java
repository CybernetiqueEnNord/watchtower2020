/*
 *
 */

package com.booleanworks.ocelloid.watchtower.messages;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.DatagramPacket;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

public class EuroBot2020StatusMessage
    implements UdpMessage<EuroBot2020StatusMessage>, Comparable<EuroBot2020StatusMessage>, Serializable, Cloneable
{

  /**
   * serialVersionUID.
   */
  private static final long serialVersionUID = -7311647314339987719L;

  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(EuroBot2020StatusMessage.class.getName());


  public final static byte EMITTER_WATCHTOWER = 1;
  public final static byte EMITTER_BIGBOT = 2;
  public final static byte EMITTER_SMALLBOT = 3;
  public final static byte EMITTER_LIGHTHOUSE = 4;

  public final static byte WHEEL_POSITION_UNKNOWN = 0;
  public final static byte WHEEL_POSITION_BLACK_UPSIDE_NORTH = 1;
  public final static byte WHEEL_POSITION_WHITE_UPSIDE_SOUTH = 2;

  public final static byte WHATCHTOWER_SIDE_UNKNOWN = 0;
  public final static byte WHATCHTOWER_SIDE_YELLOW = 1;
  public final static byte WHATCHTOWER_SIDE_BLUE = 2;

  public final static short ROBOT_FACTION_UNKNOWN = 0;
  public final static short ROBOT_FACTION_FRIEND = 1;
  public final static short ROBOT_FACTION_FOE = 2;

  public final static short ROBOT_TYPE_UNKNOWN = 0;
  public final static short ROBOT_TYPE_BIG = 1;
  public final static short ROBOT_TYPE_SMALL = 2;

  public final static short ROBOT_COLOR_UNKNOWN = 0;
  public final static short ROBOT_COLOR_YELLOW = 1;
  public final static short ROBOT_COLOR_BLUE = 2;


  public final static short BUOYS_CODE_UNKNOWN = 0;

  public final static short BUOYS_CODE_01 = 1;

  public final static short BUOYS_CODE_02 = 2;

  public final static short BUOYS_CODE_03 = 3;

  /** 1: watchtower, 2: Big bot, 3: Small bot, 4: Lighthouse */
  protected byte emitter;

  /** Timestamp of the status. (status at this timestamp). Ms from EPOCH (1970-01-01) */
  protected long timestamp;

  /** Seconds since match's start. */
  protected short secondsSinceMatchStart;

  /** 0: Unknown, 1: Black part on top, 2: White part on top. */
  protected byte wheelPosition;

  // -------------------------------------------------------------

  /** Aruco Id of robot 1. 0 if not detected */
  protected short robot1_Aruco;
  /** Faction. 0: Unknown, 1: Friend, 2:Foe */
  protected short robot1_faction;
  /** Faction. 0: Unknown, 1: Big, 2:Small */
  protected short robot1_type;
  /** Faction. 0: Unknown, 1: Yellow, 2:Blue */
  protected short robot1_color;
  /**
   * Global possible error in mm on both X or Y (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot1_position_error;
  /**
   * X position in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be ignored
   * if error equals 0.
   */
  protected short robot1_position_x;
  /**
   * Y position in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if error
   * equals 0.
   */
  protected short robot1_position_y;
  /**
   * Global possible error in mm on both dX or dY (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot1_positionDelta_error;
  /**
   * X position delta in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be
   * ignored if error equals 0.
   */
  protected short robot1_positionDelta_x;
  /**
   * Y position delta in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if
   * error equals 0.
   */
  protected short robot1_positionDelta_y;

  /** 16 bytes of "free data for robot status info". */
  protected byte[] robot1_state = new byte[16];


  // -------------------------------------------------------------

  /** Aruco Id of robot 1. 0 if not detected */
  protected short robot2_Aruco;
  /** Faction. 0: Unknown, 1: Friend, 2:Foe */
  protected short robot2_faction;
  /** Faction. 0: Unknown, 1: Big, 2:Small */
  protected short robot2_type;
  /** Faction. 0: Unknown, 1: Yellow, 2:Blue */
  protected short robot2_color;
  /**
   * Global possible error in mm on both X or Y (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot2_position_error;
  /**
   * X position in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be ignored
   * if error equals 0.
   */
  protected short robot2_position_x;
  /**
   * Y position in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if error
   * equals 0.
   */
  protected short robot2_position_y;
  /**
   * Global possible error in mm on both dX or dY (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot2_positionDelta_error;
  /**
   * X position delta in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be
   * ignored if error equals 0.
   */
  protected short robot2_positionDelta_x;
  /**
   * Y position delta in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if
   * error equals 0.
   */
  protected short robot2_positionDelta_y;

  /** 16 bytes of "free data for robot status info". */
  protected byte[] robot2_state = new byte[16];


  // -------------------------------------------------------------

  /** Aruco Id of robot 1. 0 if not detected */
  protected short robot3_Aruco;
  /** Faction. 0: Unknown, 1: Friend, 2:Foe */
  protected short robot3_faction;
  /** Faction. 0: Unknown, 1: Big, 2:Small */
  protected short robot3_type;
  /** Faction. 0: Unknown, 1: Yellow, 2:Blue */
  protected short robot3_color;
  /**
   * Global possible error in mm on both X or Y (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot3_position_error;
  /**
   * X position in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be ignored
   * if error equals 0.
   */
  protected short robot3_position_x;
  /**
   * Y position in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if error
   * equals 0.
   */
  protected short robot3_position_y;
  /**
   * Global possible error in mm on both dX or dY (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot3_positionDelta_error;
  /**
   * X position delta in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be
   * ignored if error equals 0.
   */
  protected short robot3_positionDelta_x;
  /**
   * Y position delta in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if
   * error equals 0.
   */
  protected short robot3_positionDelta_y;

  /** 16 bytes of "free data for robot status info". */
  protected byte[] robot3_state = new byte[16];
  // -------------------------------------------------------------

  /** Aruco Id of robot 1. 0 if not detected */
  protected short robot4_Aruco;
  /** Faction. 0: Unknown, 1: Friend, 2:Foe */
  protected short robot4_faction;
  /** Faction. 0: Unknown, 1: Big, 2:Small */
  protected short robot4_type;
  /** Faction. 0: Unknown, 1: Yellow, 2:Blue */
  protected short robot4_color;
  /**
   * Global possible error in mm on both X or Y (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot4_position_error;
  /**
   * X position in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be ignored
   * if error equals 0.
   */
  protected short robot4_position_x;
  /**
   * Y position in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if error
   * equals 0.
   */
  protected short robot4_position_y;
  /**
   * Global possible error in mm on both dX or dY (take the worst case). If equals 0 indicate unknown
   * position.
   */
  protected short robot4_positionDelta_error;
  /**
   * X position delta in mm. 0 at table origin as displayed in rules. X+ to the main tower. Shall be
   * ignored if error equals 0.
   */
  protected short robot4_positionDelta_x;
  /**
   * Y position delta in mm. 0 at table origin as displayed in rules. Y+ to port. Shall be ignored if
   * error equals 0.
   */
  protected short robot4_positionDelta_y;

  /** 16 bytes of "free data for robot status info". */
  protected byte[] robot4_state = new byte[16];

  // -------------------------------------------------------------

  /** Watchtower side 0: unknown 1: Yellow 2:Blue */
  protected short watchtowerSide = WHATCHTOWER_SIDE_UNKNOWN;

  /** Buoy code as stated in rules at chapter D.4.a */
  protected short buoysCode = BUOYS_CODE_UNKNOWN;

  /**
   *
   */
  public EuroBot2020StatusMessage()
  {
    super();
    this.emitter = 1;
    this.timestamp = System.currentTimeMillis();
    this.secondsSinceMatchStart = 0;
    this.wheelPosition = 0;
    this.robot1_Aruco = 0;
    this.robot1_faction = 0;
    this.robot1_type = 0;
    this.robot1_color = 0;
    this.robot1_position_error = 0;
    this.robot1_position_x = 0;
    this.robot1_position_y = 0;
    this.robot1_positionDelta_error = 0;
    this.robot1_positionDelta_x = 0;
    this.robot1_positionDelta_y = 0;
    this.robot1_state = new byte[16];
    this.robot2_Aruco = 0;
    this.robot2_faction = 0;
    this.robot2_type = 0;
    this.robot2_color = 0;
    this.robot2_position_error = 0;
    this.robot2_position_x = 0;
    this.robot2_position_y = 0;
    this.robot2_positionDelta_error = 0;
    this.robot2_positionDelta_x = 0;
    this.robot2_positionDelta_y = 0;
    this.robot2_state = new byte[16];
    this.robot3_Aruco = 0;
    this.robot3_faction = 0;
    this.robot3_type = 0;
    this.robot3_color = 0;
    this.robot3_position_error = 0;
    this.robot3_position_x = 0;
    this.robot3_position_y = 0;
    this.robot3_positionDelta_error = 0;
    this.robot3_positionDelta_x = 0;
    this.robot3_positionDelta_y = 0;
    this.robot3_state = new byte[]
    {(byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77,
        (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77};
    this.robot4_Aruco = 0;
    this.robot4_faction = 0;
    this.robot4_type = 0;
    this.robot4_color = 0;
    this.robot4_position_error = 0;
    this.robot4_position_x = 0;
    this.robot4_position_y = 0;
    this.robot4_positionDelta_error = 0;
    this.robot4_positionDelta_x = 0;
    this.robot4_positionDelta_y = 0;
    this.robot4_state = new byte[]
    {(byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77,
        (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77, (byte) 0x77};
    this.watchtowerSide = WHATCHTOWER_SIDE_UNKNOWN;
    this.buoysCode = BUOYS_CODE_UNKNOWN;
  }

  @Override
  public boolean isDecodableAs(DatagramPacket packet)
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public DatagramPacket encode()
  {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(baos);)
    {

      // Provided offsets don't take account of the one added by Java when making serialization.

      /** Providing a header (16 bits = 2 bytes, offset X+0) */
      oos.writeShort(0x0535);
      // EuroBot2020StatusMessage.logBytes("After header (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());

      /** Emitter identity (8 bits = 1 bytes, offset X+2) */
      oos.writeByte(this.emitter);
      // EuroBot2020StatusMessage.logBytes("After emitter (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());

      /** Timestamp, ms from 1970-01-01 (64 bits = 8 bytes, offset X+3) */
      oos.writeLong(this.timestamp);
      // EuroBot2020StatusMessage.logBytes("After timestamp (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());

      /** Seconds since beginning of the match (16 bits = 2 bytes, offset X+11) */
      oos.writeShort(this.secondsSinceMatchStart);
      // EuroBot2020StatusMessage.logBytes("After secondsSinceMatchStart (size:" +
      // baos.toByteArray().length + "): ",
      // baos.toByteArray());

      /** Wheel position. (8 bits = 1 bytes, offset X+13) */
      oos.writeByte(this.wheelPosition);
      // EuroBot2020StatusMessage.logBytes("After wheelPosition (size:" + baos.toByteArray().length + "):
      // ", baos.toByteArray());

      /** robot1 aruco (16 bits = 2 bytes, offset X+R+14) */
      oos.writeShort(this.robot1_Aruco);

      /** robot1 faction (16 bits = 2 bytes, offset X+R+16) */
      oos.writeShort(this.robot1_faction);

      /** robot1 type (16 bits = 2 bytes, offset X+R+18) */
      oos.writeShort(this.robot1_type);

      /** robot1 color (16 bits = 2 bytes, offset X+R+20) */
      oos.writeShort(this.robot1_color);

      /** robot1 position error (16 bits = 2 bytes, offset X+R+22) */
      oos.writeShort(this.robot1_position_error);

      /** robot1 position x (16 bits = 2 bytes, offset X+R+24) */
      oos.writeShort(this.robot1_position_x);

      /** robot1 position y (16 bits = 2 bytes, offset X+R+26) */
      oos.writeShort(this.robot1_position_y);

      /** robot1 position delta error (16 bits = 2 bytes, offset X+R+28) */
      oos.writeShort(this.robot1_positionDelta_error);

      /** robot1 position delta x (16 bits = 2 bytes, offset X+R+30) */
      oos.writeShort(this.robot1_positionDelta_x);

      /** robot1 position delta y (16 bits = 2 bytes, offset X+R+32) */
      oos.writeShort(this.robot1_positionDelta_y);

      /** robot1 status data (128 bits = 16 bytes, offset X+R+34) */
      oos.write(this.robot1_state);
      // EuroBot2020StatusMessage.logBytes("After robot1 (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());


      /** robot2 aruco (16 bits = 2 bytes, offset X+R+14) */
      oos.writeShort(this.robot2_Aruco);

      /** robot2 faction (16 bits = 2 bytes, offset X+R+16) */
      oos.writeShort(this.robot2_faction);

      /** robot2 type (16 bits = 2 bytes, offset X+R+18) */
      oos.writeShort(this.robot2_type);

      /** robot2 color (16 bits = 2 bytes, offset X+R+20) */
      oos.writeShort(this.robot2_color);

      /** robot2 position error (16 bits = 2 bytes, offset X+R+22) */
      oos.writeShort(this.robot2_position_error);

      /** robot2 position x (16 bits = 2 bytes, offset X+R+24) */
      oos.writeShort(this.robot2_position_x);

      /** robot2 position y (16 bits = 2 bytes, offset X+R+26) */
      oos.writeShort(this.robot2_position_y);

      /** robot2 position delta error (16 bits = 2 bytes, offset X+R+28) */
      oos.writeShort(this.robot2_positionDelta_error);

      /** robot2 position delta x (16 bits = 2 bytes, offset X+R+30) */
      oos.writeShort(this.robot2_positionDelta_x);

      /** robot2 position delta y (16 bits = 2 bytes, offset X+R+32) */
      oos.writeShort(this.robot2_positionDelta_y);

      /** robot2 status data (128 bits = 16 bytes, offset X+R+34) */
      oos.write(this.robot2_state);
      // EuroBot2020StatusMessage.logBytes("After robot2 (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());


      /** robot3 aruco (16 bits = 2 bytes, offset X+R+14) */
      oos.writeShort(this.robot3_Aruco);

      /** robot3 faction (16 bits = 2 bytes, offset X+R+16) */
      oos.writeShort(this.robot3_faction);

      /** robot3 type (16 bits = 2 bytes, offset X+R+18) */
      oos.writeShort(this.robot3_type);

      /** robot3 color (16 bits = 2 bytes, offset X+R+20) */
      oos.writeShort(this.robot3_color);

      /** robot3 position error (16 bits = 2 bytes, offset X+R+22) */
      oos.writeShort(this.robot3_position_error);

      /** robot3 position x (16 bits = 2 bytes, offset X+R+24) */
      oos.writeShort(this.robot3_position_x);

      /** robot3 position y (16 bits = 2 bytes, offset X+R+26) */
      oos.writeShort(this.robot3_position_y);

      /** robot3 position delta error (16 bits = 2 bytes, offset X+R+28) */
      oos.writeShort(this.robot3_positionDelta_error);

      /** robot3 position delta x (16 bits = 2 bytes, offset X+R+30) */
      oos.writeShort(this.robot3_positionDelta_x);

      /** robot3 position delta y (16 bits = 2 bytes, offset X+R+32) */
      oos.writeShort(this.robot3_positionDelta_y);

      /** robot3 status data (128 bits = 16 bytes, offset X+R+34) */
      oos.write(this.robot3_state);
      // EuroBot2020StatusMessage.logBytes("After robot3 (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());


      /** robot4 aruco (16 bits = 2 bytes, offset X+R+14) */
      oos.writeShort(this.robot4_Aruco);

      /** robot4 faction (16 bits = 2 bytes, offset X+R+16) */
      oos.writeShort(this.robot4_faction);

      /** robot4 type (16 bits = 2 bytes, offset X+R+18) */
      oos.writeShort(this.robot4_type);

      /** robot4 color (16 bits = 2 bytes, offset X+R+20) */
      oos.writeShort(this.robot4_color);

      /** robot4 position error (16 bits = 2 bytes, offset X+R+22) */
      oos.writeShort(this.robot4_position_error);

      /** robot4 position x (16 bits = 2 bytes, offset X+R+24) */
      oos.writeShort(this.robot4_position_x);

      /** robot4 position y (16 bits = 2 bytes, offset X+R+26) */
      oos.writeShort(this.robot4_position_y);

      /** robot4 position delta error (16 bits = 2 bytes, offset X+R+28) */
      oos.writeShort(this.robot4_positionDelta_error);

      /** robot4 position delta x (16 bits = 2 bytes, offset X+R+30) */
      oos.writeShort(this.robot4_positionDelta_x);

      /** robot4 position delta y (16 bits = 2 bytes, offset X+R+32) */
      oos.writeShort(this.robot4_positionDelta_y);
      // EuroBot2020StatusMessage.logBytes("After robot4 (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());

      /** robot4 status data (128 bits = 16 bytes, offset X+R+34) */
      oos.write(this.robot4_state);
      // EuroBot2020StatusMessage.logBytes("After robot4_state (size:" + baos.toByteArray().length + "):
      // ", baos.toByteArray());


      /** watchtower size (16 bits = 2 bytes, offset ???) */
      oos.writeShort(this.watchtowerSide);
      // EuroBot2020StatusMessage.logBytes("After watchtowerSide (size:" + baos.toByteArray().length + "):
      // ", baos.toByteArray());

      /** buoys code (16 bits = 2 bytes, offset ??? */
      oos.writeShort(this.buoysCode);
      // EuroBot2020StatusMessage.logBytes("After buoys code (size:" + baos.toByteArray().length + "): ",
      // baos.toByteArray());


      oos.flush();
      byte[] payload = baos.toByteArray();

      LOG.fine("EuroBot2020StatusMessage, encoded message size: " + payload.length);

      DatagramPacket result = new DatagramPacket(payload, payload.length);
      result.setLength(payload.length);
      return result;
    }
    catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public EuroBot2020StatusMessage decode(DatagramPacket packet)
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getDocumentation()
  {
    // TODO Auto-generated method stub
    return "TBD";
  }

  @Override
  public int compareTo(EuroBot2020StatusMessage arg0)
  {
    if (arg0 == null)
    {
      return -1;
    }

    return arg0.hashCode() == this.hashCode() ? 0 : -2;
  }

  public static void main(String[] args)
  {

    Random r = new Random();

    EuroBot2020StatusMessage msg = new EuroBot2020StatusMessage();
    msg.emitter = 1;
    msg.timestamp = System.currentTimeMillis();
    msg.secondsSinceMatchStart = (short) r.nextInt(100);
    msg.wheelPosition = (byte) r.nextInt(2);

    msg.robot1_Aruco = 1;
    msg.robot1_faction = 1;
    msg.robot1_type = 1;
    msg.robot1_color = 1;
    msg.robot1_position_error = 30;
    msg.robot1_position_x = 50;
    msg.robot1_position_y = 50;
    msg.robot1_positionDelta_error = 50;
    msg.robot1_positionDelta_x = 0;
    msg.robot1_positionDelta_y = 0;
    msg.robot1_state = new byte[16];
    for (byte ctb = 0; ctb < 16; ctb++)
    {
      msg.robot1_state[ctb] = ctb;
    }

    msg.robot2_Aruco = 1;
    msg.robot2_faction = 1;
    msg.robot2_type = 2;
    msg.robot2_color = 1;
    msg.robot2_position_error = 30;
    msg.robot2_position_x = (short) r.nextInt(2000);
    msg.robot2_position_y = (short) r.nextInt(3000);
    msg.robot2_positionDelta_error = 50;
    msg.robot2_positionDelta_x = 0;
    msg.robot2_positionDelta_y = 0;
    msg.robot2_state = new byte[16];
    for (byte ctb = 0; ctb < 16; ctb++)
    {
      msg.robot2_state[ctb] = ctb;
    }

    msg.robot3_Aruco = 1;
    msg.robot3_faction = 2;
    msg.robot3_type = 0;
    msg.robot3_color = 2;
    msg.robot3_position_error = 30;
    msg.robot3_position_x = 2000;
    msg.robot3_position_y = 3000;
    msg.robot3_positionDelta_error = 50;
    msg.robot3_positionDelta_x = 0;
    msg.robot3_positionDelta_y = 0;
    msg.robot3_state = new byte[16];
    for (byte ctb = 0; ctb < 16; ctb++)
    {
      msg.robot3_state[ctb] = ctb;
    }

    msg.robot4_Aruco = 1;
    msg.robot4_faction = 2;
    msg.robot4_type = 0;
    msg.robot4_color = 2;
    msg.robot4_position_error = (short) (r.nextInt(5) * 10);
    msg.robot4_position_x = (short) r.nextInt(2000);
    msg.robot4_position_y = (short) r.nextInt(3000);
    msg.robot4_positionDelta_error = (short) (r.nextInt(5) * 10);
    msg.robot4_positionDelta_x = (short) ((short) r.nextInt(600) - 300);
    msg.robot4_positionDelta_y = (short) ((short) r.nextInt(600) - 300);
    msg.robot4_state = new byte[16];
    for (byte ctb = 0; ctb < 16; ctb++)
    {
      msg.robot4_state[ctb] = ctb;
    }

    String filename = "EuroBot2020StatusMessage_example_frame_" + r.nextInt() + ".bin";

    try (FileOutputStream fos = new FileOutputStream(filename, false);)
    {

      System.out.println("filename = " + filename);
      System.out.println("msg = " + msg.toString());

      fos.write(msg.encode().getData());
      fos.flush();
      fos.close();
    }
    catch (Exception e)
    {
      System.err.println("failed");
    }



  }



  @Override
  public String toString()
  {
    return String.format(
        "EuroBot2020StatusMessage [emitter=%s, timestamp=%s, secondsSinceMatchStart=%s, wheelPosition=%s, robot1_Aruco=%s, robot1_faction=%s, robot1_type=%s, robot1_color=%s, robot1_position_error=%s, robot1_position_x=%s, robot1_position_y=%s, robot1_positionDelta_error=%s, robot1_positionDelta_x=%s, robot1_positionDelta_y=%s, robot1_state=%s, robot2_Aruco=%s, robot2_faction=%s, robot2_type=%s, robot2_color=%s, robot2_position_error=%s, robot2_position_x=%s, robot2_position_y=%s, robot2_positionDelta_error=%s, robot2_positionDelta_x=%s, robot2_positionDelta_y=%s, robot2_state=%s, robot3_Aruco=%s, robot3_faction=%s, robot3_type=%s, robot3_color=%s, robot3_position_error=%s, robot3_position_x=%s, robot3_position_y=%s, robot3_positionDelta_error=%s, robot3_positionDelta_x=%s, robot3_positionDelta_y=%s, robot3_state=%s, robot4_Aruco=%s, robot4_faction=%s, robot4_type=%s, robot4_color=%s, robot4_position_error=%s, robot4_position_x=%s, robot4_position_y=%s, robot4_positionDelta_error=%s, robot4_positionDelta_x=%s, robot4_positionDelta_y=%s, robot4_state=%s, watchtowerSide=%s, buoysCode=%s]",
        this.emitter, this.timestamp, this.secondsSinceMatchStart, this.wheelPosition, this.robot1_Aruco, this.robot1_faction,
        this.robot1_type, this.robot1_color, this.robot1_position_error, this.robot1_position_x, this.robot1_position_y,
        this.robot1_positionDelta_error, this.robot1_positionDelta_x, this.robot1_positionDelta_y,
        Arrays.toString(this.robot1_state), this.robot2_Aruco, this.robot2_faction, this.robot2_type, this.robot2_color,
        this.robot2_position_error, this.robot2_position_x, this.robot2_position_y, this.robot2_positionDelta_error,
        this.robot2_positionDelta_x, this.robot2_positionDelta_y, Arrays.toString(this.robot2_state), this.robot3_Aruco,
        this.robot3_faction, this.robot3_type, this.robot3_color, this.robot3_position_error, this.robot3_position_x,
        this.robot3_position_y, this.robot3_positionDelta_error, this.robot3_positionDelta_x, this.robot3_positionDelta_y,
        Arrays.toString(this.robot3_state), this.robot4_Aruco, this.robot4_faction, this.robot4_type, this.robot4_color,
        this.robot4_position_error, this.robot4_position_x, this.robot4_position_y, this.robot4_positionDelta_error,
        this.robot4_positionDelta_x, this.robot4_positionDelta_y, Arrays.toString(this.robot4_state), this.watchtowerSide,
        this.buoysCode);
  }

  /**
   * @return the emitter
   */
  public byte getEmitter()
  {
    return this.emitter;
  }

  /**
   * @return the timestamp
   */
  public long getTimestamp()
  {
    return this.timestamp;
  }

  /**
   * @return the secondsSinceMatchStart
   */
  public short getSecondsSinceMatchStart()
  {
    return this.secondsSinceMatchStart;
  }

  /**
   * @return the wheelPosition
   */
  public byte getWheelPosition()
  {
    return this.wheelPosition;
  }

  /**
   * @return the robot1_Aruco
   */
  public short getRobot1_Aruco()
  {
    return this.robot1_Aruco;
  }

  /**
   * @return the robot1_faction
   */
  public short getRobot1_faction()
  {
    return this.robot1_faction;
  }

  /**
   * @return the robot1_type
   */
  public short getRobot1_type()
  {
    return this.robot1_type;
  }

  /**
   * @return the robot1_color
   */
  public short getRobot1_color()
  {
    return this.robot1_color;
  }

  /**
   * @return the robot1_position_error
   */
  public short getRobot1_position_error()
  {
    return this.robot1_position_error;
  }

  /**
   * @return the robot1_position_x
   */
  public short getRobot1_position_x()
  {
    return this.robot1_position_x;
  }

  /**
   * @return the robot1_position_y
   */
  public short getRobot1_position_y()
  {
    return this.robot1_position_y;
  }

  /**
   * @return the robot1_positionDelta_error
   */
  public short getRobot1_positionDelta_error()
  {
    return this.robot1_positionDelta_error;
  }

  /**
   * @return the robot1_positionDelta_x
   */
  public short getRobot1_positionDelta_x()
  {
    return this.robot1_positionDelta_x;
  }

  /**
   * @return the robot1_positionDelta_y
   */
  public short getRobot1_positionDelta_y()
  {
    return this.robot1_positionDelta_y;
  }

  /**
   * @return the robot1_state
   */
  public byte[] getRobot1_state()
  {
    return this.robot1_state;
  }

  /**
   * @return the robot2_Aruco
   */
  public short getRobot2_Aruco()
  {
    return this.robot2_Aruco;
  }

  /**
   * @return the robot2_faction
   */
  public short getRobot2_faction()
  {
    return this.robot2_faction;
  }

  /**
   * @return the robot2_type
   */
  public short getRobot2_type()
  {
    return this.robot2_type;
  }

  /**
   * @return the robot2_color
   */
  public short getRobot2_color()
  {
    return this.robot2_color;
  }

  /**
   * @return the robot2_position_error
   */
  public short getRobot2_position_error()
  {
    return this.robot2_position_error;
  }

  /**
   * @return the robot2_position_x
   */
  public short getRobot2_position_x()
  {
    return this.robot2_position_x;
  }

  /**
   * @return the robot2_position_y
   */
  public short getRobot2_position_y()
  {
    return this.robot2_position_y;
  }

  /**
   * @return the robot2_positionDelta_error
   */
  public short getRobot2_positionDelta_error()
  {
    return this.robot2_positionDelta_error;
  }

  /**
   * @return the robot2_positionDelta_x
   */
  public short getRobot2_positionDelta_x()
  {
    return this.robot2_positionDelta_x;
  }

  /**
   * @return the robot2_positionDelta_y
   */
  public short getRobot2_positionDelta_y()
  {
    return this.robot2_positionDelta_y;
  }

  /**
   * @return the robot2_state
   */
  public byte[] getRobot2_state()
  {
    return this.robot2_state;
  }

  /**
   * @return the robot3_Aruco
   */
  public short getRobot3_Aruco()
  {
    return this.robot3_Aruco;
  }

  /**
   * @return the robot3_faction
   */
  public short getRobot3_faction()
  {
    return this.robot3_faction;
  }

  /**
   * @return the robot3_type
   */
  public short getRobot3_type()
  {
    return this.robot3_type;
  }

  /**
   * @return the robot3_color
   */
  public short getRobot3_color()
  {
    return this.robot3_color;
  }

  /**
   * @return the robot3_position_error
   */
  public short getRobot3_position_error()
  {
    return this.robot3_position_error;
  }

  /**
   * @return the robot3_position_x
   */
  public short getRobot3_position_x()
  {
    return this.robot3_position_x;
  }

  /**
   * @return the robot3_position_y
   */
  public short getRobot3_position_y()
  {
    return this.robot3_position_y;
  }

  /**
   * @return the robot3_positionDelta_error
   */
  public short getRobot3_positionDelta_error()
  {
    return this.robot3_positionDelta_error;
  }

  /**
   * @return the robot3_positionDelta_x
   */
  public short getRobot3_positionDelta_x()
  {
    return this.robot3_positionDelta_x;
  }

  /**
   * @return the robot3_positionDelta_y
   */
  public short getRobot3_positionDelta_y()
  {
    return this.robot3_positionDelta_y;
  }

  /**
   * @return the robot3_state
   */
  public byte[] getRobot3_state()
  {
    return this.robot3_state;
  }

  /**
   * @return the robot4_Aruco
   */
  public short getRobot4_Aruco()
  {
    return this.robot4_Aruco;
  }

  /**
   * @return the robot4_faction
   */
  public short getRobot4_faction()
  {
    return this.robot4_faction;
  }

  /**
   * @return the robot4_type
   */
  public short getRobot4_type()
  {
    return this.robot4_type;
  }

  /**
   * @return the robot4_color
   */
  public short getRobot4_color()
  {
    return this.robot4_color;
  }

  /**
   * @return the robot4_position_error
   */
  public short getRobot4_position_error()
  {
    return this.robot4_position_error;
  }

  /**
   * @return the robot4_position_x
   */
  public short getRobot4_position_x()
  {
    return this.robot4_position_x;
  }

  /**
   * @return the robot4_position_y
   */
  public short getRobot4_position_y()
  {
    return this.robot4_position_y;
  }

  /**
   * @return the robot4_positionDelta_error
   */
  public short getRobot4_positionDelta_error()
  {
    return this.robot4_positionDelta_error;
  }

  /**
   * @return the robot4_positionDelta_x
   */
  public short getRobot4_positionDelta_x()
  {
    return this.robot4_positionDelta_x;
  }

  /**
   * @return the robot4_positionDelta_y
   */
  public short getRobot4_positionDelta_y()
  {
    return this.robot4_positionDelta_y;
  }

  /**
   * @return the robot4_state
   */
  public byte[] getRobot4_state()
  {
    return this.robot4_state;
  }

  /**
   * @param emitter the emitter to set
   */
  public void setEmitter(byte emitter)
  {
    this.emitter = emitter;
  }

  /**
   * @param timestamp the timestamp to set
   */
  public void setTimestamp(long timestamp)
  {
    this.timestamp = timestamp;
  }

  /**
   * @param secondsSinceMatchStart the secondsSinceMatchStart to set
   */
  public void setSecondsSinceMatchStart(short secondsSinceMatchStart)
  {
    this.secondsSinceMatchStart = secondsSinceMatchStart;
  }

  /**
   * @param wheelPosition the wheelPosition to set
   */
  public void setWheelPosition(byte wheelPosition)
  {
    this.wheelPosition = wheelPosition;
  }

  /**
   * @param robot1_Aruco the robot1_Aruco to set
   */
  public void setRobot1_Aruco(short robot1_Aruco)
  {
    this.robot1_Aruco = robot1_Aruco;
  }

  /**
   * @param robot1_faction the robot1_faction to set
   */
  public void setRobot1_faction(short robot1_faction)
  {
    this.robot1_faction = robot1_faction;
  }

  /**
   * @param robot1_type the robot1_type to set
   */
  public void setRobot1_type(short robot1_type)
  {
    this.robot1_type = robot1_type;
  }

  /**
   * @param robot1_color the robot1_color to set
   */
  public void setRobot1_color(short robot1_color)
  {
    this.robot1_color = robot1_color;
  }

  /**
   * @param robot1_position_error the robot1_position_error to set
   */
  public void setRobot1_position_error(short robot1_position_error)
  {
    this.robot1_position_error = robot1_position_error;
  }

  /**
   * @param robot1_position_x the robot1_position_x to set
   */
  public void setRobot1_position_x(short robot1_position_x)
  {
    this.robot1_position_x = robot1_position_x;
  }

  /**
   * @param robot1_position_y the robot1_position_y to set
   */
  public void setRobot1_position_y(short robot1_position_y)
  {
    this.robot1_position_y = robot1_position_y;
  }

  /**
   * @param robot1_positionDelta_error the robot1_positionDelta_error to set
   */
  public void setRobot1_positionDelta_error(short robot1_positionDelta_error)
  {
    this.robot1_positionDelta_error = robot1_positionDelta_error;
  }

  /**
   * @param robot1_positionDelta_x the robot1_positionDelta_x to set
   */
  public void setRobot1_positionDelta_x(short robot1_positionDelta_x)
  {
    this.robot1_positionDelta_x = robot1_positionDelta_x;
  }

  /**
   * @param robot1_positionDelta_y the robot1_positionDelta_y to set
   */
  public void setRobot1_positionDelta_y(short robot1_positionDelta_y)
  {
    this.robot1_positionDelta_y = robot1_positionDelta_y;
  }

  /**
   * @param robot1_state the robot1_state to set
   */
  public void setRobot1_state(byte[] robot1_state)
  {
    this.robot1_state = robot1_state;
  }

  /**
   * @param robot2_Aruco the robot2_Aruco to set
   */
  public void setRobot2_Aruco(short robot2_Aruco)
  {
    this.robot2_Aruco = robot2_Aruco;
  }

  /**
   * @param robot2_faction the robot2_faction to set
   */
  public void setRobot2_faction(short robot2_faction)
  {
    this.robot2_faction = robot2_faction;
  }

  /**
   * @param robot2_type the robot2_type to set
   */
  public void setRobot2_type(short robot2_type)
  {
    this.robot2_type = robot2_type;
  }

  /**
   * @param robot2_color the robot2_color to set
   */
  public void setRobot2_color(short robot2_color)
  {
    this.robot2_color = robot2_color;
  }

  /**
   * @param robot2_position_error the robot2_position_error to set
   */
  public void setRobot2_position_error(short robot2_position_error)
  {
    this.robot2_position_error = robot2_position_error;
  }

  /**
   * @param robot2_position_x the robot2_position_x to set
   */
  public void setRobot2_position_x(short robot2_position_x)
  {
    this.robot2_position_x = robot2_position_x;
  }

  /**
   * @param robot2_position_y the robot2_position_y to set
   */
  public void setRobot2_position_y(short robot2_position_y)
  {
    this.robot2_position_y = robot2_position_y;
  }

  /**
   * @param robot2_positionDelta_error the robot2_positionDelta_error to set
   */
  public void setRobot2_positionDelta_error(short robot2_positionDelta_error)
  {
    this.robot2_positionDelta_error = robot2_positionDelta_error;
  }

  /**
   * @param robot2_positionDelta_x the robot2_positionDelta_x to set
   */
  public void setRobot2_positionDelta_x(short robot2_positionDelta_x)
  {
    this.robot2_positionDelta_x = robot2_positionDelta_x;
  }

  /**
   * @param robot2_positionDelta_y the robot2_positionDelta_y to set
   */
  public void setRobot2_positionDelta_y(short robot2_positionDelta_y)
  {
    this.robot2_positionDelta_y = robot2_positionDelta_y;
  }

  /**
   * @param robot2_state the robot2_state to set
   */
  public void setRobot2_state(byte[] robot2_state)
  {
    this.robot2_state = robot2_state;
  }

  /**
   * @param robot3_Aruco the robot3_Aruco to set
   */
  public void setRobot3_Aruco(short robot3_Aruco)
  {
    this.robot3_Aruco = robot3_Aruco;
  }

  /**
   * @param robot3_faction the robot3_faction to set
   */
  public void setRobot3_faction(short robot3_faction)
  {
    this.robot3_faction = robot3_faction;
  }

  /**
   * @param robot3_type the robot3_type to set
   */
  public void setRobot3_type(short robot3_type)
  {
    this.robot3_type = robot3_type;
  }

  /**
   * @param robot3_color the robot3_color to set
   */
  public void setRobot3_color(short robot3_color)
  {
    this.robot3_color = robot3_color;
  }

  /**
   * @param robot3_position_error the robot3_position_error to set
   */
  public void setRobot3_position_error(short robot3_position_error)
  {
    this.robot3_position_error = robot3_position_error;
  }

  /**
   * @param robot3_position_x the robot3_position_x to set
   */
  public void setRobot3_position_x(short robot3_position_x)
  {
    this.robot3_position_x = robot3_position_x;
  }

  /**
   * @param robot3_position_y the robot3_position_y to set
   */
  public void setRobot3_position_y(short robot3_position_y)
  {
    this.robot3_position_y = robot3_position_y;
  }

  /**
   * @param robot3_positionDelta_error the robot3_positionDelta_error to set
   */
  public void setRobot3_positionDelta_error(short robot3_positionDelta_error)
  {
    this.robot3_positionDelta_error = robot3_positionDelta_error;
  }

  /**
   * @param robot3_positionDelta_x the robot3_positionDelta_x to set
   */
  public void setRobot3_positionDelta_x(short robot3_positionDelta_x)
  {
    this.robot3_positionDelta_x = robot3_positionDelta_x;
  }

  /**
   * @param robot3_positionDelta_y the robot3_positionDelta_y to set
   */
  public void setRobot3_positionDelta_y(short robot3_positionDelta_y)
  {
    this.robot3_positionDelta_y = robot3_positionDelta_y;
  }

  /**
   * @param robot3_state the robot3_state to set
   */
  public void setRobot3_state(byte[] robot3_state)
  {
    this.robot3_state = robot3_state;
  }

  /**
   * @param robot4_Aruco the robot4_Aruco to set
   */
  public void setRobot4_Aruco(short robot4_Aruco)
  {
    this.robot4_Aruco = robot4_Aruco;
  }

  /**
   * @param robot4_faction the robot4_faction to set
   */
  public void setRobot4_faction(short robot4_faction)
  {
    this.robot4_faction = robot4_faction;
  }

  /**
   * @param robot4_type the robot4_type to set
   */
  public void setRobot4_type(short robot4_type)
  {
    this.robot4_type = robot4_type;
  }

  /**
   * @param robot4_color the robot4_color to set
   */
  public void setRobot4_color(short robot4_color)
  {
    this.robot4_color = robot4_color;
  }

  /**
   * @param robot4_position_error the robot4_position_error to set
   */
  public void setRobot4_position_error(short robot4_position_error)
  {
    this.robot4_position_error = robot4_position_error;
  }

  /**
   * @param robot4_position_x the robot4_position_x to set
   */
  public void setRobot4_position_x(short robot4_position_x)
  {
    this.robot4_position_x = robot4_position_x;
  }

  /**
   * @param robot4_position_y the robot4_position_y to set
   */
  public void setRobot4_position_y(short robot4_position_y)
  {
    this.robot4_position_y = robot4_position_y;
  }

  /**
   * @param robot4_positionDelta_error the robot4_positionDelta_error to set
   */
  public void setRobot4_positionDelta_error(short robot4_positionDelta_error)
  {
    this.robot4_positionDelta_error = robot4_positionDelta_error;
  }

  /**
   * @param robot4_positionDelta_x the robot4_positionDelta_x to set
   */
  public void setRobot4_positionDelta_x(short robot4_positionDelta_x)
  {
    this.robot4_positionDelta_x = robot4_positionDelta_x;
  }

  /**
   * @param robot4_positionDelta_y the robot4_positionDelta_y to set
   */
  public void setRobot4_positionDelta_y(short robot4_positionDelta_y)
  {
    this.robot4_positionDelta_y = robot4_positionDelta_y;
  }

  /**
   * @param robot4_state the robot4_state to set
   */
  public void setRobot4_state(byte[] robot4_state)
  {
    this.robot4_state = robot4_state;
  }

  /**
   * @return the watchtowerSide
   */
  public short getWatchtowerSide()
  {
    return this.watchtowerSide;
  }

  /**
   * @param watchtowerSide the watchtowerSide to set
   */
  public void setWatchtowerSide(short watchtowerSide)
  {
    this.watchtowerSide = watchtowerSide;
  }



  /**
   * @return the buoysCode
   */
  public short getBuoysCode()
  {
    return this.buoysCode;
  }

  /**
   * @param buoysCode the buoysCode to set
   */
  public void setBuoysCode(short buoysCode)
  {
    this.buoysCode = buoysCode;
  }

  public static void logBytes(String heading, byte[] bytes)
  {
    String dataAsHex = "";
    for (byte cb : bytes)
    {
      dataAsHex += String.format(" %02X", cb);
    }

    LOG.fine(heading + dataAsHex);

  }

  @Override
  public Object clone() throws CloneNotSupportedException
  {
    EuroBot2020StatusMessage result = new EuroBot2020StatusMessage();

    for (Field cField : this.getClass().getDeclaredFields())
    {
      try
      {
        int modifiers = cField.getModifiers();

        boolean isFinal = (modifiers & Modifier.FINAL) == Modifier.FINAL;
        boolean isStatic = (modifiers & Modifier.FINAL) == Modifier.STATIC;

        if (!isFinal && !isStatic && !cField.isEnumConstant() && !cField.getType().isArray() && cField.getType().isPrimitive())
        {
          cField.set(result, cField.get(this));

        }
        else if (!cField.isEnumConstant() && cField.getType().isArray() && cField.getType() == Byte.class)
        {
          byte[] srcByteArray = (byte[]) cField.get(this);

          if (srcByteArray != null)
          {
            byte[] dstByteArray = Arrays.copyOf(srcByteArray, srcByteArray.length);
            cField.set(result, dstByteArray);
          }

        }
      }
      catch (IllegalArgumentException | IllegalAccessException e)
      {
        LOG.warning("Error while cloning message on field " + cField.getName() + " with error: " + e.getMessage());
        e.printStackTrace();
      }


    }

    return result;

  }

}
