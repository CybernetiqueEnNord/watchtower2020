/*
 *
 */

package com.booleanworks.ocelloid.watchtower.messages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import com.booleanworks.ocelloid.watchtower.WatchTowerService;

public class UdpHelloMessage
    implements UdpMessage<UdpHelloMessage>, Comparable<UdpHelloMessage>, Serializable {

  /**
   * serialVersionUID.
   */
  private static final long serialVersionUID = -7311647304339987719L;

  @Override
  public boolean isDecodableAs(DatagramPacket packet) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public DatagramPacket encode() {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);) {

      /** Providing a header (16 bits = 2 bytes) */
      oos.writeShort(0x0311);


      /** Providing timestamp (64 bits = 4 bytes) */
      oos.writeLong(System.currentTimeMillis());

      oos.writeUTF("HELLO" + "\r\n");
      oos.writeUTF("TIME:" + System.currentTimeMillis() + "\r\n");
      oos.writeUTF("ARUCO-COUNT:" + WatchTowerService.getInstance().getArucoDetectionService()
          .getFiducialOccurences().size() + "\r\n");


      oos.flush();
      byte[] payload = baos.toByteArray();
      DatagramPacket result = new DatagramPacket(payload, payload.length);
      return result;
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public UdpHelloMessage decode(DatagramPacket packet) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getDocumentation() {
    // TODO Auto-generated method stub
    return "TBD";
  }

  @Override
  public int compareTo(UdpHelloMessage arg0) {
    if (arg0 == null) {
      return -1;
    }

    return arg0.hashCode() == this.hashCode() ? 0 : -2;
  }

}
