/*
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.util.TreeMap;
import java.util.function.Function;
import boofcv.alg.distort.LensDistortionNarrowFOV;
import boofcv.alg.distort.brown.LensDistortionBrown;
import boofcv.io.calibration.CalibrationIO;
import boofcv.struct.calib.CameraPinholeBrown;
import georegression.metric.UtilAngle;
import georegression.struct.EulerType;
import georegression.struct.point.Point3D_F64;
import georegression.struct.se.Se3_F64;
import georegression.transform.InvertibleTransformSequence;


public enum EnumWebCamType
{

  /** Worst choice. */
  DEFAULT(".*", 0, 565.1206663983819, 566.9618270851204, 0.0, 399.0897468215553, 300.79771450921527, 1024, 768, (in) -> {

    InvertibleTransformSequence<Se3_F64> t3d = new InvertibleTransformSequence<Se3_F64>();

    // assuming 0,0 as the same origin on the given plan.
    // Y+ along the longest dimension of the table,
    // X+ along the shortest dimension of the table. Z+ pointing to the sky.

    // An identity for "safety"
    // Se3_F64 t1i = new Se3_F64();

    // Rotate to bring Z up
    Se3_F64 t1r = new Se3_F64();
    // t1r.setTranslation(0, 1500, 1200);
    t1r.set(0, 0, 0, EulerType.XYZ, 0.0, UtilAngle.degreeToRadian(-90.0), 0.0);

    // Rotation to put X in the right dir
    Se3_F64 t2r = new Se3_F64();
    t2r.set(0, 0, 0, EulerType.XYZ, 0.0, 0.0, UtilAngle.degreeToRadian(180.0));

    // Translating to table origin
    Se3_F64 t3t = new Se3_F64();
    t3t.set(-20.0, -1500.0, -1000.0, EulerType.XYZ, 0.0, 0.0, 0.0);


    // t3d.addTransform(true, t1i);
    t3d.addTransform(true, t1r);
    t3d.addTransform(true, t2r);
    t3d.addTransform(true, t3t);

    Point3D_F64 p2 = new Point3D_F64();
    Se3_F64 t = new Se3_F64();
    t3d.computeTransform(t);
    p2 = t.transform(in, null);

    return p2;
  }),
  /** My ASUS webcam. */
  ASUS_UHC_WEBCAM_COARSE_SETUP("USB2.0 HD UVC WebCam: USB2.0 HD /dev/video[01]", 100, 565.1206663983819, 566.9618270851204, 0.0,
      399.0897468215553, 300.79771450921527, 1024, 768, (in) -> {
        InvertibleTransformSequence<Se3_F64> t3d = new InvertibleTransformSequence<Se3_F64>();

        // assuming 0,0 as the same origin on the given plan.
        // Y+ along the longest dimension of the table,
        // X+ along the shortest dimension of the table. Z+ pointing to the sky.

        // An identity for "safety"
        // Se3_F64 t1i = new Se3_F64();

        // Rotate to bring Z up
        Se3_F64 t1r = new Se3_F64();
        // t1r.setTranslation(0, 1500, 1200);
        t1r.set(0, 0, 0, EulerType.XYZ, 0.0, UtilAngle.degreeToRadian(-90.0), 0.0);

        // Rotation to put X in the right dir
        Se3_F64 t2r = new Se3_F64();
        t2r.set(0, 0, 0, EulerType.XYZ, 0.0, 0.0, UtilAngle.degreeToRadian(180.0));

        // Translating to table origin
        Se3_F64 t3t = new Se3_F64();
        t3t.set(-20.0, -1500.0, -1000.0, EulerType.XYZ, 0.0, 0.0, 0.0);


        // t3d.addTransform(true, t1i);
        t3d.addTransform(true, t1r);
        t3d.addTransform(true, t2r);
        t3d.addTransform(true, t3t);

        Point3D_F64 p2 = new Point3D_F64();
        Se3_F64 t = new Se3_F64();
        t3d.computeTransform(t);
        p2 = t.transform(in, null);

        return p2;
      }),
  /** Raspberry V4L video0. */
  RASPBERRY_CAM_VIDEO0("/dev/video0", 10, 565.1206663983819, 566.9618270851204, 0.0, 399.0897468215553, 300.79771450921527, 1024,
      768, (in) -> {



        InvertibleTransformSequence<Se3_F64> t3d = new InvertibleTransformSequence<Se3_F64>();

        // assuming 0,0 as the same origin on the given plan.
        // Y+ along the longest dimension of the table,
        // X+ along the shortest dimension of the table. Z+ pointing to the sky.

        // An identity for "safety"
        // Se3_F64 t1i = new Se3_F64();

        // Rotate to bring Z up
        Se3_F64 t1r = new Se3_F64();
        // t1r.setTranslation(0, 1500, 1200);
        t1r.set(0, 0, 0, EulerType.XYZ, 0.0, UtilAngle.degreeToRadian(-90.0), 0.0);

        // Rotation to put X in the right dir
        Se3_F64 t2r = new Se3_F64();
        t2r.set(0, 0, 0, EulerType.XYZ, 0.0, 0.0, UtilAngle.degreeToRadian(180.0));

        // Translating to table origin
        Se3_F64 t3t = new Se3_F64();
        t3t.set(-20.0, -1500.0, -1000.0, EulerType.XYZ, 0.0, 0.0, 0.0);


        // t3d.addTransform(true, t1i);
        t3d.addTransform(true, t1r);
        t3d.addTransform(true, t2r);
        t3d.addTransform(true, t3t);

        Point3D_F64 p2 = new Point3D_F64();
        Se3_F64 t = new Se3_F64();
        t3d.computeTransform(t);
        p2 = t.transform(in, null);

        return p2;
      }),
  /** Raspberry V4L video0. */
  RASPBERRY_CAM_VIDEO2("/dev/video2", 10, 565.1206663983819, 566.9618270851204, 0.0, 399.0897468215553, 300.79771450921527, 1024,
      768, (in) -> {



        InvertibleTransformSequence<Se3_F64> t3d = new InvertibleTransformSequence<Se3_F64>();

        // assuming 0,0 as the same origin on the given plan.
        // Y+ along the longest dimension of the table,
        // X+ along the shortest dimension of the table. Z+ pointing to the sky.

        // An identity for "safety"
        // Se3_F64 t1i = new Se3_F64();

        // Rotate to bring Z up
        Se3_F64 t1r = new Se3_F64();
        // t1r.setTranslation(0, 1500, 1200);
        t1r.set(0, 0, 0, EulerType.XYZ, 0.0, UtilAngle.degreeToRadian(-90.0), 0.0);

        // Rotation to put X in the right dir
        Se3_F64 t2r = new Se3_F64();
        t2r.set(0, 0, 0, EulerType.XYZ, 0.0, 0.0, UtilAngle.degreeToRadian(180.0));

        // Translating to table origin
        Se3_F64 t3t = new Se3_F64();
        t3t.set(-20.0, -1500.0, -1000.0, EulerType.XYZ, 0.0, 0.0, 0.0);


        // t3d.addTransform(true, t1i);
        t3d.addTransform(true, t1r);
        t3d.addTransform(true, t2r);
        t3d.addTransform(true, t3t);

        Point3D_F64 p2 = new Point3D_F64();
        Se3_F64 t = new Se3_F64();
        t3d.computeTransform(t);
        p2 = t.transform(in, null);

        return p2;
      }),
  /** Raspberry V4L video4. */
  RASPBERRY_CAM_VIDEO4("/dev/video4", 10, 565.1206663983819, 566.9618270851204, 0.0, 399.0897468215553, 300.79771450921527, 1280,
      720, (in) -> {



        InvertibleTransformSequence<Se3_F64> t3d = new InvertibleTransformSequence<Se3_F64>();

        // assuming 0,0 as the same origin on the given plan.
        // Y+ along the longest dimension of the table,
        // X+ along the shortest dimension of the table. Z+ pointing to the sky.

        // An identity for "safety"
        // Se3_F64 t1i = new Se3_F64();

        // Rotate to bring Z up
        Se3_F64 t1r = new Se3_F64();
        // t1r.setTranslation(0, 1500, 1200);
        t1r.set(0, 0, 0, EulerType.XYZ, 0.0, UtilAngle.degreeToRadian(-90.0), 0.0);

        // Rotation to put X in the right dir
        Se3_F64 t2r = new Se3_F64();
        t2r.set(0, 0, 0, EulerType.XYZ, 0.0, 0.0, UtilAngle.degreeToRadian(180.0));

        // Translating to table origin
        Se3_F64 t3t = new Se3_F64();
        t3t.set(-20.0, -1500.0, -1000.0, EulerType.XYZ, 0.0, 0.0, 0.0);


        // t3d.addTransform(true, t1i);
        t3d.addTransform(true, t1r);
        t3d.addTransform(true, t2r);
        t3d.addTransform(true, t3t);

        Point3D_F64 p2 = new Point3D_F64();
        Se3_F64 t = new Se3_F64();
        t3d.computeTransform(t);
        p2 = t.transform(in, null);

        return p2;
      })
  /** My ASUS webcam, more detailled */
  // ASUS_UHC_WEBCAM_FINE_SETUP("USB2.0 HD UVC WebCam: USB2.0 HD /dev/video[01]", 20,
  // "/asus-hd-uvc-webcam-13d3-56a2-v1a.yaml", (in) -> {
  // InvertibleTransformSequence<Se3_F64> t3d = new InvertibleTransformSequence<Se3_F64>();
  //
  // // assuming 0,0 as the same origin on the given plan.
  // // Y+ along the longest dimension of the table,
  // // X+ along the shortest dimension of the table. Z+ pointing to the sky.
  //
  // // An identity for "safety"
  // // Se3_F64 t1i = new Se3_F64();
  // // Translation on the tower
  // Se3_F64 t2t = new Se3_F64();
  // t2t.setTranslation(0, 1500, 1200);
  //
  // // Rotation looking horizon toward the central aruco.
  // Se3_F64 t3r = new Se3_F64();
  // t3r.set(0, 0, 0, EulerType.XYZ, 0.0, UtilAngle.degreeToRadian(90.0), 0.0);
  //
  // // t3d.addTransform(true, t1i);
  // t3d.addTransform(true, t2t);
  // t3d.addTransform(true, t3r);
  //
  // Point3D_F64 p2 = new Point3D_F64();
  // Se3_F64 t = new Se3_F64();
  // t3d.computeTransform(t);
  // p2 = t.transformReverse(in, null);
  //
  // return p2;
  // })
  ;

  /*
   * public static EnumWebCamType detect(Webcam webcam) {
   * 
   * TreeMap<Integer, EnumWebCamType> candidates = new TreeMap<Integer, EnumWebCamType>();
   * candidates.put(DEFAULT.recognitionPriority, DEFAULT);
   * 
   * for (EnumWebCamType knownCam : EnumWebCamType.values()) { if
   * (webcam.getName().matches(knownCam.getNameRecognitionPattern())) {
   * candidates.put(knownCam.getRecognitionPriority(), knownCam); } }
   * 
   * return candidates.lastEntry().getValue(); }
   */

  public static EnumWebCamType detect(String webcamName)
  {

    TreeMap<Integer, EnumWebCamType> candidates = new TreeMap<Integer, EnumWebCamType>();
    candidates.put(DEFAULT.recognitionPriority, DEFAULT);

    for (EnumWebCamType knownCam : EnumWebCamType.values())
    {
      if (webcamName.matches(knownCam.getNameRecognitionPattern()))
      {
        candidates.put(knownCam.getRecognitionPriority(), knownCam);
      }
    }

    return candidates.lastEntry().getValue();
  }

  /** CameraPinholeBrown. */
  CameraPinholeBrown cameraPinholeBrown;

  /** CameraPinholeBrown param. */
  private double cx;

  /** CameraPinholeBrown param. */
  private double cy;
  /** CameraPinholeBrown param. */
  private double fx;
  /** CameraPinholeBrown param. */
  private double fy;
  /** CameraPinholeBrown param. */
  private int height;
  /** Lens distortion. */
  private LensDistortionNarrowFOV lensDistortion;
  /** Pattern for recognition. */
  private String nameRecognitionPattern;
  /** Highest ranks will be take into account first. */
  private int recognitionPriority;

  /** CameraPinholeBrown param. */
  private double skew;


  /** CameraPinholeBrown param. */
  private int width;

  private Function<Point3D_F64, Point3D_F64> coordinateMapper;


  /**
   * @param nameRecognitionPattern
   * @param recognitionPriority Higher is more "specific" and shall be used first when matches.
   * @param fx
   * @param fy
   * @param skew
   * @param cx
   * @param cy
   * @param width
   * @param height
   */
  private EnumWebCamType(String nameRecognitionPattern, int recognitionPriority, double fx, double fy, double skew, double cx,
      double cy, int width, int height, Function<Point3D_F64, Point3D_F64> coordinateMapper)
  {
    this.nameRecognitionPattern = nameRecognitionPattern;
    this.recognitionPriority = recognitionPriority;
    this.fx = fx;
    this.fy = fy;
    this.skew = skew;
    this.cx = cx;
    this.cy = cy;
    this.width = width;
    this.height = height;
    this.coordinateMapper = coordinateMapper;

    this.cameraPinholeBrown = new CameraPinholeBrown(fx, fy, skew, cx, cy, width, height);
    this.lensDistortion = new LensDistortionBrown(this.cameraPinholeBrown);
  }

  private EnumWebCamType(String nameRecognitionPattern, int recognitionPriority, String resourceName,
      Function<Point3D_F64, Point3D_F64> coordinateMapper)
  {

    try
    {
      this.cameraPinholeBrown = CalibrationIO.load(EnumWebCamType.class.getClassLoader().getResource(resourceName));
    }
    catch (Exception e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    if (this.cameraPinholeBrown == null)
    {
      this.nameRecognitionPattern = nameRecognitionPattern;
      this.recognitionPriority = 1;
      this.fx = 565.1206663983819;
      this.fy = 566.9618270851204;
      this.skew = 0.0;
      this.cx = 399.0897468215553;
      this.cy = 300.79771450921527;
      this.width = 640;
      this.height = 480;
      this.coordinateMapper = coordinateMapper;


    }


    this.nameRecognitionPattern = nameRecognitionPattern;
    this.recognitionPriority = recognitionPriority;
    this.fx = this.cameraPinholeBrown.getFy();
    this.fy = this.cameraPinholeBrown.getFx();
    this.skew = this.cameraPinholeBrown.getSkew();
    this.cx = this.cameraPinholeBrown.getCx();
    this.cy = this.cameraPinholeBrown.getCy();
    this.width = this.cameraPinholeBrown.getWidth();
    this.height = this.cameraPinholeBrown.getHeight();



    this.lensDistortion = new LensDistortionBrown(this.cameraPinholeBrown);
  }



  /**
   * @return the cameraPinholeBrown
   */
  public CameraPinholeBrown getCameraPinholeBrown()
  {
    return this.cameraPinholeBrown;
  }


  /**
   * @return the cx
   */
  public double getCx()
  {
    return this.cx;
  }


  /**
   * @return the cy
   */
  public double getCy()
  {
    return this.cy;
  }


  /**
   * @return the fx
   */
  public double getFx()
  {
    return this.fx;
  }


  /**
   * @return the fy
   */
  public double getFy()
  {
    return this.fy;
  }


  /**
   * @return the height
   */
  public int getHeight()
  {
    return this.height;
  }


  /**
   * @return the lensDistortion
   */
  public LensDistortionNarrowFOV getLensDistortion()
  {
    return this.lensDistortion;
  }


  /**
   * @return the nameRecognitionPattern
   */
  public String getNameRecognitionPattern()
  {
    return this.nameRecognitionPattern;
  }


  /**
   * @return the recognitionPriority
   */
  public int getRecognitionPriority()
  {
    return this.recognitionPriority;
  }


  /**
   * @return the skew
   */
  public double getSkew()
  {
    return this.skew;
  }

  /**
   * @return the width
   */
  public int getWidth()
  {
    return this.width;
  }

  /**
   * @return the coordinateMapper
   */
  public Function<Point3D_F64, Point3D_F64> getCoordinateMapper()
  {
    return this.coordinateMapper;
  }



}

