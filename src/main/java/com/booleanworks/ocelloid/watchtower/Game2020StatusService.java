/**
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import com.booleanworks.ocelloid.watchtower.messages.EuroBot2020StatusMessage;
import com.booleanworks.ocelloid.watchtower.messages.FiducialDetectionOccurence;
import com.google.common.collect.ImmutableList;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import boofcv.alg.color.ColorHsv;
import boofcv.alg.filter.binary.BinaryImageOps;
import boofcv.alg.filter.binary.Contour;
import boofcv.alg.filter.binary.ThresholdImageOps;
import boofcv.alg.filter.blur.BlurImageOps;
import boofcv.alg.misc.ImageStatistics;
import boofcv.alg.shapes.FitData;
import boofcv.alg.shapes.ShapeFittingOps;
import boofcv.core.image.ConvertImage;
import boofcv.gui.feature.VisualizeShapes;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.ConnectRule;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.ImageType;
import boofcv.struct.image.Planar;
import georegression.metric.UtilAngle;
import georegression.struct.curve.EllipseRotated_F64;
import georegression.struct.point.Point2D_F64;

/**
 * @author vortigern
 *
 */
public class Game2020StatusService extends Thread implements BaseService
{


  public class BuoyCode
  {

    /** The buoy code. */
    final public Short buoyCode;

    final public List<Color> colors;

    /**
     * @param buoyCode
     * @param colors
     */
    public BuoyCode(Short buoyCode, List<Color> colors)
    {
      super();
      this.buoyCode = buoyCode;

      if (Objects.isNull(colors))
      {
        throw new IllegalArgumentException("Null list !");
      }

      this.colors = ImmutableList.<Color>copyOf(colors);
    }


    public BuoyCode(Short buoyCode, Color... colors)
    {
      super();
      this.buoyCode = buoyCode;
      this.colors = ImmutableList.<Color>copyOf(colors);
    }

    boolean matches(List<Color> otherColors)
    {
      if (Objects.isNull(otherColors) || (otherColors.size() != this.colors.size()))
      {
        return false;
      }

      for (int idx = 0; idx < this.colors.size(); idx++)
      {
        if (!this.colors.get(idx).equals(otherColors.get(idx)))
        {
          return false;
        }
      }

      return true;

    }


    /**
     * @return the buoyCode
     */
    public Short getBuoyCode()
    {
      return this.buoyCode;
    }


    /**
     * @return the colors
     */
    public List<Color> getColors()
    {
      return this.colors;
    }



  }


  public Random rnd = new Random();

  public final static EnumWebCamType BLUE_SIDE_CAMERA = EnumWebCamType.RASPBERRY_CAM_VIDEO2;
  public final static EnumWebCamType CENTER_CAMERA = EnumWebCamType.RASPBERRY_CAM_VIDEO4;
  public final static EnumWebCamType YELLOW_SIDE_CAMERA = EnumWebCamType.RASPBERRY_CAM_VIDEO0;

  public final static String[] BLUE_ROBOT_TAGS = new String[]
  {"1", "2", "3", "4", "5"};
  public final static String[] YELLOW_ROBOT_TAGS = new String[]
  {"6", "7", "8", "9", "10"};

  public final static String[] BLUE_TEAM_TAGS = new String[]
  {"51", "52", "53", "54", "55"};
  public final static String[] YELLOW_TEAM_TAGS = new String[]
  {"71", "72", "73", "74", "75"};

  // create gpio controller
  private final static GpioController gpio = GpioFactory.getInstance();

  /** Sensor pin at the blue side (physical position, not result !) */
  public final static GpioPinDigitalInput sensorPinAtTheYellowSide = gpio.provisionDigitalInputPin(RaspiPin.GPIO_24);
  /** Sensor pin at the yellow side (physical position, not result !) */
  public final static GpioPinDigitalInput sensorPinAtTheBlueSide = gpio.provisionDigitalInputPin(RaspiPin.GPIO_27);

  /** Pin(s) used to feed the sensor. */
  public final static GpioPinDigitalOutput sensorFeedAtTheYellowSide =
      gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, PinState.HIGH);
  /** Pin(s) used to feed the sensor. */
  public final static GpioPinDigitalOutput sensorFeedAtTheBlueSide =
      gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28, PinState.HIGH);
  /** Pin(s) used to feed the sensor. */
  public final static GpioPinDigitalOutput sensorGroundAtTheBlueSide =
      gpio.provisionDigitalOutputPin(RaspiPin.GPIO_29, PinState.LOW);


  public final static GpioPinPwmOutput gpioPinLedRedChannel = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_14, 0);
  public final static GpioPinPwmOutput gpioPinLedGreenChannel = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_13, 0);
  public final static GpioPinPwmOutput gpioPinLedBlueChannel = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_12, 0);

  // public final static GpioPinDigitalOutput gpioPinLedRedChannel =
  // gpio.provisionDigitalOutputPin(RaspiPin.GPIO_14);
  // public final static GpioPinDigitalOutput gpioPinLedGreenChannel =
  // gpio.provisionDigitalOutputPin(RaspiPin.GPIO_13);
  // public final static GpioPinDigitalOutput gpioPinLedBlueChannel =
  // gpio.provisionDigitalOutputPin(RaspiPin.GPIO_12);

  private final static ScheduledThreadPoolExecutor buoyDetectionExecutionPool = new ScheduledThreadPoolExecutor(1);

  private final static ScheduledThreadPoolExecutor windsockDetectionExecutionPool = new ScheduledThreadPoolExecutor(1);

  private final static ScheduledThreadPoolExecutor weathervaneDetectionExecutionPool = new ScheduledThreadPoolExecutor(1);

  private final static ScheduledThreadPoolExecutor ledCodeExecutionPool = new ScheduledThreadPoolExecutor(1);

  public static final int CYCLE_DELAY = Cfg.getInt("c1.services.gamestatus.cycle", 600);



  public static int cycleCounter = 0;

  public TreeMap<Long, Double> yellowSideMeanLumi = new TreeMap<>((a, b) -> (int) (a - b));
  public TreeMap<Long, Double> blueSideMeanLumi = new TreeMap<>((a, b) -> (int) (a - b));



  /** In construction status */
  private EuroBot2020StatusMessage nextStatus = new EuroBot2020StatusMessage();

  /** Published current status (shall not be modified) */
  private EuroBot2020StatusMessage currentStatus = new EuroBot2020StatusMessage();

  /** Previous status (shall npot be modified) */
  private EuroBot2020StatusMessage previousStatus = new EuroBot2020StatusMessage();

  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(Game2020StatusService.class.getName());

  /** Lighter log for game info */
  private static final Logger GAME_LOG = Logger.getLogger("GAME_LOG");


  /** Thread dedicated to long processing. */
  private Thread groundedBuoysThread = null;

  /** Thread dedicated to long processing. */
  private Thread weathervaneThread = null;

  private Thread robotLocatorThread = null;


  long gameLoopStartup = 0l;

  long gameOfficialT0 = 0l;

  private EnumServiceStatus serviceStatus;



  @Override
  public boolean bootstrap()
  {


    LOG.info("Starting Game2020StatusService...");
    if (!this.isAlive())
    {
      this.start();
    }



    return true;
  }


  /**
   * return the serviceStatus.
   *
   * @return the serviceStatus.
   */
  @Override
  public EnumServiceStatus getServiceStatus()
  {

    if (this.serviceStatus == null)
    {
      this.serviceStatus = EnumServiceStatus.UNKNOWN;
    }

    return this.serviceStatus;
  }

  @Override
  public boolean init()
  {
    if (this.getServiceStatus() != EnumServiceStatus.UNKNOWN)
    {
      return false;
    }

    this.serviceStatus = EnumServiceStatus.INITIALIZING;
    this.setName(Game2020StatusService.class.getName());

    try
    {
      FileHandler fileHandler = new FileHandler("game-%u.log", 10 * 1014 * 1024, 10, true);
      fileHandler.setLevel(Level.FINE);
      fileHandler.setFormatter(new SimpleFormatter());

      GAME_LOG.addHandler(fileHandler);

    }
    catch (SecurityException | IOException e1)
    {
      e1.printStackTrace();
    }



    try
    {

      Color grey1 = new Color(100, 100, 100);
      Color red1 = new Color(100, 0, 0);
      Color green1 = new Color(0, 0, 100);

      // Ws2812ThruSpi.getInstance(SpiChannel.CS0).setColorsAndSend(grey1, red1, Color.BLACK,
      // Color.BLACK, Color.BLACK, Color.BLACK, green1, grey1);

      // this.issueLedCode(grey1, red1, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, green1,
      // grey1);

      this.issueLedCode(Color.WHITE, Color.WHITE, Color.BLUE, Color.BLUE);
    }
    catch (Exception e)
    {
      LOG.warning("[Game2020StatusService] Failed to use ws2812 led strip at init()");
      e.printStackTrace();
    }

    this.serviceStatus = EnumServiceStatus.READY;
    return true;
  }



  @Override
  public void run()
  {

    this.gameLoopStartup = System.currentTimeMillis();

    this.serviceStatus = EnumServiceStatus.RUNNING;

    while (this.getServiceStatus() != EnumServiceStatus.STOPPING && this.getServiceStatus() != EnumServiceStatus.FULL_FAILURE)
    {

      try
      {

        int errorCounter = 0;
        int warningCounter = 0;
        cycleCounter++;

        long t0 = System.currentTimeMillis();



        CopyOnWriteArrayList<FiducialDetectionOccurence> retainedArucos = new CopyOnWriteArrayList<FiducialDetectionOccurence>();

        try
        {


          retainedArucos = this.refineArucoDetection();


        }
        catch (Exception e)
        {
          LOG.warning("[Game2020StatusService] Error while refining arucos... " + e.getMessage());
          errorCounter++;
          e.printStackTrace();
        }

        long t1 = System.currentTimeMillis();

        try
        {
          this.detectTableSidePosition();

        }
        catch (Exception e)
        {
          LOG.warning("[Game2020StatusService] Error while detecting watchower position... " + e.getMessage());
          errorCounter++;
          e.printStackTrace();
        }

        long t2 = System.currentTimeMillis();

        try
        {


          if (cycleCounter % Cfg.getInt("c1.services.gamestatus.locaterobots.cyclemodulo", 5) == 0)
          {
            this.locateRobots(retainedArucos);
          }
          else
          {
            LOG.info("Bypassing this.locateRobots(retainedArucos)");
          }


        }
        catch (Exception e)
        {
          LOG.warning("[Game2020StatusService] Error while locating robots... " + e.getMessage());
          errorCounter++;
          e.printStackTrace();
        }

        long t3 = System.currentTimeMillis();

        try
        {

          if (this.weathervaneThread == null || !this.weathervaneThread.isAlive())
          {
            LOG.info("[Game2020StatusService] Forking in this.detectWheelPosition()... ");
            this.weathervaneThread = new Thread(() -> this.detectWheelPosition());
            this.weathervaneThread.setName("detectWheelPosition");
            this.weathervaneThread.setPriority(Cfg.getInt("c1.services.gamestatus.wheeldetection.priority", 10));
            this.weathervaneThread.start();
          }
          else
          {
            LOG.warning("[Game2020StatusService] this.detectWheelPosition() still running... ");

          }

        }
        catch (Exception e)
        {
          LOG.warning("[Game2020StatusService] Error while detecting wheel... " + e.getMessage());
          errorCounter++;
          e.printStackTrace();
        }

        long t4 = System.currentTimeMillis();

        try
        {

          if (this.groundedBuoysThread == null || !this.groundedBuoysThread.isAlive())
          {
            LOG.info("[Game2020StatusService] Forking in this.detectGroundedBuoys()... ");
            this.groundedBuoysThread = new Thread(() -> this.detectGroundedBuoys());
            this.groundedBuoysThread.setName("detectGroundedBuoys");
            this.groundedBuoysThread.setPriority(Cfg.getInt("c1.services.gamestatus.groundedbuoys.priority", 1));
            this.groundedBuoysThread.start();
          }
          else
          {
            LOG.warning("[Game2020StatusService] this.detectGroundedBuoys() still running... ");
          }

        }
        catch (Exception e)
        {
          LOG.warning("[Game2020StatusService] Error while detecting grounded buoys... " + e.getMessage());
          errorCounter++;
          e.printStackTrace();
        }


        long t5 = System.currentTimeMillis();

        long t0t5duration = t5 - t0;


        long t0t1duration = t1 - t0;
        long t1t2duration = t2 - t1;
        long t2t3duration = t3 - t2;
        long t3t4duration = t4 - t3;
        long t4t5duration = t5 - t4;


        if (Cfg.getBoolean("c1.services.root.debug4", false))
        {
          this.currentStatus.setWheelPosition(Cfg.getByte("c1.services.gamestatus.debug5.fakeweathervane", (byte) 1));
          this.nextStatus.setWheelPosition(Cfg.getByte("c1.services.gamestatus.debug5.fakeweathervane", (byte) 1));

          this.currentStatus.setBuoysCode(Cfg.getShort("c1.services.gamestatus.debug5.fakegroundedbuoys", (short) 1));
          this.nextStatus.setBuoysCode(Cfg.getShort("c1.services.gamestatus.debug5.fakegroundedbuoys", (short) 1));

        }

        // shifting statuses...
        this.previousStatus = this.currentStatus;
        this.currentStatus = this.nextStatus;
        // this.nextStatus = new EuroBot2020StatusMessage();

        if (this.nextStatus == null)
        {
          this.nextStatus = new EuroBot2020StatusMessage();
          this.currentStatus = new EuroBot2020StatusMessage();
        }
        else
        {
          try
          {
            this.nextStatus = (EuroBot2020StatusMessage) this.currentStatus.clone();
          }
          catch (Exception e)
          {
            LOG.warning("Error while cloning message ! issuing a new 'nextStatus'... " + e.getMessage());
            this.nextStatus = new EuroBot2020StatusMessage();
          }

        }


        LOG.info("t0t1duration = " + t0t1duration);
        LOG.info("t1t2duration = " + t1t2duration);
        LOG.info("t2t3duration = " + t2t3duration);
        LOG.info("t3t4duration = " + t3t4duration);
        LOG.info("t4t5duration = " + t4t5duration);
        LOG.info("t0t5duration = " + t0t5duration);

        LOG.info("this.previousStatus = " + this.previousStatus.toString());
        LOG.info("this.currentStatus = " + this.currentStatus.toString());



        long cycleSleep = CYCLE_DELAY - t0t5duration;
        if (cycleSleep < 20)
        {
          LOG.warning("[Game2020StatusService]  cycleSleep = " + cycleSleep);

        }
        else
        {
          Thread.sleep(cycleSleep);
        }



        Color healthCode = Color.RED;
        Color warningCode = Color.RED;
        Color sideCode = Color.RED;
        Color buoyCode1 = Color.WHITE;
        Color buoyCode2 = Color.WHITE;
        Color buoyCode3 = Color.WHITE;

        if (cycleSleep < 20)
        {
          healthCode = Color.ORANGE;
        }
        else
        {
          healthCode = Color.GREEN;
        }

        if (errorCounter > 0)
        {
          warningCode = Color.RED;
        }
        else if (warningCounter > 0)
        {
          warningCode = Color.ORANGE;
        }
        else
        {
          warningCode = Color.GREEN;
        }

        switch (this.getPreviousStatus().getWatchtowerSide())
        {
          case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_UNKNOWN:
            sideCode = Color.WHITE;
            break;
          case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_YELLOW:
            sideCode = Color.YELLOW;
            break;
          case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_BLUE:
            sideCode = Color.BLUE;
            break;
          default:
            sideCode = Color.WHITE;
            break;
        }

        this.issueLedCode(sideCode);

        for (Handler cHandler : LOG.getHandlers())
        {
          cHandler.flush();

        }

        this.setPriority(Cfg.getInt("c1.services.gamestatus.priority", 7));



      }
      catch (Exception e)
      {
        LOG.warning("[Game2020StatusService] Error in the main loop... " + e.getMessage());

        e.printStackTrace();
      }



    }



  }


  /**
   * @param retainedArucos
   */
  private CopyOnWriteArrayList<FiducialDetectionOccurence> refineArucoDetection()
  {
    CopyOnWriteArrayList<FiducialDetectionOccurence> retainedArucos = new CopyOnWriteArrayList<FiducialDetectionOccurence>();

    CopyOnWriteArrayList<String> arucoIds = new CopyOnWriteArrayList<String>();

    CopyOnWriteArrayList<FiducialDetectionOccurence> originalArucoList =
        WatchTowerService.getInstance().getArucoDetectionService().getFiducialOccurences();

    // Building an id catalog
    originalArucoList.stream().forEach((cFidOcc) -> {

      if (!arucoIds.contains(cFidOcc.getDataAsString()))
      {
        arucoIds.add(cFidOcc.getDataAsString());
      }

    });

    // ---------------------
    // Retaining best Aruco for each id
    arucoIds.forEach((id) -> {

      // Ignore id with an already retained tag.
      long arucoWithThisIdCount = retainedArucos.stream().filter(a -> id.equalsIgnoreCase(a.getDataAsString())).count();
      if (arucoWithThisIdCount > 0)
      {
        return;
      }

      CopyOnWriteArrayList<FiducialDetectionOccurence> similarAruco = new CopyOnWriteArrayList<FiducialDetectionOccurence>();

      // Retrieving similar Aruco
      originalArucoList.stream().filter((a) -> a.getDataAsString().contentEquals(id)).forEach((a) -> {
        similarAruco.add(a);
      });



      // ---------------------------------
      // First, stability computation before webcam filtering

      // Reminder: min stability is better (grrrr)
      double meanStability = 0.0;
      double minStability = Double.MAX_VALUE;
      double maxStability = Double.MIN_VALUE;
      FiducialDetectionOccurence mostStableAruco = originalArucoList.get(0);

      for (FiducialDetectionOccurence a : similarAruco)
      {
        meanStability += a.getStability().location;
        maxStability = Math.max(maxStability, a.getStability().location);


        if (a.getStability().location < minStability)
        {
          mostStableAruco = a;
        }
        minStability = Math.min(minStability, a.getStability().location);

      }

      // Removing Aruco from another cam thant the most stable one
      final String webcamWithTheBestAruco = mostStableAruco.getWebcamName();
      // Removing Aruco from other cams before mean point processing.
      similarAruco.removeIf(a -> !a.getWebcamName().equalsIgnoreCase(webcamWithTheBestAruco));

      // ---------------------------------
      // After, mean point deviance filtering and new stability computation

      double meanX = 0.0;
      double meanY = 0.0;

      meanStability = 0.0;
      minStability = Double.MAX_VALUE;
      maxStability = Double.MIN_VALUE;

      for (FiducialDetectionOccurence a : similarAruco)
      {
        meanX += a.getCenter().getX();
        meanY += a.getCenter().getY();
        meanStability += a.getStability().location;
        maxStability = Math.max(maxStability, a.getStability().location);

        if (a.getStability().location < minStability)
        {
          mostStableAruco = a;
        }

        minStability = Math.min(minStability, a.getStability().location);

      }

      meanX = meanX / similarAruco.size();
      meanY = meanY / similarAruco.size();
      meanStability = meanStability / similarAruco.size();

      Point2D_F64 meanCenter = new Point2D_F64(meanX, meanY);

      double meanDistToMeanCenter = 0.0;
      double meanDistToMostStableCenter = 0.0;

      for (FiducialDetectionOccurence a : similarAruco)
      {
        meanDistToMeanCenter += a.getCenter().distance(meanCenter);
        meanDistToMostStableCenter += a.getCenter().distance(mostStableAruco.getCenter());
      }

      meanDistToMeanCenter = meanDistToMeanCenter / similarAruco.size();
      meanDistToMostStableCenter = meanDistToMostStableCenter / similarAruco.size();



      if (similarAruco.size() > 1 && maxStability < 40.0 && meanDistToMostStableCenter < 40)
      {

        EnumWebCamType wt = EnumWebCamType.detect(mostStableAruco.getWebcamName());
        mostStableAruco.setProjectedLocation(wt.getCoordinateMapper().apply(mostStableAruco.getOriginalLocation()));

        LOG.fine("[Game2020StatusService] New retained Aruco=" + mostStableAruco.toStringAlt1());

        retainedArucos.add(mostStableAruco);
      }
      else
      {
        LOG.fine("[Game2020StatusService] Discarded on final filtering Aruco=" + mostStableAruco.toStringAlt1());

      }


    });

    return retainedArucos;
  }

  @Override
  public boolean tearDown()
  {
    this.serviceStatus = EnumServiceStatus.STOPPING;

    try
    {
      Thread.sleep(CYCLE_DELAY);
    }
    catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    this.serviceStatus = EnumServiceStatus.STOPPED;

    return true;
  }

  /**
  *
  */
  public Game2020StatusService()
  {
    super();
    this.serviceStatus = EnumServiceStatus.UNKNOWN;
  }

  /**
   * Selectively displays only pixels which have a similar hue and saturation values to what is
   * provided. This is intended to be a simple example of color based segmentation. Color based
   * segmentation can be done in RGB color, but is more problematic due to it not being intensity
   * invariant. More robust techniques can use Gaussian models instead of a uniform distribution, as
   * is done below.
   *
   * @return
   */
  public static BufferedImage thresholdHue(BufferedImage image, float hue, float saturation, float distance, int radius)
  {
    Planar<GrayF32> input = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);



    Planar<GrayF32> blurredInput = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);

    BlurImageOps.median(input, blurredInput, radius, null);

    Planar<GrayF32> hsv = blurredInput.createSameShape();

    // Convert into HSV
    ColorHsv.rgbToHsv(blurredInput, hsv);

    // Euclidean distance squared threshold for deciding which pixels are members of the selected
    // set
    float maxDist2 = distance * distance;

    // Extract hue and saturation bands which are independent of intensity
    GrayF32 H = hsv.getBand(0);
    GrayF32 S = hsv.getBand(1);

    // Adjust the relative importance of Hue and Saturation.
    // Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
    float adjustUnits = (float) (Math.PI / 2.0);

    int whiteColor = Color.WHITE.getRGB();
    // int blackColor = Color.BLACK.getRGB();


    // step through each pixel and mark how close it is to the selected color
    BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
    for (int y = 0; y < hsv.height; y++)
    {
      for (int x = 0; x < hsv.width; x++)
      {
        // Hue is an angle in radians, so simple subtraction doesn't work
        float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
        float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;

        // this distance measure is a bit naive, but good enough for to demonstrate the concept
        float dist2 = dh * dh + ds * ds;
        if (dist2 <= maxDist2)
        {
          // output.setRGB(x, y, image.getRGB(x, y));
          output.setRGB(x, y, whiteColor);
        }


      }
    }

    return output;

  }

  public static ArrayList<EllipseRotated_F64> getEllipsesFromImage(BufferedImage image)
  {
    GrayF32 input = ConvertBufferedImage.convertFromSingle(image, null, GrayF32.class);

    GrayU8 binary = new GrayU8(input.width, input.height);

    // the mean pixel value is often a reasonable threshold when creating a binary image
    double mean = ImageStatistics.mean(input);

    // create a binary image by thresholding
    ThresholdImageOps.threshold(input, binary, (float) mean, false);

    // reduce noise with some filtering
    GrayU8 filtered = BinaryImageOps.erode8(binary, 1, null);
    filtered = BinaryImageOps.dilate8(filtered, 1, null);

    // Find only external contours around each shape. Much faster than if internal + labeled images
    // are considered.
    List<Contour> contours = BinaryImageOps.contourExternal(filtered, ConnectRule.FOUR);

    ArrayList<EllipseRotated_F64> ellipses = new ArrayList<EllipseRotated_F64>();

    for (Contour c : contours)
    {
      FitData<EllipseRotated_F64> ellipse = ShapeFittingOps.fitEllipse_I32(c.external, 0, false, null);


      ellipses.add(ellipse.shape);
    }

    return ellipses;
  }


  /**
   * @return the nextStatus
   */
  public EuroBot2020StatusMessage getNextStatus()
  {
    return this.nextStatus;
  }


  /**
   * @return the currentStatus
   */
  public EuroBot2020StatusMessage getCurrentStatus()
  {
    return this.currentStatus;
  }


  /**
   * @return the previousStatus
   */
  public EuroBot2020StatusMessage getPreviousStatus()
  {
    return this.previousStatus;
  }


  /**
   * @param nextStatus the nextStatus to set
   */
  public void setNextStatus(EuroBot2020StatusMessage nextStatus)
  {
    this.nextStatus = nextStatus;
  }


  /**
   * @param currentStatus the currentStatus to set
   */
  public void setCurrentStatus(EuroBot2020StatusMessage currentStatus)
  {
    this.currentStatus = currentStatus;
  }


  /**
   * @param previousStatus the previousStatus to set
   */
  public void setPreviousStatus(EuroBot2020StatusMessage previousStatus)
  {
    this.previousStatus = previousStatus;
  }

  public void detectTableSidePosition()
  {

    Game2020StatusService.sensorPinAtTheBlueSide.setPullResistance(PinPullResistance.OFF);
    Game2020StatusService.sensorPinAtTheYellowSide.setPullResistance(PinPullResistance.OFF);

    Game2020StatusService.sensorFeedAtTheBlueSide.high();
    Game2020StatusService.sensorFeedAtTheYellowSide.high();

    Game2020StatusService.sensorGroundAtTheBlueSide.low();


    boolean contactAtTheBlueSide = Game2020StatusService.sensorPinAtTheBlueSide.isHigh();
    boolean contactAtTheYellowSide = Game2020StatusService.sensorPinAtTheYellowSide.isHigh();

    if (contactAtTheBlueSide && !contactAtTheYellowSide)
    {
      LOG.info("We are at the yellow side !!");
      this.getNextStatus().setWatchtowerSide(EuroBot2020StatusMessage.WHATCHTOWER_SIDE_YELLOW);
    }
    else if (!contactAtTheBlueSide && contactAtTheYellowSide)
    {
      LOG.info("We are at the blue side !!");
      this.getNextStatus().setWatchtowerSide(EuroBot2020StatusMessage.WHATCHTOWER_SIDE_BLUE);
    }
    else if (!contactAtTheBlueSide && !contactAtTheYellowSide)
    {
      LOG.warning("We are not sure, probaly not on the table !!");
      this.getNextStatus().setWatchtowerSide(EuroBot2020StatusMessage.WHATCHTOWER_SIDE_UNKNOWN);
    }
    else
    {
      LOG.warning("We are not sure, somebody is probably playing with switches !!");
      this.getNextStatus().setWatchtowerSide(EuroBot2020StatusMessage.WHATCHTOWER_SIDE_UNKNOWN);
    }
  }

  public void locateRobots(CopyOnWriteArrayList<FiducialDetectionOccurence> arucos)
  {
    CopyOnWriteArrayList<FiducialDetectionOccurence> blueBots = new CopyOnWriteArrayList<FiducialDetectionOccurence>();
    CopyOnWriteArrayList<FiducialDetectionOccurence> yellowBots = new CopyOnWriteArrayList<FiducialDetectionOccurence>();

    for (String cId : Game2020StatusService.BLUE_ROBOT_TAGS)
    {
      Optional<FiducialDetectionOccurence> blueBot =
          arucos.stream().filter(a -> a.getDataAsString().contentEquals(cId)).findFirst();

      if (blueBot.isPresent())
      {
        blueBots.add(blueBot.get());
      }
    }

    for (String cId : Game2020StatusService.YELLOW_ROBOT_TAGS)
    {
      Optional<FiducialDetectionOccurence> yellowBot =
          arucos.stream().filter(a -> a.getDataAsString().contentEquals(cId)).findFirst();

      if (yellowBot.isPresent())
      {
        yellowBots.add(yellowBot.get());
      }
    }

    FiducialDetectionOccurence firstBlue = blueBots.size() >= 1 ? blueBots.get(0) : null;
    FiducialDetectionOccurence secondBlue = blueBots.size() >= 2 ? blueBots.get(1) : null;

    FiducialDetectionOccurence firstYellow = yellowBots.size() >= 1 ? yellowBots.get(0) : null;
    FiducialDetectionOccurence secondYellow = yellowBots.size() >= 2 ? yellowBots.get(1) : null;

    EuroBot2020StatusMessage n = this.getNextStatus();

    switch (n.getWatchtowerSide())
    {
      case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_BLUE:
        if (firstBlue != null)
        {
          n.setRobot1_Aruco(Short.parseShort(firstBlue.getDataAsString()));
          n.setRobot1_color(EuroBot2020StatusMessage.ROBOT_COLOR_BLUE);
          n.setRobot1_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FRIEND);
          n.setRobot1_position_error((short) 50);
          n.setRobot1_position_x((short) firstBlue.getProjectedLocation().getX());
          n.setRobot1_position_y((short) firstBlue.getProjectedLocation().getY());
          n.setRobot1_positionDelta_error((short) 0);
          n.setRobot1_positionDelta_x((short) 0);
          n.setRobot1_positionDelta_y((short) 0);
          n.setRobot1_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (secondBlue != null)
        {
          n.setRobot2_Aruco(Short.parseShort(secondBlue.getDataAsString()));
          n.setRobot2_color(EuroBot2020StatusMessage.ROBOT_COLOR_BLUE);
          n.setRobot2_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FRIEND);
          n.setRobot2_position_error((short) 50);
          n.setRobot2_position_x((short) secondBlue.getProjectedLocation().getX());
          n.setRobot2_position_y((short) secondBlue.getProjectedLocation().getY());
          n.setRobot2_positionDelta_error((short) 0);
          n.setRobot2_positionDelta_x((short) 0);
          n.setRobot2_positionDelta_y((short) 0);
          n.setRobot2_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (firstYellow != null)
        {
          n.setRobot3_Aruco(Short.parseShort(firstYellow.getDataAsString()));
          n.setRobot3_color(EuroBot2020StatusMessage.ROBOT_COLOR_YELLOW);
          n.setRobot3_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FOE);
          n.setRobot3_position_error((short) 50);
          n.setRobot3_position_x((short) firstYellow.getProjectedLocation().getX());
          n.setRobot3_position_y((short) firstYellow.getProjectedLocation().getY());
          n.setRobot3_positionDelta_error((short) 0);
          n.setRobot3_positionDelta_x((short) 0);
          n.setRobot3_positionDelta_y((short) 0);
          n.setRobot3_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (secondYellow != null)
        {
          n.setRobot4_Aruco(Short.parseShort(secondYellow.getDataAsString()));
          n.setRobot4_color(EuroBot2020StatusMessage.ROBOT_COLOR_YELLOW);
          n.setRobot4_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FOE);
          n.setRobot4_position_error((short) 50);
          n.setRobot4_position_x((short) secondYellow.getProjectedLocation().getX());
          n.setRobot4_position_y((short) secondYellow.getProjectedLocation().getY());
          n.setRobot4_positionDelta_error((short) 0);
          n.setRobot4_positionDelta_x((short) 0);
          n.setRobot4_positionDelta_y((short) 0);
          n.setRobot4_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        break;
      case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_YELLOW:
        if (firstYellow != null)
        {
          n.setRobot1_Aruco(Short.parseShort(firstYellow.getDataAsString()));
          n.setRobot1_color(EuroBot2020StatusMessage.ROBOT_COLOR_YELLOW);
          n.setRobot1_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FRIEND);
          n.setRobot1_position_error((short) 50);
          n.setRobot1_position_x((short) firstYellow.getProjectedLocation().getX());
          n.setRobot1_position_y((short) firstYellow.getProjectedLocation().getY());
          n.setRobot1_positionDelta_error((short) 0);
          n.setRobot1_positionDelta_x((short) 0);
          n.setRobot1_positionDelta_y((short) 0);
          n.setRobot1_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (secondYellow != null)
        {
          n.setRobot2_Aruco(Short.parseShort(secondYellow.getDataAsString()));
          n.setRobot2_color(EuroBot2020StatusMessage.ROBOT_COLOR_YELLOW);
          n.setRobot2_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FRIEND);
          n.setRobot2_position_error((short) 50);
          n.setRobot2_position_x((short) secondYellow.getProjectedLocation().getX());
          n.setRobot2_position_y((short) secondYellow.getProjectedLocation().getY());
          n.setRobot2_positionDelta_error((short) 0);
          n.setRobot2_positionDelta_x((short) 0);
          n.setRobot2_positionDelta_y((short) 0);
          n.setRobot2_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (firstBlue != null)
        {
          n.setRobot3_Aruco(Short.parseShort(firstBlue.getDataAsString()));
          n.setRobot3_color(EuroBot2020StatusMessage.ROBOT_COLOR_BLUE);
          n.setRobot3_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FOE);
          n.setRobot3_position_error((short) 50);
          n.setRobot3_position_x((short) firstBlue.getProjectedLocation().getX());
          n.setRobot3_position_y((short) firstBlue.getProjectedLocation().getY());
          n.setRobot3_positionDelta_error((short) 0);
          n.setRobot3_positionDelta_x((short) 0);
          n.setRobot3_positionDelta_y((short) 0);
          n.setRobot3_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (secondBlue != null)
        {
          n.setRobot4_Aruco(Short.parseShort(secondBlue.getDataAsString()));
          n.setRobot4_color(EuroBot2020StatusMessage.ROBOT_COLOR_BLUE);
          n.setRobot4_faction(EuroBot2020StatusMessage.ROBOT_FACTION_FOE);
          n.setRobot4_position_error((short) 50);
          n.setRobot4_position_x((short) secondBlue.getProjectedLocation().getX());
          n.setRobot4_position_y((short) secondBlue.getProjectedLocation().getY());
          n.setRobot4_positionDelta_error((short) 0);
          n.setRobot4_positionDelta_x((short) 0);
          n.setRobot4_positionDelta_y((short) 0);
          n.setRobot4_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        break;
      default:
        if (firstYellow != null)
        {
          n.setRobot1_Aruco(Short.parseShort(firstYellow.getDataAsString()));
          n.setRobot1_color(EuroBot2020StatusMessage.ROBOT_COLOR_YELLOW);
          n.setRobot1_faction(EuroBot2020StatusMessage.ROBOT_FACTION_UNKNOWN);
          n.setRobot1_position_error((short) 50);
          n.setRobot1_position_x((short) firstYellow.getProjectedLocation().getX());
          n.setRobot1_position_y((short) firstYellow.getProjectedLocation().getY());
          n.setRobot1_positionDelta_error((short) 0);
          n.setRobot1_positionDelta_x((short) 0);
          n.setRobot1_positionDelta_y((short) 0);
          n.setRobot1_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (secondYellow != null)
        {
          n.setRobot2_Aruco(Short.parseShort(secondYellow.getDataAsString()));
          n.setRobot2_color(EuroBot2020StatusMessage.ROBOT_COLOR_YELLOW);
          n.setRobot2_faction(EuroBot2020StatusMessage.ROBOT_FACTION_UNKNOWN);
          n.setRobot2_position_error((short) 50);
          n.setRobot2_position_x((short) secondYellow.getProjectedLocation().getX());
          n.setRobot2_position_y((short) secondYellow.getProjectedLocation().getY());
          n.setRobot2_positionDelta_error((short) 0);
          n.setRobot2_positionDelta_x((short) 0);
          n.setRobot2_positionDelta_y((short) 0);
          n.setRobot2_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (firstBlue != null)
        {
          n.setRobot3_Aruco(Short.parseShort(firstBlue.getDataAsString()));
          n.setRobot3_color(EuroBot2020StatusMessage.ROBOT_COLOR_BLUE);
          n.setRobot3_faction(EuroBot2020StatusMessage.ROBOT_FACTION_UNKNOWN);
          n.setRobot3_position_error((short) 50);
          n.setRobot3_position_x((short) firstBlue.getProjectedLocation().getX());
          n.setRobot3_position_y((short) firstBlue.getProjectedLocation().getY());
          n.setRobot3_positionDelta_error((short) 0);
          n.setRobot3_positionDelta_x((short) 0);
          n.setRobot3_positionDelta_y((short) 0);
          n.setRobot3_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        if (secondBlue != null)
        {
          n.setRobot4_Aruco(Short.parseShort(secondBlue.getDataAsString()));
          n.setRobot4_color(EuroBot2020StatusMessage.ROBOT_COLOR_BLUE);
          n.setRobot4_faction(EuroBot2020StatusMessage.ROBOT_FACTION_UNKNOWN);
          n.setRobot4_position_error((short) 50);
          n.setRobot4_position_x((short) secondBlue.getProjectedLocation().getX());
          n.setRobot4_position_y((short) secondBlue.getProjectedLocation().getY());
          n.setRobot4_positionDelta_error((short) 0);
          n.setRobot4_positionDelta_x((short) 0);
          n.setRobot4_positionDelta_y((short) 0);
          n.setRobot4_type(EuroBot2020StatusMessage.ROBOT_TYPE_UNKNOWN);
        }
        break;
    }

  }


  public void detectWheelPosition()
  {

    long t0 = System.currentTimeMillis();

    byte detectedPostionFromYellowSide = EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN;
    byte detectedPostionFromBlueSide = EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN;

    Optional<CapturedImageOccurence> imageFromYellowSide = WatchTowerService.getInstance().getOpenCvCamService().getImages()
        .stream().filter((i) -> i.getWebcamName().matches(Game2020StatusService.YELLOW_SIDE_CAMERA.getNameRecognitionPattern()))
        .findFirst();

    if (imageFromYellowSide.isPresent())
    {
      LOG.info("[Game2020StatusService] detectWheelPosition(), yellow side, image found.");

      BufferedImage cYellowSideImage = imageFromYellowSide.get().getImage();

      // Graphics2D graphicsForMasking = cYellowSideImage.createGraphics();

      BufferedImage yellowSubImage;
      try
      {
        switch (this.getCurrentStatus().getWatchtowerSide())
        {
          case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_BLUE:

            yellowSubImage = cYellowSideImage.getSubimage(
                Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.yellowside.window.x", 540),
                Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.yellowside.window.y", 480),
                Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.yellowside.window.width", 470),
                Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.yellowside.window.height", 180));

            break;
          case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_YELLOW:
            yellowSubImage = cYellowSideImage.getSubimage(
                Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.yellowside.window.x", 540),
                Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.yellowside.window.y", 480),
                Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.yellowside.window.width", 470),
                Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.yellowside.window.height", 180));
            break;
          default:
            yellowSubImage =
                cYellowSideImage.getSubimage(Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.x", 540),
                    Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.y", 480),
                    Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.width", 470),
                    Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.height", 180));
        }
      }
      catch (Exception e)
      {
        LOG.warning("[Game2020StatusService] detectWheelPosition(), yellow side, ERROR ON SUB IMAGE !!!");

        yellowSubImage = cYellowSideImage.getSubimage(Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.x", 540),
            Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.y", 480),
            Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.width", 470),
            Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.window.height", 180));
      }


      if (Cfg.getBoolean("c1.services.root.debug5", false))
      {
        UtilImageIO.saveImage(yellowSubImage, "weathervane_raw_yellowSubImage-" + System.currentTimeMillis() + ".png");
      }


      Planar<GrayU8> yellowSubImagePlanar = ConvertBufferedImage.convertFrom(yellowSubImage, true, ImageType.pl(3, GrayU8.class));

      GrayU8 yellowSubImageGray = new GrayU8(yellowSubImagePlanar.width, yellowSubImagePlanar.height);

      // To gray
      ConvertImage.average(yellowSubImagePlanar, yellowSubImageGray);

      double cMean = ImageStatistics.mean(yellowSubImageGray);

      LOG.info("[Game2020StatusService] ImageStatistics.mean(yellowSubImageGray) = " + cMean);

      this.yellowSideMeanLumi.put(imageFromYellowSide.get().getTimestamp(), cMean);

      if (this.yellowSideMeanLumi.size() > Cfg.getInt("c1.services.gamestatus.weathervane.yellowside.histosize", 200))
      {
        this.yellowSideMeanLumi.pollFirstEntry();
        this.yellowSideMeanLumi.pollFirstEntry();
      }

      double maxMean = Double.MIN_VALUE;
      double minMean = Double.MAX_VALUE;
      double meanMean = 0.0d;

      for (Double inLoopCurrentMean : this.yellowSideMeanLumi.values())
      {
        maxMean = Math.max(maxMean, inLoopCurrentMean);
        minMean = Math.min(minMean, inLoopCurrentMean);
        meanMean += inLoopCurrentMean;
      }
      meanMean = meanMean / this.yellowSideMeanLumi.size();

      LOG.info("[Game2020StatusService] yellow side, maxMean = " + maxMean);
      LOG.info("[Game2020StatusService] yellow side, minMean = " + minMean);
      LOG.info("[Game2020StatusService] yellow side, meanMean = " + meanMean);


      double recentMaxMean = Double.MIN_VALUE;
      double recentMinMean = Double.MAX_VALUE;
      double recentMeanMean = 0.0d;



      NavigableMap<Long, Double> recentYellowSideMeanLumi = this.yellowSideMeanLumi.tailMap(
          System.currentTimeMillis() - Cfg.getLong("c1.services.gamestatus.weathervane.yellowside.recentduration", 20000), true);

      LOG.info("[Game2020StatusService] this.yellowSideMeanLumi.size() = " + this.yellowSideMeanLumi.size());
      LOG.info("[Game2020StatusService] recentYellowSideMeanLumi.size() = " + recentYellowSideMeanLumi.size());

      for (Double inLoopCurrentMean : recentYellowSideMeanLumi.values())
      {
        recentMaxMean = Math.max(recentMaxMean, inLoopCurrentMean);
        recentMinMean = Math.min(recentMinMean, inLoopCurrentMean);
        recentMeanMean += inLoopCurrentMean;
      }

      if (recentYellowSideMeanLumi.size() > 0)
      {
        recentMeanMean = recentMeanMean / recentYellowSideMeanLumi.size();
      }
      else
      {
        LOG.info("[Game2020StatusService] yellow side, recentMaxMean = meanMean = " + recentMaxMean);
        recentMeanMean = meanMean;
      }

      LOG.info("[Game2020StatusService] yellow side, recentMaxMean = " + recentMaxMean);
      LOG.info("[Game2020StatusService] yellow side, recentMinMean = " + recentMinMean);
      LOG.info("[Game2020StatusService] yellow side, recentMeanMean = " + recentMeanMean);

      if (recentMeanMean > meanMean + Cfg.getDouble("c1.services.gamestatus.weathervane.yellowside.detectionmargin", 1.8))
      {
        LOG.info("[Game2020StatusService] yellow side wheel detection, having a white side");
        detectedPostionFromYellowSide = EuroBot2020StatusMessage.WHEEL_POSITION_WHITE_UPSIDE_SOUTH;
      }
      else if (recentMeanMean < meanMean - Cfg.getDouble("c1.services.gamestatus.weathervane.yellowside.detectionmargin", 1.8))
      {
        LOG.info("[Game2020StatusService] yellow side wheel detection, having a black side");
        detectedPostionFromYellowSide = EuroBot2020StatusMessage.WHEEL_POSITION_BLACK_UPSIDE_NORTH;
      }
      else
      {
        LOG.info("[Game2020StatusService] yellow side wheel detection, UNKNOWN !");
        detectedPostionFromYellowSide = EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN;
      }

    }
    else
    {
      LOG.info("[Game2020StatusService] detectWheelPosition(), yellow side, NO IMAGE FOUND !");
    }


    Optional<CapturedImageOccurence> imageFromBlueSide = WatchTowerService.getInstance().getOpenCvCamService().getImages()
        .stream().filter((i) -> i.getWebcamName().matches(Game2020StatusService.BLUE_SIDE_CAMERA.getNameRecognitionPattern()))
        .findFirst();

    if (imageFromBlueSide.isPresent())
    {
      LOG.info("[Game2020StatusService] detectWheelPosition(), blue side, image found.");
      BufferedImage cBlueSideImage = imageFromBlueSide.get().getImage();

      // Graphics2D graphicsForMasking = cBlueSideImage.createGraphics();


      BufferedImage blueSubImage;
      try
      {
        switch (this.getCurrentStatus().getWatchtowerSide())
        {
          case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_BLUE:
            blueSubImage =
                cBlueSideImage.getSubimage(Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.blueside.window.x", 0),
                    Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.blueside.window.y", 480),
                    Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.blueside.window.width", 470),
                    Cfg.getInt("c1.services.gamestatus.weathervane.blueposition.blueside.window.height", 180));
            break;
          case EuroBot2020StatusMessage.WHATCHTOWER_SIDE_YELLOW:
            blueSubImage =
                cBlueSideImage.getSubimage(Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.blueside.window.x", 0),
                    Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.blueside.window.y", 480),
                    Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.blueside.window.width", 470),
                    Cfg.getInt("c1.services.gamestatus.weathervane.yellowposition.blueside.window.height", 180));
            break;
          default:
            blueSubImage = cBlueSideImage.getSubimage(Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.x", 0),
                Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.y", 480),
                Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.width", 470),
                Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.height", 180));
            break;
        }
      }
      catch (Exception e)
      {
        LOG.warning("[Game2020StatusService] detectWheelPosition(), blue side, ERROR ON SUB IMAGE !!!!");
        blueSubImage = cBlueSideImage.getSubimage(Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.x", 0),
            Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.y", 480),
            Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.width", 470),
            Cfg.getInt("c1.services.gamestatus.weathervane.blueside.window.height", 180));
      }



      if (Cfg.getBoolean("c1.services.root.debug5", false))
      {
        UtilImageIO.saveImage(blueSubImage, "weathervane_raw_blueSubImage-" + System.currentTimeMillis() + ".png");
      }

      Planar<GrayU8> blueSubImagePlanar = ConvertBufferedImage.convertFrom(blueSubImage, true, ImageType.pl(3, GrayU8.class));

      GrayU8 blueSubImageGray = new GrayU8(blueSubImagePlanar.width, blueSubImagePlanar.height);

      // To gray
      ConvertImage.average(blueSubImagePlanar, blueSubImageGray);

      double cMean = ImageStatistics.mean(blueSubImageGray);

      LOG.info("[Game2020StatusService] ImageStatistics.mean(blueSubImageGray) = " + cMean);

      this.blueSideMeanLumi.put(imageFromBlueSide.get().getTimestamp(), cMean);

      if (this.blueSideMeanLumi.size() > Cfg.getInt("c1.services.gamestatus.weathervane.blueside.histosize", 200))
      {
        this.blueSideMeanLumi.pollFirstEntry();
        this.blueSideMeanLumi.pollFirstEntry();
      }

      double maxMean = Double.MIN_VALUE;
      double minMean = Double.MAX_VALUE;
      double meanMean = 0.0d;

      for (Double inLoopCurrentMean : this.blueSideMeanLumi.values())
      {
        maxMean = Math.max(maxMean, inLoopCurrentMean);
        minMean = Math.min(minMean, inLoopCurrentMean);
        meanMean += inLoopCurrentMean;
      }
      meanMean = meanMean / this.blueSideMeanLumi.size();

      LOG.info("[Game2020StatusService] blue side, maxMean = " + maxMean);
      LOG.info("[Game2020StatusService] blue side, minMean = " + minMean);
      LOG.info("[Game2020StatusService] blue side, meanMean = " + meanMean);


      double recentMaxMean = Double.MIN_VALUE;
      double recentMinMean = Double.MAX_VALUE;
      double recentMeanMean = 0.0d;



      NavigableMap<Long, Double> recentBlueSideMeanLumi = this.blueSideMeanLumi.tailMap(
          System.currentTimeMillis() - Cfg.getLong("c1.services.gamestatus.weathervane.blueside.recentduration", 20000), true);

      LOG.info("[Game2020StatusService] this.blueSideMeanLumi.size() = " + this.yellowSideMeanLumi.size());
      LOG.info("[Game2020StatusService] recentBlueSideMeanLumi.size() = " + recentBlueSideMeanLumi.size());

      for (Double inLoopCurrentMean : recentBlueSideMeanLumi.values())
      {
        recentMaxMean = Math.max(recentMaxMean, inLoopCurrentMean);
        recentMinMean = Math.min(recentMinMean, inLoopCurrentMean);
        recentMeanMean += inLoopCurrentMean;
      }

      if (recentBlueSideMeanLumi.size() > 0)
      {
        recentMeanMean = recentMeanMean / recentBlueSideMeanLumi.size();
      }
      else
      {
        LOG.info("[Game2020StatusService] blue side, recentMaxMean = meanMean = " + recentMaxMean);
        recentMeanMean = meanMean;
      }

      LOG.info("[Game2020StatusService] blue side, recentMaxMean = " + recentMaxMean);
      LOG.info("[Game2020StatusService] blue side, recentMinMean = " + recentMinMean);
      LOG.info("[Game2020StatusService] blue side, recentMeanMean = " + recentMeanMean);

      if (recentMeanMean > meanMean + Cfg.getDouble("c1.services.gamestatus.weathervane.blueside.detectionmargin", 1.8))
      {
        LOG.info("[Game2020StatusService] blue side wheel detection, having a white side");
        detectedPostionFromBlueSide = EuroBot2020StatusMessage.WHEEL_POSITION_WHITE_UPSIDE_SOUTH;
      }
      else if (recentMeanMean < meanMean - Cfg.getDouble("c1.services.gamestatus.weathervane.blueside.detectionmargin", 1.8))
      {
        LOG.info("[Game2020StatusService] blue side wheel detection, having a black side");
        detectedPostionFromBlueSide = EuroBot2020StatusMessage.WHEEL_POSITION_BLACK_UPSIDE_NORTH;
      }
      else
      {
        LOG.info("[Game2020StatusService] blue side wheel detection, UNKNOWN !");
        detectedPostionFromBlueSide = EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN;
      }



    }
    else
    {
      LOG.info("[Game2020StatusService] detectWheelPosition(), blue side, NO IMAGE FOUND !");
    }

    if (detectedPostionFromBlueSide != EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN
        && detectedPostionFromYellowSide == EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN)
    {
      this.getCurrentStatus().setWheelPosition(detectedPostionFromBlueSide);
      this.getNextStatus().setWheelPosition(detectedPostionFromBlueSide);
    }
    else if (detectedPostionFromBlueSide == EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN
        && detectedPostionFromYellowSide != EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN)
    {
      this.getCurrentStatus().setWheelPosition(detectedPostionFromYellowSide);
      this.getNextStatus().setWheelPosition(detectedPostionFromYellowSide);
    }
    else if (detectedPostionFromBlueSide != EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN
        && detectedPostionFromYellowSide != EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN
        && detectedPostionFromBlueSide == detectedPostionFromYellowSide)
    {
      this.getCurrentStatus().setWheelPosition(detectedPostionFromYellowSide);
      this.getNextStatus().setWheelPosition(detectedPostionFromYellowSide);
    }
    else
    {
      this.getCurrentStatus().setWheelPosition(EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN);
      this.getNextStatus().setWheelPosition(EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN);
    }

    String wheelFriendlyString = "";
    switch (this.getCurrentStatus().getWheelPosition())
    {
      case EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN:
        wheelFriendlyString += "U";
        break;
      case EuroBot2020StatusMessage.WHEEL_POSITION_WHITE_UPSIDE_SOUTH:
        wheelFriendlyString += "W";
        break;
      case EuroBot2020StatusMessage.WHEEL_POSITION_BLACK_UPSIDE_NORTH:
        wheelFriendlyString += "B";
        break;
      default:
        wheelFriendlyString += "?";
        break;
    }
    switch (detectedPostionFromBlueSide)
    {
      case EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN:
        wheelFriendlyString += "U";
        break;
      case EuroBot2020StatusMessage.WHEEL_POSITION_WHITE_UPSIDE_SOUTH:
        wheelFriendlyString += "W";
        break;
      case EuroBot2020StatusMessage.WHEEL_POSITION_BLACK_UPSIDE_NORTH:
        wheelFriendlyString += "B";
        break;
      default:
        wheelFriendlyString += "?";
        break;
    }
    switch (detectedPostionFromYellowSide)
    {
      case EuroBot2020StatusMessage.WHEEL_POSITION_UNKNOWN:
        wheelFriendlyString += "U";
        break;
      case EuroBot2020StatusMessage.WHEEL_POSITION_WHITE_UPSIDE_SOUTH:
        wheelFriendlyString += "W";
        break;
      case EuroBot2020StatusMessage.WHEEL_POSITION_BLACK_UPSIDE_NORTH:
        wheelFriendlyString += "B";
        break;
      default:
        wheelFriendlyString += "?";
        break;
    }

    GAME_LOG.info("weathervane (final / blue / yellow) = " + wheelFriendlyString);

    if (Cfg.getBoolean("c1.services.root.debug3", false) && (this.cycleCounter % 10 == 0))
    {
      Runnable lumiGraph = () -> {
        final int graphWidth = 6000;
        final int graphHeight = 600;
        final int pointHalfSize = 2;

        final long firstBlueSideLumiTime =
            this.blueSideMeanLumi.keySet().stream().min((a, b) -> Long.valueOf(a - b).intValue()).get();
        final long firstYellowSideLumiTime =
            this.yellowSideMeanLumi.keySet().stream().min((a, b) -> Long.valueOf(a - b).intValue()).get();

        final long lastBlueSideLumiTime =
            this.blueSideMeanLumi.keySet().stream().max((a, b) -> Long.valueOf(a - b).intValue()).get();
        final long lastYellowSideLumiTime =
            this.yellowSideMeanLumi.keySet().stream().max((a, b) -> Long.valueOf(a - b).intValue()).get();

        final double minBlueLumi = this.blueSideMeanLumi.values().stream().min((a, b) -> Double.valueOf(a - b).intValue()).get();
        final double minYellowLumi =
            this.yellowSideMeanLumi.values().stream().min((a, b) -> Double.valueOf(a - b).intValue()).get();

        final double maxBlueLumi = this.blueSideMeanLumi.values().stream().max((a, b) -> Double.valueOf(a - b).intValue()).get();
        final double maxYellowLumi =
            this.yellowSideMeanLumi.values().stream().max((a, b) -> Double.valueOf(a - b).intValue()).get();

        final double blueLumiRange = maxBlueLumi - minBlueLumi;
        final double yellowLumiRange = maxYellowLumi - minYellowLumi;


        final double minLumi = Math.min(minBlueLumi, minYellowLumi);
        final double maxLumi = Math.max(maxBlueLumi, maxYellowLumi);

        final long firstMeasure = Math.min(firstYellowSideLumiTime, firstBlueSideLumiTime);
        final long lastMeasure = Math.max(lastBlueSideLumiTime, lastYellowSideLumiTime);

        final double lrange = (maxLumi - minLumi);
        final double trange = Math.max(Long.valueOf(lastMeasure - firstMeasure).doubleValue(), 500.0 * 1000.0);

        final double tscale = Integer.valueOf(graphWidth).doubleValue() / trange;

        final double lscale = Integer.valueOf(graphWidth).doubleValue() / lrange;

        final Random rnd1 = new Random();


        LOG.info("LumiGraph, firstBlueSideLumiTime = " + firstBlueSideLumiTime);
        LOG.info("LumiGraph, firstYellowSideLumiTime = " + firstYellowSideLumiTime);
        LOG.info("LumiGraph, lastBlueSideLumiTime = " + lastBlueSideLumiTime);
        LOG.info("LumiGraph, lastYellowSideLumiTime = " + lastYellowSideLumiTime);
        LOG.info("LumiGraph, minBlueLumi = " + minBlueLumi);
        LOG.info("LumiGraph, minYellowLumi = " + minYellowLumi);
        LOG.info("LumiGraph, maxBlueLumi = " + maxBlueLumi);
        LOG.info("LumiGraph, maxYellowLumi = " + maxYellowLumi);


        LOG.info("LumiGraph, blueLumiRange = " + blueLumiRange);
        LOG.info("LumiGraph, yellowLumiRange = " + yellowLumiRange);
        LOG.info("LumiGraph, minLumi = " + minLumi);
        LOG.info("LumiGraph, maxLumi = " + maxLumi);
        LOG.info("LumiGraph, firstMeasure = " + firstMeasure);
        LOG.info("LumiGraph, lastMeasure = " + lastMeasure);
        LOG.info("LumiGraph, lrange = " + lrange);
        LOG.info("LumiGraph, trange = " + trange);
        LOG.info("LumiGraph, tscale = " + tscale);
        LOG.info("LumiGraph, lscale = " + lscale);

        // GraphicsEnvironment ge;
        // ge = GraphicsEnvironment.getLocalGraphicsEnvironment();


        final BufferedImage blueLumiGraphImg = new BufferedImage(graphWidth, graphHeight, BufferedImage.TYPE_INT_ARGB);
        final Graphics2D g2BlueLumi = blueLumiGraphImg.createGraphics();
        g2BlueLumi.setPaint(Color.WHITE);
        g2BlueLumi.setColor(Color.WHITE);
        g2BlueLumi.setBackground(Color.WHITE);
        g2BlueLumi.setStroke(new BasicStroke(3));
        g2BlueLumi.fillRect(0, 0, graphWidth, graphHeight);

        g2BlueLumi.setPaint(Color.BLUE);
        g2BlueLumi.setColor(Color.BLUE);
        g2BlueLumi.setBackground(Color.BLUE);
        g2BlueLumi.setStroke(new BasicStroke(3));
        this.blueSideMeanLumi.forEach((t, l) -> {

          long tdelta = t - firstMeasure;

          double t_point = Long.valueOf(t - firstMeasure).doubleValue() * tscale;
          double l_point = (l - minLumi) * lscale;

          int t_point_int = Double.valueOf(t_point).intValue();
          int l_point_int = Double.valueOf(l_point).intValue();



          g2BlueLumi.fillRect(t_point_int - pointHalfSize, l_point_int - pointHalfSize, 2 * pointHalfSize, 2 * pointHalfSize);

          // double TimeInMs = Math.floor(Long.valueOf(t - firstMeasure).doubleValue() / 1000.0);
          double lTruncated = Math.floor(l * 1000.0) / 1000;
          String label = "b,t=" + tdelta + "ms, l=" + lTruncated;

          // g2BlueLumi.drawString(label, t_point_int + 12, l_point_int + 12 + 12 *
          // rnd1.nextInt(20));

          LOG.info("LumiGraph, blue, t = " + t);
          LOG.info("LumiGraph, blue, l = " + l);
          LOG.info("LumiGraph, blue, tdelta = " + tdelta);
          LOG.info("LumiGraph, blue, tdelta/1000 = " + tdelta / 1000);
          LOG.info("LumiGraph, blue, t_point = " + t_point);
          LOG.info("LumiGraph, blue, l_point = " + l_point);
          LOG.info("LumiGraph, blue, t_point_int = " + t_point_int);
          LOG.info("LumiGraph, blue, l_point_int = " + l_point_int);

        });

        final BufferedImage yellowLumiGraphImg = new BufferedImage(graphWidth, graphHeight, BufferedImage.TYPE_INT_ARGB);
        final Graphics2D g2YellowLumi = yellowLumiGraphImg.createGraphics();
        g2YellowLumi.setPaint(Color.WHITE);
        g2YellowLumi.setColor(Color.WHITE);
        g2YellowLumi.setBackground(Color.WHITE);
        g2YellowLumi.setStroke(new BasicStroke(3));
        g2YellowLumi.fillRect(0, 0, graphWidth, graphHeight);

        g2YellowLumi.setPaint(Color.BLACK);
        g2YellowLumi.setColor(Color.BLACK);
        g2YellowLumi.setBackground(Color.BLACK);
        g2YellowLumi.setStroke(new BasicStroke(3));
        this.yellowSideMeanLumi.forEach((t, l) -> {

          long tdelta = t - firstMeasure;

          double t_point = Long.valueOf(t - firstMeasure).doubleValue() * tscale;
          double l_point = (l - minLumi) * lscale;

          int t_point_int = Double.valueOf(t_point).intValue();
          int l_point_int = Double.valueOf(l_point).intValue();



          g2YellowLumi.fillRect(t_point_int - pointHalfSize, l_point_int - pointHalfSize, 2 * pointHalfSize, 2 * pointHalfSize);

          // double TimeInMs = Math.floor(Long.valueOf(t - firstMeasure).doubleValue() / 1000.0);
          double lTruncated = Math.floor(l * 1000.0) / 1000;
          String label = "y,t=" + tdelta + "ms, l=" + lTruncated;


          // g2BlueLumi.drawString(label, t_point_int + 12, l_point_int + 12 + 12 *
          // rnd1.nextInt(20));

          LOG.info("LumiGraph, yellow, t = " + t);
          LOG.info("LumiGraph, yellow, l = " + l);
          LOG.info("LumiGraph, yellow, tdelta = " + tdelta);
          LOG.info("LumiGraph, yellow, tdelta/1000 = " + tdelta / 1000);
          LOG.info("LumiGraph, yellow, t_point = " + t_point);
          LOG.info("LumiGraph, yellow, l_point = " + l_point);
          LOG.info("LumiGraph, yellow, t_point_int = " + t_point_int);
          LOG.info("LumiGraph, yellow, l_point_int = " + l_point_int);

        });

        g2BlueLumi.dispose();

        UtilImageIO.saveImage(blueLumiGraphImg, "weathervane-blue-histo-" + System.currentTimeMillis() + ".png");
        UtilImageIO.saveImage(yellowLumiGraphImg, "weathervane-yellow-histo-" + System.currentTimeMillis() + ".png");
      };

      new Thread(lumiGraph, "lumiGraph-" + System.currentTimeMillis()).start();


    }

    long t1 = System.currentTimeMillis();
    LOG.info("detectWheelPosition() took " + (t1 - t0) + " ms");



  }

  public void detectGroundedBuoys()
  {

    short yellowCode = this.getNextStatus().getBuoysCode();
    short blueCode = this.getNextStatus().getBuoysCode();

    ArrayList<Color> xYellowSideSortedColors = new ArrayList<>();
    ArrayList<Color> xBlueSideSortedColors = new ArrayList<>();



    final float redHue = (float) (Math.PI * 2) * Cfg.getFloat("c1.services.gamestatus.groundedbuoys.redfilter.centerhue", 0.96f);
    final float greenHue =
        (float) (Math.PI * 2) * Cfg.getFloat("c1.services.gamestatus.groundedbuoys.greenfilter.centerhue", 0.24f);

    final Comparator<EllipseRotated_F64> xComp = (e1, e2) -> e1.center.x > e2.center.x ? 1 : e1.center.x == e2.center.x ? 0 : -1;

    final Comparator<Integer> iComp = (i1, i2) -> i1 > i2 ? 1 : i1 == i2 ? 0 : -1;

    final float minBuoyApparentSize = Cfg.getFloat("c1.services.gamestatus.groundedbuoys.minbuoyapparentsize", 18.7f);
    final float maxBuoyApparentSize = Cfg.getFloat("c1.services.gamestatus.groundedbuoys.maxbuoyapparentsize", 39.0f);

    final double maxEllipseElong = Cfg.getDouble("c1.services.gamestatus.groundedbuoys.common.maxellipseelong", 3.5);

    float neutralMultipliers[] =
    {Cfg.getFloat("c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.neutral", 1.53f),
        Cfg.getFloat("c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.neutral", 2.05f)};
    float nearSideMultipliers[] =
    {Cfg.getFloat("c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.near", 1.53f),
        Cfg.getFloat("c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.near", 2.05f)};
    float farSideMultipliers[] =
    {Cfg.getFloat("c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.far", 1.53f),
        Cfg.getFloat("c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.far", 2.05f)};

    final float yellowSideMultipliers[];
    final float blueSideMultipliers[];

    if (this.currentStatus.getWatchtowerSide() == EuroBot2020StatusMessage.WHATCHTOWER_SIDE_BLUE)
    {
      yellowSideMultipliers = farSideMultipliers;
      blueSideMultipliers = nearSideMultipliers;
    }
    else if (this.currentStatus.getWatchtowerSide() == EuroBot2020StatusMessage.WHATCHTOWER_SIDE_YELLOW)
    {
      yellowSideMultipliers = nearSideMultipliers;
      blueSideMultipliers = farSideMultipliers;
    }
    else
    {
      yellowSideMultipliers = neutralMultipliers;
      blueSideMultipliers = neutralMultipliers;
    }


    Optional<CapturedImageOccurence> imageFromYellowSide = WatchTowerService.getInstance().getOpenCvCamService().getImages()
        .stream().filter((i) -> i.getWebcamName().matches(Game2020StatusService.YELLOW_SIDE_CAMERA.getNameRecognitionPattern()))
        .findFirst();

    Optional<CapturedImageOccurence> imageFromBlueSide = WatchTowerService.getInstance().getOpenCvCamService().getImages()
        .stream().filter((i) -> i.getWebcamName().matches(Game2020StatusService.BLUE_SIDE_CAMERA.getNameRecognitionPattern()))
        .findFirst();

    if (imageFromYellowSide.isPresent())
    {
      LOG.info("[Game2020StatusService] detectGroundedBuoys(), got image from yellow side");



      BufferedImage yellowSideImage = imageFromYellowSide.get().getImage().getSubimage(
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.yellowside.window.x", 56),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.yellowside.window.y", 570),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.yellowside.window.width", 560),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.yellowside.window.height", 120));


      BufferedImage redFilteredImage = Game2020StatusService.thresholdHue(yellowSideImage, redHue,
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.yellowside.redfilter.saturation", 0.43f),
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.yellowside.redfilter.radius", 0.28f),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.yellowside.redfilter.blur", 7));
      BufferedImage greenFilteredImage = Game2020StatusService.thresholdHue(yellowSideImage, greenHue,
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.saturation", 0.17f),
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.radius", 0.51f),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.blur", 7));

      // BufferedImage redFilteredImage =
      // Game2020StatusService.thresholdHue(yellowSideImage, redHue, 0.43f, 0.28f, 7);
      // BufferedImage greenFilteredImage =
      // Game2020StatusService.thresholdHue(yellowSideImage, greenHue, 0.17f, 0.51f, 7);



      if (Cfg.getBoolean("c1.services.root.debug5", false))
      {
        UtilImageIO.saveImage(yellowSideImage, "raw_fromYellowSide-" + System.currentTimeMillis() + ".png");
        UtilImageIO.saveImage(redFilteredImage, "redFilteredImage_fromYellowSide-" + System.currentTimeMillis() + ".png");
        UtilImageIO.saveImage(greenFilteredImage, "greenFilteredImage_fromYellowSide-" + System.currentTimeMillis() + ".png");
      }

      ArrayList<EllipseRotated_F64> yellowSideRedEllipses = Game2020StatusService.getEllipsesFromImage(redFilteredImage);
      ArrayList<EllipseRotated_F64> yellowSideGreenEllipses = Game2020StatusService.getEllipsesFromImage(greenFilteredImage);


      for (EllipseRotated_F64 cRedEllipse : yellowSideRedEllipses)
      {
        LOG.info("[Game2020StatusService] detectGroundedBuoys(), yellow side, before filtering, cRedEllipse="
            + cRedEllipse.toString());
      }

      for (EllipseRotated_F64 cGreenEllipse : yellowSideGreenEllipses)
      {
        LOG.info("[Game2020StatusService] detectGroundedBuoys(), yellow side, before filtering, cGreenEllipse="
            + cGreenEllipse.toString());
      }

      // Cfg.getBoolean("c1.services.root.debug5", false)
      // c1.services.gamestatus.groundedbuoys.yellowside

      if (!Cfg.getBoolean("cc1.services.gamestatus.groundedbuoys.yellowside.disableellipsefilter", false))
      {
        yellowSideRedEllipses.removeIf(e -> ((e.a < minBuoyApparentSize || e.a > maxBuoyApparentSize)
            && (e.b < minBuoyApparentSize || e.b > maxBuoyApparentSize)));
        yellowSideGreenEllipses.removeIf(e -> ((e.a < minBuoyApparentSize || e.a > maxBuoyApparentSize)
            && (e.b < minBuoyApparentSize || e.b > maxBuoyApparentSize)));



        yellowSideRedEllipses.removeIf(e -> e.a / e.b > maxEllipseElong);
        yellowSideRedEllipses.removeIf(e -> e.b / e.a > maxEllipseElong);
        yellowSideGreenEllipses.removeIf(e -> e.a / e.b > maxEllipseElong);
        yellowSideGreenEllipses.removeIf(e -> e.b / e.a > maxEllipseElong);

      }



      for (EllipseRotated_F64 cRedEllipse : yellowSideRedEllipses)
      {
        LOG.info(
            "[Game2020StatusService] detectGroundedBuoys(), yellow side, after filtering, cRedEllipse=" + cRedEllipse.toString());
      }

      for (EllipseRotated_F64 cGreenEllipse : yellowSideGreenEllipses)
      {
        LOG.info("[Game2020StatusService] detectGroundedBuoys(), yellow side, after filtering, cGreenEllipse="
            + cGreenEllipse.toString());
      }

      if (Cfg.getBoolean("c1.services.root.debug5", false))
      {
        BufferedImage yellowSideEllipses =
            new BufferedImage(redFilteredImage.getWidth(), redFilteredImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = yellowSideEllipses.createGraphics();

        g2.setColor(new Color(255, 50, 50));
        g2.setStroke(new BasicStroke(4));
        for (EllipseRotated_F64 cRedEllipse : yellowSideRedEllipses)
        {
          VisualizeShapes.drawEllipse(cRedEllipse, g2);
        }

        g2.setColor(new Color(50, 255, 50));
        g2.setStroke(new BasicStroke(4));
        for (EllipseRotated_F64 cGreenEllipse : yellowSideGreenEllipses)
        {
          VisualizeShapes.drawEllipse(cGreenEllipse, g2);
        }

        UtilImageIO.saveImage(yellowSideEllipses, "yellowSideEllipses_" + "_" + System.currentTimeMillis() + ".png");

      }

      ArrayList<EllipseRotated_F64> allYellowSideEllipse = new ArrayList<>();
      allYellowSideEllipse.addAll(yellowSideGreenEllipses);
      allYellowSideEllipse.addAll(yellowSideRedEllipses);



      // Iterate the merged red/green ellipses, feeding sXortedColors with colors by polling
      // red/green list


      allYellowSideEllipse.stream().sorted(xComp).forEach(e -> {



        double elong = e.a / e.b;
        // LOG.info("YellowSide elong: " + elong);


        Optional<EllipseRotated_F64> optionalPrevious =
            allYellowSideEllipse.stream().filter(e1 -> e1.center.x < e.center.x).max(xComp);

        if (optionalPrevious.isPresent())
        {

          EllipseRotated_F64 p = optionalPrevious.get();

          double gapWithPrevious = (e.center.x - p.center.x) - ((e.a + p.a) / 2.0);
          LOG.info("YellowSide, look for gap with previous ellipse, gap is " + gapWithPrevious);

          if (gapWithPrevious > minBuoyApparentSize && gapWithPrevious < maxBuoyApparentSize)
          {
            xYellowSideSortedColors.add(Color.BLACK);
          }

          if (gapWithPrevious > maxBuoyApparentSize && gapWithPrevious < 2 * minBuoyApparentSize)
          {
            xYellowSideSortedColors.add(Color.BLACK);
            xYellowSideSortedColors.add(Color.BLACK);
          }

        }
        else
        {
          LOG.info("YellowSide, No previous ellipse for gap analysis...");
        }

        if (yellowSideGreenEllipses.contains(e))
        {
          LOG.info("YellowSide green ellipse (" + elong + ", single): " + e.toString());
          xYellowSideSortedColors.add(Color.GREEN);
          if (elong > yellowSideMultipliers[0])
          {
            LOG.info("YellowSide green ellipse (" + elong + ", doubled): " + e.toString());
            xYellowSideSortedColors.add(Color.GREEN);
          }
          if (elong > yellowSideMultipliers[1])
          {
            LOG.info("YellowSide green ellipse (" + elong + ", tripled): " + e.toString());
            xYellowSideSortedColors.add(Color.GREEN);
          }
        }
        else if (yellowSideRedEllipses.contains(e))
        {
          LOG.info("YellowSide red ellipse (" + elong + ", single): " + e.toString());
          xYellowSideSortedColors.add(Color.RED);
          if (elong > yellowSideMultipliers[0])
          {
            LOG.info("YellowSide red ellipse (" + elong + ", doubled): " + e.toString());
            xYellowSideSortedColors.add(Color.RED);
          }
          if (elong > yellowSideMultipliers[1])
          {
            LOG.info("YellowSide red ellipse (" + elong + ", tripled): " + e.toString());
            xYellowSideSortedColors.add(Color.RED);
          }
        }
        else
        {
          LOG.warning("YellowSide unknown ellipse (" + elong + ", single): " + e.toString());
          xYellowSideSortedColors.add(Color.BLACK);
          if (elong > yellowSideMultipliers[0])
          {
            LOG.warning("YellowSide unknown ellipse (" + elong + ", doubled): " + e.toString());
            xYellowSideSortedColors.add(Color.BLACK);
          }
          if (elong > yellowSideMultipliers[1])
          {
            LOG.warning("YellowSide unknown ellipse (" + elong + ", tripled): " + e.toString());
            xYellowSideSortedColors.add(Color.BLACK);
          }

        }



      });

      String userFriendlyColorCode = "";



      for (Color c : xYellowSideSortedColors)
      {
        LOG.info("YellowSide color sequence, color:" + c.toString());

        if (c.equals(Color.GREEN))
        {
          userFriendlyColorCode += "G";
        }
        else if (c.equals(Color.RED))
        {
          userFriendlyColorCode += "R";
        }
        else if (c.equals(Color.BLACK))
        {
          userFriendlyColorCode += "B";
        }
        else
        {
          userFriendlyColorCode += "x";
        }

      }

      GAME_LOG.info("yellow side color sequence: " + userFriendlyColorCode);


      List<BuoyCode> yellowSideCodes = ImmutableList.<BuoyCode>builder()

          // Nominal codes
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_01, Color.RED, Color.GREEN, Color.RED, Color.RED, Color.GREEN))
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_02, Color.RED, Color.RED, Color.GREEN, Color.RED, Color.GREEN))
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_03, Color.RED, Color.RED, Color.RED, Color.GREEN, Color.GREEN))

          // Not so nominal code 01 with missing green buoy at the second rank from external part of
          // the table at yellow side.
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_01, Color.RED, Color.RED, Color.RED, Color.GREEN, Color.GREEN))
          .build();


      Optional<BuoyCode> optionalYellowSideCode =
          yellowSideCodes.stream().filter(bc -> bc.matches(xYellowSideSortedColors)).findFirst();

      if (optionalYellowSideCode.isPresent())
      {
        yellowCode = optionalYellowSideCode.get().getBuoyCode();
      }
      else
      {
        yellowCode = EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN;
      }



    }
    else
    {
      LOG.info("[Game2020StatusService] detectGroundedBuoys(), NO VALID image from yellow side");
      // for (CapturedImageOccurence cio : WatchTowerService.getInstance().getWebCamForPiService()
      // .getImages())
      // {
      // LOG.info("[Game2020StatusService] image from " + cio.getWebcamName() + ", at "
      // + cio.getTimestamp());
      // }

    }

    if (imageFromBlueSide.isPresent())
    {
      LOG.info("[Game2020StatusService] detectGroundedBuoys(), got image from blue side");

      BufferedImage blueSideImage = imageFromYellowSide.get().getImage().getSubimage(
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.blueside.window.x", 75),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.blueside.window.y", 570),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.blueside.window.width", 560),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.blueside.window.height", 120));



      BufferedImage redFilteredImage = Game2020StatusService.thresholdHue(blueSideImage, redHue,
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.blueside.redfilter.saturation", 0.43f),
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.blueside.redfilter.radius", 0.28f),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.blueside.redfilter.blur", 7));
      BufferedImage greenFilteredImage = Game2020StatusService.thresholdHue(blueSideImage, greenHue,
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.blueside.greenfilter.saturation", 0.17f),
          Cfg.getFloat("c1.services.gamestatus.groundedbuoys.blueside.greenfilter.radius", 0.51f),
          Cfg.getInt("c1.services.gamestatus.groundedbuoys.blueside.greenfilter.blur", 7));

      // BufferedImage redFilteredImage =
      // Game2020StatusService.thresholdHue(blueSideImage, redHue, 0.43f, 0.28f, 7);
      // BufferedImage greenFilteredImage =
      // Game2020StatusService.thresholdHue(blueSideImage, greenHue, 0.17f, 0.47f, 7);

      if (Cfg.getBoolean("c1.services.root.debug5", false))
      {
        UtilImageIO.saveImage(blueSideImage, "raw_fromBlueSide-" + System.currentTimeMillis() + ".png");
        UtilImageIO.saveImage(redFilteredImage, "redFilteredImage_fromBlueSide-" + System.currentTimeMillis() + ".png");
        UtilImageIO.saveImage(greenFilteredImage, "greenFilteredImage_fromBlueSide-" + System.currentTimeMillis() + ".png");
      }

      ArrayList<EllipseRotated_F64> blueSideRedEllipses = Game2020StatusService.getEllipsesFromImage(redFilteredImage);
      ArrayList<EllipseRotated_F64> blueSideGreenEllipses = Game2020StatusService.getEllipsesFromImage(greenFilteredImage);

      for (EllipseRotated_F64 cRedEllipse : blueSideRedEllipses)
      {
        LOG.info(
            "[Game2020StatusService] detectGroundedBuoys(), blue side, before filtering, cRedEllipse=" + cRedEllipse.toString());

      }

      for (EllipseRotated_F64 cGreenEllipse : blueSideGreenEllipses)
      {
        LOG.info("[Game2020StatusService] detectGroundedBuoys(), blue side, before filtering, cGreenEllipse="
            + cGreenEllipse.toString());
      }

      if (!Cfg.getBoolean("cc1.services.gamestatus.groundedbuoys.blueside.disableellipsefilter", false))
      {
        blueSideRedEllipses.removeIf(e -> ((e.a < minBuoyApparentSize || e.a > maxBuoyApparentSize)
            && (e.b < minBuoyApparentSize || e.b > maxBuoyApparentSize)));
        blueSideGreenEllipses.removeIf(e -> ((e.a < minBuoyApparentSize || e.a > maxBuoyApparentSize)
            && (e.b < minBuoyApparentSize || e.b > maxBuoyApparentSize)));

        blueSideRedEllipses.removeIf(e -> e.a / e.b > maxEllipseElong);
        blueSideRedEllipses.removeIf(e -> e.b / e.a > maxEllipseElong);
        blueSideGreenEllipses.removeIf(e -> e.a / e.b > maxEllipseElong);
        blueSideGreenEllipses.removeIf(e -> e.b / e.a > maxEllipseElong);

      }



      for (EllipseRotated_F64 cRedEllipse : blueSideRedEllipses)
      {
        LOG.info(
            "[Game2020StatusService] detectGroundedBuoys(), blue side, after filtering, cRedEllipse=" + cRedEllipse.toString());

      }

      for (EllipseRotated_F64 cGreenEllipse : blueSideGreenEllipses)
      {
        LOG.info("[Game2020StatusService] detectGroundedBuoys(), blue side, after filtering, cGreenEllipse="
            + cGreenEllipse.toString());
      }

      if (Cfg.getBoolean("c1.services.root.debug5", false))
      {
        BufferedImage blueSideEllipses =
            new BufferedImage(redFilteredImage.getWidth(), redFilteredImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = blueSideEllipses.createGraphics();

        g2.setColor(new Color(255, 50, 50));
        g2.setStroke(new BasicStroke(4));
        for (EllipseRotated_F64 cRedEllipse : blueSideRedEllipses)
        {
          VisualizeShapes.drawEllipse(cRedEllipse, g2);
        }

        g2.setColor(new Color(50, 255, 50));
        g2.setStroke(new BasicStroke(4));
        for (EllipseRotated_F64 cGreenEllipse : blueSideGreenEllipses)
        {
          VisualizeShapes.drawEllipse(cGreenEllipse, g2);
        }

        UtilImageIO.saveImage(blueSideEllipses, "blueSideEllipses_" + "_" + System.currentTimeMillis() + ".png");

      }

      ArrayList<EllipseRotated_F64> allBlueSideEllipse = new ArrayList<>();
      allBlueSideEllipse.addAll(blueSideGreenEllipses);
      allBlueSideEllipse.addAll(blueSideRedEllipses);



      allBlueSideEllipse.stream().sorted(xComp).forEach(e -> {

        double elong = e.a / e.b;
        // LOG.info("BlueSide elong: " + elong);


        Optional<EllipseRotated_F64> optionalPrevious =
            allBlueSideEllipse.stream().filter(e1 -> e1.center.x < e.center.x).max(xComp);

        if (optionalPrevious.isPresent())
        {
          EllipseRotated_F64 p = optionalPrevious.get();

          double gapWithPrevious = (e.center.x - p.center.x) - ((e.a + p.a) / 2);
          LOG.info("BlueSide, look for gap with previous ellipse, gap is " + gapWithPrevious);

          if (gapWithPrevious > minBuoyApparentSize && gapWithPrevious < maxBuoyApparentSize)
          {
            xBlueSideSortedColors.add(Color.BLACK);
          }

          if (gapWithPrevious > maxBuoyApparentSize && gapWithPrevious < 2 * minBuoyApparentSize)
          {
            xBlueSideSortedColors.add(Color.BLACK);
            xBlueSideSortedColors.add(Color.BLACK);
          }


        }
        else
        {
          LOG.info("BlueSide, No previous ellipse for gap analysis...");
        }


        if (blueSideGreenEllipses.contains(e))
        {
          LOG.info("BlueSide green ellipse (" + elong + ", single): " + e.toString());
          xBlueSideSortedColors.add(Color.GREEN);
          if (elong > blueSideMultipliers[0])
          {
            LOG.info("BlueSide green ellipse (" + elong + ", doubled): " + e.toString());
            xBlueSideSortedColors.add(Color.GREEN);
          }
          if (elong > blueSideMultipliers[1])
          {
            LOG.info("BlueSide green ellipse (" + elong + ", tripled): " + e.toString());
            xBlueSideSortedColors.add(Color.GREEN);
          }
        }
        else if (blueSideRedEllipses.contains(e))
        {
          LOG.info("BlueSide red ellipse (" + elong + ", single): " + e.toString());
          xBlueSideSortedColors.add(Color.RED);
          if (elong > blueSideMultipliers[0])
          {
            LOG.info("BlueSide red ellipse (" + elong + ", doubled): " + e.toString());
            xBlueSideSortedColors.add(Color.RED);
          }
          if (elong > blueSideMultipliers[1])
          {
            LOG.info("BlueSide red ellipse (" + elong + ", tripled): " + e.toString());
            xBlueSideSortedColors.add(Color.RED);
          }
        }
        else
        {
          LOG.warning("BlueSide unknown ellipse (" + elong + ", single): " + e.toString());
          xBlueSideSortedColors.add(Color.BLACK);
          if (elong > blueSideMultipliers[0])
          {
            LOG.warning("BlueSide unknown ellipse (" + elong + ", doubled): " + e.toString());
            xBlueSideSortedColors.add(Color.BLACK);
          }
          if (elong > blueSideMultipliers[1])
          {
            LOG.warning("BlueSide unknown ellipse (" + elong + ", tripled): " + e.toString());
            xBlueSideSortedColors.add(Color.BLACK);
          }

        }



      });



      String userFriendlyColorCode = "";


      for (Color c : xBlueSideSortedColors)
      {
        LOG.info("BlueSide color sequence, color:" + c.toString());

        if (c.equals(Color.GREEN))
        {
          userFriendlyColorCode += "G";
        }
        else if (c.equals(Color.RED))
        {
          userFriendlyColorCode += "R";
        }
        else if (c.equals(Color.BLACK))
        {
          userFriendlyColorCode += "B";
        }
        else
        {
          userFriendlyColorCode += "x";
        }

      }

      GAME_LOG.info("blue side color sequence: " + userFriendlyColorCode);



      List<BuoyCode> blueSideCodes = ImmutableList.<BuoyCode>builder()

          // Nominal codes
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_01, Color.RED, Color.GREEN, Color.GREEN, Color.RED, Color.GREEN))
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_02, Color.RED, Color.GREEN, Color.RED, Color.GREEN, Color.GREEN))
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_03, Color.RED, Color.RED, Color.GREEN, Color.GREEN, Color.GREEN))

          // Not nominal 01 code at blue side with green *2 / black * 2 / green * 1, probable bad
          // red filter.
          .add(new BuoyCode(EuroBot2020StatusMessage.BUOYS_CODE_01, Color.GREEN, Color.GREEN, Color.BLACK, Color.BLACK,
              Color.GREEN))

          .build();


      Optional<BuoyCode> optionalBlueSideCode =
          blueSideCodes.stream().filter(bc -> bc.matches(xYellowSideSortedColors)).findFirst();

      if (optionalBlueSideCode.isPresent())
      {
        blueCode = optionalBlueSideCode.get().getBuoyCode();
      }
      else
      {
        blueCode = EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN;
      }

    }
    else
    {
      LOG.info("[Game2020StatusService] detectGroundedBuoys(), NO VALID image from blue side");
      // for (CapturedImageOccurence cio : WatchTowerService.getInstance().getWebCamForPiService()
      // .getImages())
      // {
      // LOG.info("[Game2020StatusService] image from " + cio.getWebcamName() + ", at "
      // + cio.getTimestamp());
      // }
    }

    LOG.info("[Game2020StatusService] detectGroundedBuoys(), blueCode=" + blueCode);
    LOG.info("[Game2020StatusService] detectGroundedBuoys(), yellowCode=" + yellowCode);


    if (blueCode != EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN && yellowCode == EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN)
    {
      this.getCurrentStatus().setBuoysCode(blueCode);
      this.getNextStatus().setBuoysCode(blueCode);
    }
    else if (blueCode == EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN && yellowCode != EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN)
    {
      this.getCurrentStatus().setBuoysCode(yellowCode);
      this.getNextStatus().setBuoysCode(yellowCode);
    }
    else if (blueCode == yellowCode && yellowCode != EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN)
    {
      this.getCurrentStatus().setBuoysCode(yellowCode);
      this.getNextStatus().setBuoysCode(yellowCode);

    }
    else
    {
      this.getCurrentStatus().setBuoysCode(EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN);
      this.getNextStatus().setBuoysCode(EuroBot2020StatusMessage.BUOYS_CODE_UNKNOWN);
    }

    LOG.info("[Game2020StatusService] detectGroundedBuoys(), BuoyCode=" + this.getNextStatus().getBuoysCode());


  }

  public void detectFreeBuoys()
  {

  }


  public boolean issueLedCode(final Color... colors)
  {
    Runnable issueLedCode = () -> {

      // Thread.currentThread().setPriority(MAX_PRIORITY);

      GpioPinPwmOutput r = Game2020StatusService.gpioPinLedRedChannel;
      GpioPinPwmOutput g = Game2020StatusService.gpioPinLedGreenChannel;
      GpioPinPwmOutput b = Game2020StatusService.gpioPinLedBlueChannel;

      // GpioPinDigitalOutput r = Game2020StatusService.gpioPinLedRedChannel;
      // GpioPinDigitalOutput g = Game2020StatusService.gpioPinLedGreenChannel;
      // GpioPinDigitalOutput b = Game2020StatusService.gpioPinLedBlueChannel;


      r.setPwmRange(100);
      g.setPwmRange(100);
      b.setPwmRange(100);

      for (Color c : colors)
      {
        try
        {
          // r.setPwm(0);
          // g.setPwm(0);
          // b.setPwm(0);

          // r.low();
          // g.low();
          // b.low();

          Thread.sleep(100);

          int rval = (c.getRed() * 10000) / 25500;
          int gval = (c.getGreen() * 10000) / 25500;
          int bval = (c.getBlue() * 10000) / 25500;

          LOG.info("[Game2020StatusService] issueLedCode(), c=" + c.toString());

          LOG.info("[Game2020StatusService] issueLedCode(), rval=" + rval + ", gval=" + gval + ", bval=" + bval);

          r.setPwm(rval);
          g.setPwm(gval);
          b.setPwm(bval);

          // r.setState(rval == 0 ? PinState.LOW : PinState.HIGH);
          // g.setState(gval == 0 ? PinState.LOW : PinState.HIGH);
          // b.setState(bval == 0 ? PinState.LOW : PinState.HIGH);

          Thread.sleep(100);

        }
        catch (Exception e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }



      }


    };

    if (ledCodeExecutionPool.getQueue().size() < 2)
    {
      ledCodeExecutionPool.execute(issueLedCode);
    }



    return true;
  }

}
