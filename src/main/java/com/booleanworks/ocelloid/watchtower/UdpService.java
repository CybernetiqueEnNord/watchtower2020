/**
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import com.booleanworks.ocelloid.watchtower.messages.FiducialDetectionOccurence;
import com.booleanworks.ocelloid.watchtower.messages.UdpHelloMessage;
import com.booleanworks.ocelloid.watchtower.messages.UdpMessage;

/**
 * @author vortigern
 *
 */
public class UdpService extends Thread implements BaseService
{

  private CopyOnWriteArrayList<UdpMessage> udpMessageforSending = new CopyOnWriteArrayList<UdpMessage>();

  private CopyOnWriteArrayList<UdpMessage> udpMessagereceived = new CopyOnWriteArrayList<UdpMessage>();

  private ScheduledThreadPoolExecutor executionPool = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());

  /** Semaphore for IPv6 sending. */
  private Semaphore ipv6TransmissionSemaphore = new Semaphore(1);
  /** Semaphore for IPv4 sending. */
  private Semaphore ipv4TransmissionSemaphore = new Semaphore(1);



  public static final int CYCLE_DELAY = 10000;
  public static final int UDP_PORT_IPV6 = 9999;
  public static final int UDP_PORT_IPV4 = 9998;
  public static final String IPV4_BROADCAST = "255.255.255.255";
  public static final String IPV4_MCAST_GROUP = "228.5.6.7";
  public static final String IPV6_MCAST_GROUP = "FF7E:230::1234";



  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(UdpService.class.getName());
  private EnumServiceStatus serviceStatus;

  /** Our ipv6 Socket for broadcast. */
  private MulticastSocket ipv6Socket;

  /** Our ipv4 Socket for broadcast. */
  private MulticastSocket ipv4Socket;



  @Override
  public boolean bootstrap()
  {


    LOG.info("Starting UdpService...");
    if (!this.isAlive())
    {
      this.start();
    }

    return true;
  }


  /**
   * return the serviceStatus.
   *
   * @return the serviceStatus.
   */
  @Override
  public EnumServiceStatus getServiceStatus()
  {

    if (this.serviceStatus == null)
    {
      this.serviceStatus = EnumServiceStatus.UNKNOWN;
    }

    return this.serviceStatus;
  }

  @Override
  public boolean init()
  {
    if (this.getServiceStatus() != EnumServiceStatus.UNKNOWN)
    {
      return false;
    }

    this.serviceStatus = EnumServiceStatus.INITIALIZING;
    this.setName(UdpService.class.getName());

    if (this.ipv6Socket == null)
    {
      try
      {
        this.initSockets();



      }
      catch (SocketException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
        this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
        return false;
      }
      catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
        this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
        return false;
      }

    }

    this.serviceStatus = EnumServiceStatus.READY;
    return true;
  }


  /**
   * @throws IOException
   * @throws SocketException
   * @throws UnknownHostException
   */
  private synchronized void initSockets() throws IOException, SocketException, UnknownHostException
  {

    if (this.ipv6Socket != null && !this.ipv6Socket.isClosed())
    {
      this.ipv6Socket.close();
    }

    if (this.ipv4Socket != null && !this.ipv4Socket.isClosed())
    {
      this.ipv4Socket.close();
    }

    this.ipv6Socket = new MulticastSocket(UDP_PORT_IPV6);
    this.ipv6Socket.setBroadcast(true);
    this.ipv6Socket.setSendBufferSize(63 * 1024);
    this.ipv6Socket.setReceiveBufferSize(63 * 1024);
    this.ipv6Socket.setSoTimeout(300);
    this.ipv6Socket.joinGroup(InetAddress.getByName(IPV6_MCAST_GROUP));

    this.ipv4Socket = new MulticastSocket(UDP_PORT_IPV4);
    this.ipv4Socket.setBroadcast(true);
    this.ipv4Socket.setSendBufferSize(63 * 1024);
    this.ipv4Socket.setReceiveBufferSize(63 * 1024);
    this.ipv4Socket.setSoTimeout(300);
    this.ipv4Socket.joinGroup(InetAddress.getByName(IPV4_MCAST_GROUP));

    this.ipv4TransmissionSemaphore.release();
    this.ipv6TransmissionSemaphore.release();
  }


  @Override
  public void run()
  {

    this.serviceStatus = EnumServiceStatus.RUNNING;

    while (this.getServiceStatus() != EnumServiceStatus.STOPPING && this.getServiceStatus() != EnumServiceStatus.FULL_FAILURE)
    {

      try
      {

        Thread.sleep(CYCLE_DELAY);



        WatchTowerService wts = WatchTowerService.getInstance();

        this.udpMessageforSending.clear();

        this.udpMessageforSending.add(new UdpHelloMessage());

        for (CapturedImageOccurence cimg : wts.getOpenCvCamService().getImages())
        {
          if (!this.udpMessageforSending.contains(cimg))
          {
            if (cimg.getUsageCounter() < 5)
            {
              this.udpMessageforSending.add(cimg);
              cimg.incrementUsageCounter();
            }

          }
        }

        for (CapturedImageOccurence cimg : wts.getOpenCvCamService().getImages())
        {
          if (!this.udpMessageforSending.contains(cimg))
          {
            if (cimg.getUsageCounter() < 5)
            {
              this.udpMessageforSending.add(cimg);
              cimg.incrementUsageCounter();
            }
          }
        }

        for (FiducialDetectionOccurence cfidocc : wts.getArucoDetectionService().getFiducialOccurences())
        {
          if (!this.udpMessageforSending.contains(cfidocc))
          {

            LOG.finer("Fiducial to send: " + cfidocc.toString());
            this.udpMessageforSending.add(cfidocc);
          }

        }


        CopyOnWriteArrayList<UdpMessage> sentMessages = new CopyOnWriteArrayList<UdpMessage>();

        for (UdpMessage msg : this.udpMessageforSending)
        {

          LOG.finer("[UdpService] this.udpMessageforSending.size() = " + this.udpMessageforSending.size());

          this.executionPool.execute(() -> {
            try
            {
              DatagramPacket newIpv6MulticastPacket;
              DatagramPacket newIpv4MulticastPacket;
              DatagramPacket newIpv4BroadcastPacket;

              // First packet with generic broadcast
              // newPacket = msg.encode();
              // newPacket.setAddress(InetAddress.getByName("255.255.255.255"));
              // newPacket.setPort(UDP_PORT_IPV6);
              // this.socket.send(newPacket);

              // IPV4 multicast
              // newPacket = msg.encode();
              // this.socket.joinGroup(InetAddress.getByName(IPV4_MCAST_GROUP));
              // newPacket.setAddress(InetAddress.getByName(IPV4_MCAST_GROUP));
              // newPacket.setPort(UDP_PORT_IPV6);
              // this.socket.send(newPacket);
              // this.socket.leaveGroup(InetAddress.getByName(IPV4_MCAST_GROUP));


              this.ipv6TransmissionSemaphore.tryAcquire(400, TimeUnit.MILLISECONDS);


              // IPV6 multicast
              newIpv6MulticastPacket = msg.encode();
              // this.socket.joinGroup(InetAddress.getByName(IPV6_MCAST_GROUP));
              newIpv6MulticastPacket.setAddress(InetAddress.getByName(IPV6_MCAST_GROUP));
              newIpv6MulticastPacket.setPort(UDP_PORT_IPV6);
              this.ipv6Socket.send(newIpv6MulticastPacket);
              // this.socket.leaveGroup(InetAddress.getByName(IPV6_MCAST_GROUP));


              this.ipv6TransmissionSemaphore.release();

              this.ipv4TransmissionSemaphore.tryAcquire(400, TimeUnit.MILLISECONDS);

              // IPV4 multicast
              newIpv4MulticastPacket = msg.encode();
              // this.socket.joinGroup(InetAddress.getByName(IPV6_MCAST_GROUP));
              newIpv4MulticastPacket.setAddress(InetAddress.getByName(IPV4_MCAST_GROUP));
              newIpv4MulticastPacket.setPort(UDP_PORT_IPV4);
              this.ipv4Socket.send(newIpv4MulticastPacket);
              // this.socket.leaveGroup(InetAddress.getByName(IPV6_MCAST_GROUP));

              // IPV4 multicast
              newIpv4BroadcastPacket = msg.encode();
              // this.socket.joinGroup(InetAddress.getByName(IPV6_MCAST_GROUP));
              newIpv4BroadcastPacket.setAddress(InetAddress.getByName(IPV4_BROADCAST));
              newIpv4BroadcastPacket.setPort(UDP_PORT_IPV4);
              this.ipv4Socket.send(newIpv4BroadcastPacket);
              // this.socket.leaveGroup(InetAddress.getByName(IPV6_MCAST_GROUP));

              this.ipv4TransmissionSemaphore.release();

              sentMessages.add(msg);


            }
            catch (Exception e)
            {
              LOG.warning("[UdpService] Error while sending packets trying to recover... ");
              e.printStackTrace();
              try
              {
                this.initSockets();
              }
              catch (IOException e1)
              {
                LOG.warning("[UdpService] Failed to recover... ");
                e1.printStackTrace();
              }


            }
          });
        }


        this.udpMessageforSending.removeAll(sentMessages);

        this.executionPool.execute(() -> {

          boolean shouldStopReception = false;
          while (!shouldStopReception)
          {
            try
            {
              DatagramPacket newPacket = new DatagramPacket(new byte[1500], 1500);
              this.ipv6Socket.receive(newPacket);
              LOG.finest("Got a packet !");
            }
            catch (SocketTimeoutException e)
            {
              shouldStopReception = true;
            }
            catch (IOException e)
            {
              shouldStopReception = true;

              e.printStackTrace();
            }

          }

        });

        this.executionPool.execute(() -> {

          boolean shouldStopReception = false;
          while (!shouldStopReception)
          {
            try
            {
              DatagramPacket newPacket = new DatagramPacket(new byte[1500], 1500);
              this.ipv4Socket.receive(newPacket);
              LOG.finest("Got a packet !");
            }
            catch (SocketTimeoutException e)
            {
              shouldStopReception = true;
            }
            catch (IOException e)
            {
              shouldStopReception = true;

              e.printStackTrace();
            }

          }

        });


      }
      catch (Exception e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }



    }

  }

  @Override
  public boolean tearDown()
  {
    this.serviceStatus = EnumServiceStatus.STOPPING;

    try
    {
      Thread.sleep(CYCLE_DELAY * 2);
    }
    catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    this.serviceStatus = EnumServiceStatus.STOPPED;

    return true;
  }

  /**
  *
  */
  public UdpService()
  {
    super();
    this.serviceStatus = EnumServiceStatus.UNKNOWN;
  }

}
