/*
 *
 */

package com.booleanworks.ocelloid.watchtower;

public interface BaseService {

  /** Start the service for operations. */
  boolean bootstrap();

  /** Get service status. */
  EnumServiceStatus getServiceStatus();

  /** Init (before bootstrap() */
  boolean init();

  /** Ensure clean tear down before stop. */
  boolean tearDown();

}
