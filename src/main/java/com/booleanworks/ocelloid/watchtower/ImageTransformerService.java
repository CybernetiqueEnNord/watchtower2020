/**
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Logger;

/**
 * @author vortigern
 *
 */
public class ImageTransformerService extends Thread implements BaseService
{



  private ScheduledThreadPoolExecutor executionPool =
      new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());



  public static final int CYCLE_DELAY = 300;



  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(ImageTransformerService.class.getName());
  private EnumServiceStatus serviceStatus;



  @Override
  public boolean bootstrap()
  {


    LOG.info("Starting ImageTransformerService...");
    if (!this.isAlive())
    {
      this.start();
    }

    return true;
  }


  /**
   * return the serviceStatus.
   *
   * @return the serviceStatus.
   */
  @Override
  public EnumServiceStatus getServiceStatus()
  {

    if (this.serviceStatus == null)
    {
      this.serviceStatus = EnumServiceStatus.UNKNOWN;
    }

    return this.serviceStatus;
  }

  @Override
  public boolean init()
  {
    if (this.getServiceStatus() != EnumServiceStatus.UNKNOWN)
    {
      return false;
    }

    this.serviceStatus = EnumServiceStatus.INITIALIZING;
    this.setName(ImageTransformerService.class.getName());

    this.serviceStatus = EnumServiceStatus.READY;
    return true;
  }



  @Override
  public void run()
  {

    this.serviceStatus = EnumServiceStatus.RUNNING;

    while (this.getServiceStatus() != EnumServiceStatus.STOPPING
        && this.getServiceStatus() != EnumServiceStatus.FULL_FAILURE)
    {

      try
      {

        Thread.sleep(CYCLE_DELAY);



      }
      catch (Exception e)
      {
        LOG.warning("[Game2020StatusService] Error while processing data... " + e.getMessage());
        e.printStackTrace();
      }



    }

  }

  @Override
  public boolean tearDown()
  {
    this.serviceStatus = EnumServiceStatus.STOPPING;

    try
    {
      Thread.sleep(CYCLE_DELAY);
    }
    catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    this.serviceStatus = EnumServiceStatus.STOPPED;

    return true;
  }

  /**
  *
  */
  public ImageTransformerService()
  {
    super();
    this.serviceStatus = EnumServiceStatus.UNKNOWN;
  }

}
