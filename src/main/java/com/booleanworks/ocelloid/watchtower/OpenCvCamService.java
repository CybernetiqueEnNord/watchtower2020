/**
*
*/

package com.booleanworks.ocelloid.watchtower;

import java.awt.image.BufferedImage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.javacv.Java2DFrameConverter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;



/**
 * // Read image to Mat as before Mat rgba = ...; Imgproc.cvtColor(rgba, rgba,
 * Imgproc.COLOR_RGB2GRAY, 0);
 *
 * // Create an empty image in matching format BufferedImage gray = new BufferedImage(rgba.width(),
 * rgba.height(), BufferedImage.TYPE_BYTE_GRAY);
 *
 * // Get the BufferedImage's backing array and copy the pixels directly into it byte[] data =
 * ((DataBufferByte) gray.getRaster().getDataBuffer()).getData(); rgba.get(0, 0, data);
 *
 *
 */


/**
 * @author vortigern
 *
 */
public class OpenCvCamService extends Thread implements BaseService
{

  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(OpenCvCamService.class.getName());

  /** Known images. */
  protected final ConcurrentSkipListSet<CapturedImageOccurence> images = new ConcurrentSkipListSet<CapturedImageOccurence>();


  protected final ConcurrentHashMap<String, FrameGrabber> frameGrabbers = new ConcurrentHashMap<String, FrameGrabber>();


  private Java2DFrameConverter converter = new Java2DFrameConverter();

  /** Max stored frames. */
  protected long maxStoredFrames = Cfg.getLong("c1.services.webcampi.maxstoredframe", 50);

  /** Min delay between frames. */
  protected long minimalMsDelayBetweenFrames = Cfg.getLong("c1.services.webcampi.mindelaybetweenframes", 200);

  int frameCounter = 0;

  private Semaphore broadLock = new Semaphore(1);


  /** Status of the server */
  protected EnumServiceStatus serviceStatus = EnumServiceStatus.UNKNOWN;



  /**
   *
   */
  public OpenCvCamService()
  {
    super();
    // TODO Auto-generated constructor stub
  }

  @Override
  public boolean bootstrap()
  {

    LOG.info("Starting OpenCvCamService...");
    if (!this.isAlive())
    {
      this.start();
    }

    Runtime.getRuntime().addShutdownHook(new Thread(() -> {


    }));

    return true;
  }

  /**
   * @return the images
   */
  public ConcurrentSkipListSet<CapturedImageOccurence> getImages()
  {
    return this.images;
  }

  /**
   * @return the kwnownWebcams
   */
  /*
   * public CopyOnWriteArrayList<Webcam> getKwnownWebcams() { return null; }
   */

  /**
   * @return the minimalMsDelayBetweenFrames
   */
  public long getMinimalMsDelayBetweenFrames()
  {
    return this.minimalMsDelayBetweenFrames;
  }

  /**
   * return the serviceStatus.
   *
   * @return the serviceStatus.
   */
  @Override
  public EnumServiceStatus getServiceStatus()
  {

    if (this.serviceStatus == null)
    {
      this.serviceStatus = EnumServiceStatus.UNKNOWN;
    }

    return this.serviceStatus;
  }

  /** Init method. */
  @Override
  public boolean init()
  {

    LOG.info("Init OpenCvCamService...");


    if (this.getServiceStatus() != EnumServiceStatus.UNKNOWN)
    {
      return false;
    }

    this.serviceStatus = EnumServiceStatus.INITIALIZING;

    this.setName(OpenCvCamService.class.getName());

    ImmutableList<String> deviceFileNames = ImmutableList.<String>of("/dev/video0", "/dev/video2", "/dev/video4");

    ImmutableMap<String, Integer> deviceWidth =
        ImmutableMap.<String, Integer>of("/dev/video0", 800, "/dev/video2", 800, "/dev/video4", 800);

    ImmutableMap<String, Integer> deviceHeight =
        ImmutableMap.<String, Integer>of("/dev/video0", 600, "/dev/video2", 600, "/dev/video4", 600);

    deviceFileNames.forEach(devFile -> {

      try
      {
        LOG.info("[OpenCvCamService] creating grabber for " + devFile);
        FrameGrabber fbg = FFmpegFrameGrabber.createDefault(devFile);
        LOG.info("[OpenCvCamService] starting grabber for " + devFile);
        fbg.start();
        fbg.setImageWidth(deviceWidth.get(devFile));
        fbg.setImageHeight(deviceHeight.get(devFile));
        this.frameGrabbers.put(devFile, fbg);
        LOG.info("[OpenCvCamService] grabber added for " + devFile);


      }
      catch (Exception e1)
      {

        LOG.log(Level.SEVERE, "error when grabbing a frame", e1);
      }

    });



    this.serviceStatus = EnumServiceStatus.READY;
    // this.start();

    LOG.info("Init of WebCamService done.");

    return true;

  }

  @Override
  public void run()
  {

    this.serviceStatus = EnumServiceStatus.RUNNING;

    while (this.getServiceStatus() != EnumServiceStatus.STOPPING && this.getServiceStatus() != EnumServiceStatus.FULL_FAILURE)
    {

      try
      {

        LOG.info("[OpenCvCamService] run, checkpoint 010");

        long t1 = System.currentTimeMillis();

        this.minimalMsDelayBetweenFrames = Cfg.getLong("c1.services.opencvcam.mindelaybetweenframes", 50);
        this.maxStoredFrames = Cfg.getLong("c1.services.opencvcam.maxstoredframe", 50);
        this.setPriority(Cfg.getInt("c1.services.opencvcam.priority", 5));


        this.frameGrabbers.forEach((dev, fg) -> {
          try
          {
            LOG.info("[OpenCvCamService] grabbing a frame from " + dev);
            Frame newFrame = fg.grabFrame();
            if (newFrame != null)
            {
              LOG.info("[OpenCvCamService] grabbed a frame from " + dev);
              BufferedImage newImage = this.converter.convert(newFrame);
              CapturedImageOccurence newCap = new CapturedImageOccurence(newImage, System.currentTimeMillis(), dev);
              this.images.add(newCap);
              // WatchTowerService.getInstance().getWebCamForPiService().getImages().add(newCap);
              LOG.info("[OpenCvCamService] new image grabbed from " + dev);

            }
            else
            {
              LOG.info("[OpenCvCamService] grabbed NO frame from " + dev);
            }


          }
          catch (Exception e)
          {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }

        });


        if (this.images.size() > this.maxStoredFrames)
        {

          this.frameGrabbers.forEach((dev, fg) -> {
            this.images.pollFirst();
            LOG.info("[OpenCvCamService] old image removed");
          });

        }



        long t2 = System.currentTimeMillis();


        Thread.sleep(Math.max(20l, this.minimalMsDelayBetweenFrames - t2 + t1));



      }
      catch (InterruptedException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }


    }
  }



  /**
   * @param minimalMsDelayBetweenFrames the minimalMsDelayBetweenFrames to set
   */
  public void setMinimalMsDelayBetweenFrames(long minimalMsDelayBetweenFrames)
  {
    this.minimalMsDelayBetweenFrames = minimalMsDelayBetweenFrames;
  }

  @Override
  public boolean tearDown()
  {


    return true;
  }



}
