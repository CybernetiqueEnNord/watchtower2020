/*
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;
import com.google.common.collect.ImmutableList;

public class Cfg
{

  public static final Logger LOG = Logger.getLogger(Cfg.class.getName());

  public static final ImmutableList<String> CONFIG_LOCATIONS =
      ImmutableList.<String>of("~/ocelloid.properties", "~/ocelloid/ocelloid.properties",
          "/etc/ocelloid.properties", "ocelloid.properties");

  public static final boolean ALLOW_WRITEBACK = true;
  public static boolean needwriteBack = false;


  public static Properties configProperties = null;
  public static File configFile = null;

  public static long lastModifiedTimestamp;
  public static long lastLoadedTimestamp;


  public static boolean getBoolean(String key, boolean defaultValue)
  {
    boolean result = defaultValue;

    String resultString = getString(key, Boolean.toString(defaultValue));

    if (resultString == null || "NULL".equals(resultString))
    {
      LOG.finest("getString(...) returned a bad value !");
      return result;
    }

    LOG.finest("getString(...) returned : " + key + " = " + resultString);

    try
    {
      boolean booleanResult = Boolean.parseBoolean(resultString);
      return booleanResult;

    }
    catch (NumberFormatException e)
    {
      LOG.finest("NumberFormatException : " + e.getMessage());
      e.printStackTrace();
    }
    catch (NullPointerException e)
    {
      LOG.finest("NullPointerException : " + e.getMessage());
      e.printStackTrace();
    }



    return result;
  }

  public static int getInt(String key, int defaultValue)
  {
    int result = defaultValue;

    String resultString = getString(key, Integer.toString(defaultValue));

    if (resultString == null || "NULL".equals(resultString))
    {
      LOG.finest("getString(...) returned a bad value !");
      return result;
    }

    LOG.finest("getString(...) returned : " + key + " = " + resultString);

    try
    {
      int intResult = Integer.parseInt(resultString);
      return intResult;

    }
    catch (NumberFormatException e)
    {
      LOG.finest("NumberFormatException : " + e.getMessage());
      e.printStackTrace();
    }
    catch (NullPointerException e)
    {
      LOG.finest("NullPointerException : " + e.getMessage());
      e.printStackTrace();
    }



    return result;
  }

  public static short getShort(String key, short defaultValue)
  {
    short result = defaultValue;

    String resultString = getString(key, Short.toString(defaultValue));

    if (resultString == null || "NULL".equals(resultString))
    {
      LOG.finest("getString(...) returned a bad value !");
      return result;
    }

    LOG.finest("getString(...) returned : " + key + " = " + resultString);

    try
    {
      short shortResult = Short.parseShort(resultString);
      return shortResult;

    }
    catch (NumberFormatException e)
    {
      LOG.finest("NumberFormatException : " + e.getMessage());
      e.printStackTrace();
    }
    catch (NullPointerException e)
    {
      LOG.finest("NullPointerException : " + e.getMessage());
      e.printStackTrace();
    }



    return result;
  }

  public static byte getByte(String key, byte defaultValue)
  {
    byte result = defaultValue;

    String resultString = getString(key, Byte.toString(defaultValue));

    if (resultString == null || "NULL".equals(resultString))
    {
      LOG.finest("getString(...) returned a bad value !");
      return result;
    }

    LOG.finest("getString(...) returned : " + key + " = " + resultString);

    try
    {
      byte byteResult = Byte.parseByte(resultString, 10);
      return byteResult;

    }
    catch (NumberFormatException e)
    {
      LOG.finest("NumberFormatException : " + e.getMessage());
      e.printStackTrace();
    }
    catch (NullPointerException e)
    {
      LOG.finest("NullPointerException : " + e.getMessage());
      e.printStackTrace();
    }



    return result;
  }


  public static long getLong(String key, long defaultValue)
  {
    long result = defaultValue;

    String resultString = getString(key, Long.toString(defaultValue));

    if (resultString == null || "NULL".equals(resultString))
    {
      LOG.finest("getString(...) returned a bad value !");
      return result;
    }

    LOG.finest("getString(...) returned : " + key + " = " + resultString);

    try
    {
      long longResult = Long.parseLong(resultString);
      return longResult;

    }
    catch (NumberFormatException e)
    {
      LOG.finest("NumberFormatException : " + e.getMessage());
      e.printStackTrace();
    }
    catch (NullPointerException e)
    {
      LOG.finest("NullPointerException : " + e.getMessage());
      e.printStackTrace();
    }



    return result;
  }

  public static double getDouble(String key, double defaultValue)
  {
    double result = defaultValue;

    String resultString = getString(key, Double.toString(defaultValue));

    if (resultString == null || "NULL".equals(resultString))
    {
      LOG.finest("getString(...) returned a bad value !");
      return result;
    }

    LOG.finest("getString(...) returned : " + key + " = " + resultString);

    try
    {
      double doubleResult = Double.parseDouble(resultString);
      return doubleResult;

    }
    catch (NumberFormatException e)
    {
      LOG.finest("NumberFormatException : " + e.getMessage());
      e.printStackTrace();
    }
    catch (NullPointerException e)
    {
      LOG.finest("NullPointerException : " + e.getMessage());
      e.printStackTrace();
    }



    return result;
  }

  public static float getFloat(String key, float defaultValue)
  {
    float result = defaultValue;

    String resultString = getString(key, Float.toString(defaultValue));

    if (resultString == null || "NULL".equals(resultString))
    {
      LOG.finest("getString(...) returned a bad value !");
      return result;
    }

    LOG.finest("getString(...) returned : " + key + " = " + resultString);

    try
    {
      float doubleResult = Float.parseFloat(resultString);
      return doubleResult;

    }
    catch (NumberFormatException e)
    {
      LOG.finest("NumberFormatException : " + e.getMessage());
      e.printStackTrace();
    }
    catch (NullPointerException e)
    {
      LOG.finest("NullPointerException : " + e.getMessage());
      e.printStackTrace();
    }



    return result;
  }


  public static String getString(String key, String defaultValue)
  {
    String result = defaultValue;

    if (key == null || defaultValue == null || key.length() == 0)
    {
      LOG.finest("getString(...) received ugly parameters !");
      return "NULL";
    }

    if (checkConfig() && configProperties != null)
    {
      if (configProperties.containsKey(key))
      {
        String value = (String) configProperties.get(key);
        if (value != null)
        {
          LOG.finest("getString(...) returning: " + value);
          return value;
        }
        else
        {
          LOG.finest("configProperties.get(key) returned NULL !");
        }
      }
      else
      {
        LOG.finest("getString(...) asking write back: " + key + " = " + defaultValue);
        configProperties.put(key, defaultValue);
        needwriteBack = true;
        return defaultValue;
      }

    }


    return result;
  }

  public static boolean checkConfig()
  {


    for (String cFile : CONFIG_LOCATIONS)
    {
      File candidateConfig = new File(cFile);


      if ((configFile == null))
      {
        try
        {
          LOG.finest("Examination of: " + cFile);
          if (candidateConfig.exists() && candidateConfig.isFile() && candidateConfig.canRead())
          {
            LOG.finest("Is a good candidate: " + candidateConfig.getCanonicalPath());

            Properties newConfig = new Properties();
            try (FileReader fr = new FileReader(candidateConfig);)
            {
              LOG.finest("Loading: " + candidateConfig.getCanonicalPath());
              newConfig.load(fr);
              LOG.finest("Loaded: " + candidateConfig.getCanonicalPath());

              configProperties = newConfig;
              configFile = candidateConfig;
              lastModifiedTimestamp = configFile.lastModified();
              lastLoadedTimestamp = System.currentTimeMillis();

              LOG.finest("newConfig.size() = " + newConfig.size());

            }
            catch (IOException e)
            {
              LOG.warning("Error while reeding: " + candidateConfig.getCanonicalPath());
            }

          }
        }
        catch (IOException e)
        {
          LOG.warning("Error while iterating files.");
        }
      }
      else
      {
        LOG.finest("Letting here: " + cFile);
      }

    }



    if (configFile != null && configProperties != null && configFile.exists() && configFile.isFile()
        && configFile.canRead())
    {
      if (configFile.lastModified() > lastModifiedTimestamp
          && (configFile.lastModified() + 2000 < System.currentTimeMillis()))
      {
        try
        {
          LOG.finest("Was modified since last load: " + configFile.getCanonicalPath());

          Properties newConfig = new Properties();
          try (FileReader fr = new FileReader(configFile);)
          {
            LOG.finest("Loading: " + configFile.getCanonicalPath());
            newConfig.load(fr);
            LOG.finest("Loaded: " + configFile.getCanonicalPath());

            configProperties = newConfig;
            lastModifiedTimestamp = configFile.lastModified();
            lastLoadedTimestamp = System.currentTimeMillis();

            LOG.finest("newConfig.size() = " + newConfig.size());

          }
          catch (IOException e)
          {
            LOG.warning("Error while reeding: " + configFile.getCanonicalPath());
          }
        }
        catch (IOException e)
        {
          LOG.warning("Error while looking for reloading...");
        }

      }

    }

    if (needwriteBack && ALLOW_WRITEBACK && configFile != null && configProperties != null
        && configFile.exists() && configFile.isFile() && configFile.canRead()
        && configFile.canWrite() && configFile.lastModified() == lastModifiedTimestamp)
    {

      needwriteBack = false;

      LOG.warning("Proceeding with write back...");
      try (FileWriter fw = new FileWriter(configFile, false);)
      {

        configProperties.store(fw, "Ocelloid writeback at " + System.currentTimeMillis());
        fw.flush();
        fw.close();
        lastModifiedTimestamp = configFile.lastModified();
        lastLoadedTimestamp = System.currentTimeMillis();
        LOG.warning("Configuration updated...");
      }
      catch (IOException e)
      {
        LOG.warning("Error while writing back the configuration...");
      }

    }



    return configFile != null && configProperties != null;

  }

}
