/**
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.util.Arrays;
import java.util.Objects;
import com.booleanworks.ocelloid.watchtower.messages.UdpMessage;

/**
 * @author vortigern
 *
 */
public class CapturedImageOccurence
    implements UdpMessage<CapturedImageOccurence>, Comparable<CapturedImageOccurence>
{


  protected final BufferedImage image;
  protected final long timestamp;
  protected int usageCounter;
  protected final String webcamName;

  /**
   * @param image
   * @param timestamp
   * @param webcamName
   */
  public CapturedImageOccurence(BufferedImage image, long timestamp, String webcamName)
  {
    super();
    this.image = image;
    this.timestamp = timestamp;
    this.webcamName = webcamName;
    this.usageCounter = 0;
  }

  @Override
  public int compareTo(CapturedImageOccurence arg0)
  {

    if (arg0.getTimestamp() != this.getTimestamp())
    {
      return (int) (arg0.getTimestamp() - this.getTimestamp());
    }
    else
    {
      return arg0.getWebcamName().compareTo(this.getWebcamName());
    }

  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
    {
      return true;
    }
    if (obj == null)
    {
      return false;
    }
    if (this.getClass() != obj.getClass())
    {
      return false;
    }
    CapturedImageOccurence other = (CapturedImageOccurence) obj;
    return this.timestamp == other.timestamp && Objects.equals(this.webcamName, other.webcamName);
  }

  /**
   * @return the image
   */
  public BufferedImage getImage()
  {
    return this.image;
  }

  /**
   * @return the timestamp
   */
  public long getTimestamp()
  {
    return this.timestamp;
  }

  /**
   * @return the usageCounter
   */
  public synchronized int getUsageCounter()
  {
    return this.usageCounter;
  }

  /**
   * @return the webcamName
   */
  public String getWebcamName()
  {
    return this.webcamName;
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(this.timestamp, this.webcamName);
  }

  public int incrementUsageCounter()
  {
    this.usageCounter++;
    return this.usageCounter;
  }

  @Override
  public boolean isDecodableAs(DatagramPacket packet)
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public DatagramPacket encode()
  {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);)
    {

      /** Providing a header (16 bits = 2 bytes) */
      oos.writeShort(0x0133);


      /** Providing timestamp (64 bits = 4 bytes) */
      oos.writeLong(this.timestamp);

      /** Providing truncated webcam name (128 bits = 16 bytes) */
      String localWebcamName = this.webcamName == null ? "NULL" : this.webcamName;
      byte[] nameAsBytes = localWebcamName.getBytes("US-ASCII");
      // Trim to 16 or fill with 0.
      byte[] fixedSizedName = Arrays.copyOf(nameAsBytes, 16);
      fixedSizedName[15] = 0; // ENnsure that the last byte equals 0.
      oos.write(fixedSizedName);



      oos.flush();
      byte[] payload = baos.toByteArray();
      DatagramPacket result = new DatagramPacket(payload, payload.length);
      return result;
    }
    catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public CapturedImageOccurence decode(DatagramPacket packet)
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getDocumentation()
  {
    // TODO Auto-generated method stub
    return null;
  }



}
