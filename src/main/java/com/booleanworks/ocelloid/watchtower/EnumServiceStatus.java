/*
 *
 */

package com.booleanworks.ocelloid.watchtower;

public enum EnumServiceStatus {

  /** Full failure. Unable to provide functionalities. */
  FULL_FAILURE,
  /** Initializing. */
  INITIALIZING,
  /** Some components failed. */
  PARTIAL_FAILURE,
  /** Ready for bootstrap. */
  READY,
  /** Running fine. */
  RUNNING,
  /** Starting. */
  STARTING,
  /** Stopped. */
  STOPPED,
  /** Preparing a full stop. */
  STOPPING,
  /** Unknown status. */
  UNKNOWN;

}
