/**
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import com.booleanworks.ocelloid.watchtower.messages.UdpMessage;
import com.pi4j.io.gpio.exception.UnsupportedBoardType;
import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;

/**
 * @author vortigern
 *
 */
public class SerialService extends Thread implements BaseService, SerialDataEventListener
{


  private Serial serial;
  private SerialConfig serialConfig;

  private CopyOnWriteArrayList<UdpMessage> serialMessageforSending =
      new CopyOnWriteArrayList<UdpMessage>();

  private CopyOnWriteArrayList<UdpMessage> serialMessagereceived =
      new CopyOnWriteArrayList<UdpMessage>();

  private ScheduledThreadPoolExecutor executionPool =
      new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());

  /** Semaphore for serial sending. */
  private Semaphore serialTransmissionSemaphore = new Semaphore(1);



  public static final int CYCLE_DELAY = 1000;



  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(SerialService.class.getName());
  private EnumServiceStatus serviceStatus;



  @Override
  public boolean bootstrap()
  {


    LOG.info("Starting SerialService...");
    if (!this.isAlive())
    {
      this.start();
    }

    return true;
  }


  /**
   * return the serviceStatus.
   *
   * @return the serviceStatus.
   */
  @Override
  public EnumServiceStatus getServiceStatus()
  {

    if (this.serviceStatus == null)
    {
      this.serviceStatus = EnumServiceStatus.UNKNOWN;
    }

    return this.serviceStatus;
  }

  @Override
  public boolean init()
  {
    if (this.getServiceStatus() != EnumServiceStatus.UNKNOWN)
    {
      return false;
    }

    this.serviceStatus = EnumServiceStatus.INITIALIZING;
    this.setName(SerialService.class.getName());


    try
    {
      this.serial = SerialFactory.createInstance();
      this.serialConfig = new SerialConfig();
      // Trying this one on pi4
      this.serialConfig.device("/dev/ttyS0").baud(Baud._115200).dataBits(DataBits._8)
          .stopBits(StopBits._1).flowControl(FlowControl.NONE);
      this.serial.open(this.serialConfig);

      LOG.info("[SerialService] init P1 OK.");

      this.serial.addListener(this);

      if (this.serial == null)
      {
        this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
        return false;
      }
    }
    catch (UnsupportedBoardType | IOException e)
    {
      LOG.fine("[SerialService] Error during init()..." + e.getMessage());
      this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
      e.printStackTrace();

      return false;
    }


    this.serviceStatus = EnumServiceStatus.READY;
    return true;
  }



  @Override
  public void run()
  {

    this.serviceStatus = EnumServiceStatus.RUNNING;

    while (this.getServiceStatus() != EnumServiceStatus.STOPPING
        && this.getServiceStatus() != EnumServiceStatus.FULL_FAILURE)
    {

      try
      {

        Thread.sleep(CYCLE_DELAY);



        WatchTowerService wts = WatchTowerService.getInstance();

        this.serialMessageforSending.clear();

        this.serialMessageforSending.add(wts.getGame2020StatusService().getCurrentStatus());

        // for (CapturedImageOccurence cimg : wts.getWebCamService().getImages())
        // {
        // if (!this.serialMessageforSending.contains(cimg))
        // {
        // if (cimg.getUsageCounter() < 5)
        // {
        // this.serialMessageforSending.add(cimg);
        // cimg.incrementUsageCounter();
        // }
        //
        // }
        // }
        //
        // for (CapturedImageOccurence cimg : wts.getWebCamForPiService().getImages())
        // {
        // if (!this.serialMessageforSending.contains(cimg))
        // {
        // if (cimg.getUsageCounter() < 5)
        // {
        // this.serialMessageforSending.add(cimg);
        // cimg.incrementUsageCounter();
        // }
        // }
        // }
        //
        // for (FiducialDetectionOccurence cfidocc : wts.getArucoDetectionService()
        // .getFiducialOccurences())
        // {
        // if (!this.serialMessageforSending.contains(cfidocc))
        // {
        //
        // LOG.finer("Fiducial to send: " + cfidocc.toString());
        // this.serialMessageforSending.add(cfidocc);
        // }
        //
        // }


        CopyOnWriteArrayList<UdpMessage> sentMessages = new CopyOnWriteArrayList<UdpMessage>();

        for (UdpMessage msg : this.serialMessageforSending)
        {

          LOG.fine("[SerialService] this.serialMessageforSending.size() = "
              + this.serialMessageforSending.size());

          this.executionPool.execute(() -> {
            try
            {

              LOG.fine("[SerialService] Sending message...");

              byte[] dataToSend = msg.encode().getData();

              this.serialTransmissionSemaphore.tryAcquire(100, TimeUnit.MILLISECONDS);
              this.serial.write(dataToSend);
              this.serialTransmissionSemaphore.release();

              LOG.fine("[SerialService] sent message (size " + dataToSend.length + " bytes): "
                  + msg.toString());

              String dataAsHex = "";
              for (byte cb : dataToSend)
              {
                dataAsHex += String.format(" %02X", cb);
              }

              LOG.fine("[SerialService] data sent: " + dataAsHex);



            }
            catch (Exception e)
            {
              LOG.warning("[UdpService] Error while sending packets trying to recover... "
                  + e.getMessage());
              // e.printStackTrace();


            }
          });
        }


        this.serialMessageforSending.removeAll(sentMessages);

        this.executionPool.execute(() -> {

          boolean shouldStopReception = false;
          while (!shouldStopReception)
          {
            try
            {
              shouldStopReception = true; // TODO: code pourri à supprimer

              // DatagramPacket newPacket = new DatagramPacket(new byte[1500], 1500);
              // this.ipv6Socket.receive(newPacket);
              // LOG.finest("Got a packet !");
            }
            catch (Exception e)
            {
              shouldStopReception = true;
            }
          }

        });

        this.executionPool.execute(() -> {

          boolean shouldStopReception = false;
          while (!shouldStopReception)
          {
            try
            {
              shouldStopReception = true; // TODO: temporary code
              // DatagramPacket newPacket = new DatagramPacket(new byte[1500], 1500);
              // this.ipv4Socket.receive(newPacket);
              // LOG.finest("Got a packet !");
            }
            catch (Exception e)
            {
              shouldStopReception = true;
            }
          }

        });

        this.setPriority(Cfg.getInt("c1.services.serial.priority", 2));


      }
      catch (Exception e)
      {
        LOG.warning(
            "[UdpService] Error while receiving packets trying to recover... " + e.getMessage());
      }



    }

  }

  @Override
  public boolean tearDown()
  {
    this.serviceStatus = EnumServiceStatus.STOPPING;

    try
    {
      Thread.sleep(CYCLE_DELAY * 2);
    }
    catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    this.serviceStatus = EnumServiceStatus.STOPPED;

    return true;
  }

  /**
  *
  */
  public SerialService()
  {
    super();
    this.serviceStatus = EnumServiceStatus.UNKNOWN;
  }


  @Override
  public void dataReceived(SerialDataEvent event)
  {
    LOG.fine("[SerialService] data received.");

    try
    {
      byte[] receivedData = this.serial.read();


      String dataAsHex = "";
      for (byte cb : receivedData)
      {
        dataAsHex += String.format(" %02X", cb);
      }

      LOG.fine("[SerialService] data received: " + dataAsHex);

      LOG.fine(
          "[SerialService] data received: " + new String(receivedData, StandardCharsets.UTF_8));

    }
    catch (IllegalStateException e)
    {
      LOG.warning("[SerialService] Error while reading incoming data E1");
      e.printStackTrace();
    }
    catch (IOException e)
    {
      LOG.warning("[SerialService] Error while reading incoming data E2");
      e.printStackTrace();
    }

  }

}
