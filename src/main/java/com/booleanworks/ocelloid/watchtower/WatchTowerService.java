/**
 *
 */

package com.booleanworks.ocelloid.watchtower;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;



/**
 * @author m023271
 *
 */
public class WatchTowerService extends Thread implements BaseService
{

  public static final boolean DEBUG2 = Cfg.getBoolean("c1.services.root.debug2", false);
  public static final boolean DEBUG3 = Cfg.getBoolean("c1.services.root.debug3", false);
  public static final boolean DEBUG4 = Cfg.getBoolean("c1.services.root.debug4", false);
  public static final boolean DEBUG5 = Cfg.getBoolean("c1.services.root.debug5", false);

  /** The Constant LOG. */
  private static final Logger LOG = Logger.getLogger(WatchTowerService.class.getName());

  /** The Constant SERVER_VERSION. */
  public static final String SERVER_VERSION = "WatchTowerService-20191001-001";

  /** Singleton. */
  protected static WatchTowerService singleton = null;

  private static final int THREAD_SLEEPTIME = 200;

  /** Standard getter for a singleton instance. */
  public static WatchTowerService getInstance()
  {

    if (WatchTowerService.singleton == null)
    {
      WatchTowerService.singleton = new WatchTowerService();
    }

    return WatchTowerService.singleton;

  }

  static
  {
    // OpenCV.loadShared();
    // Webcam.setDriver(new V4l4jDriverEx()); // this is important
  }

  /**
   * @param args
   */
  public static void main(String[] args)
  {

    WatchTowerService.getInstance().init();
    WatchTowerService.getInstance().bootstrap();

  }



  /** Service in charge of getting webcams and images from it. */
  ArucoDetectionService arucoDetectionService;

  /** Service in charge of REST service. */
  // RestService restService;

  /** Status of the server */
  protected EnumServiceStatus serviceStatus = EnumServiceStatus.UNKNOWN;

  /** Service in charge of webcam management */
  // WebCamService webCamService;
  /** Service in charge of webcam management */
  // WebCamForPiService webCamForPiService;

  /** UDP service. */
  UdpService udpService;

  /** Serial service. */
  SerialService serialService;

  /** Game 2020 service. */
  Game2020StatusService game2020StatusService;

  /** Image transformer service. */
  ImageTransformerService imageTransformerService;

  OpenCvCamService openCvCamService;


  /**
   * Hidden constructor.
   */
  protected WatchTowerService()
  {
    super();
    this.serviceStatus = EnumServiceStatus.UNKNOWN;
  }

  /** Method allowing to start the server. */
  @Override
  public boolean bootstrap()
  {


    LOG.info(
        "[WatchTowerService] bootstrap, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 010");
    try
    {
      // this.getWebCamService().bootstrap();

      LOG.info(
          "[WatchTowerService] bootstrap, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 020");

      this.getOpenCvCamService().bootstrap();
      // this.getWebCamForPiService().bootstrap();

      try
      {
        Thread.sleep(10000);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

      LOG.info(
          "[WatchTowerService] bootstrap, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 030");

      this.getArucoDetectionService().bootstrap();

      try
      {
        Thread.sleep(5000);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

      LOG.info(
          "[WatchTowerService] bootstrap, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 040");

      this.getGame2020StatusService().bootstrap();
    }
    catch (Exception e)
    {
      this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
      e.printStackTrace();
      return false;

    }

    try
    {
      this.getSerialService().bootstrap();

      LOG.info(
          "[WatchTowerService] bootstrap, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 050");
    }
    catch (Exception e)
    {
      this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
      e.printStackTrace();
      return false;

    }


    // try
    // {
    // this.getUdpService().bootstrap();
    // LOG.info("[WatchTowerService] bootstrap, Thread.currentThread().getId()="
    // + Thread.currentThread().getId() + ", checkpoint 060");
    // }
    // catch (Exception e)
    // {
    // this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
    // e.printStackTrace();
    // return false;
    //
    // }

    // try
    // {
    // this.getRestService().bootstrap();
    // }
    // catch (Exception e)
    // {
    // this.serviceStatus = EnumServiceStatus.PARTIAL_FAILURE;
    // e.printStackTrace();
    // return false;
    //
    // }


    LOG.info("Starting WatchTowerService...");
    if (!this.isAlive())
    {
      LOG.info("[WatchTowerService] this.start()");
      this.start();
    }
    else
    {
      LOG.info("[WatchTowerService] this.start() aborted because already alive !!");
    }

    return true;

  }



  /**
   * @return the webCamService
   */
  public ArucoDetectionService getArucoDetectionService()
  {

    if (this.arucoDetectionService == null)
    {
      this.arucoDetectionService = new ArucoDetectionService();
    }

    return this.arucoDetectionService;
  }

  /**
   * @return the webCamService
   */
  // public RestService getRestService()
  // {
  //
  // if (this.restService == null)
  // {
  // this.restService = new RestService();
  // }
  //
  // return this.restService;
  // }

  /**
   * return the serviceStatus.
   *
   * @return the serviceStatus.
   */
  @Override
  public EnumServiceStatus getServiceStatus()
  {

    if (this.serviceStatus == null)
    {
      this.serviceStatus = EnumServiceStatus.UNKNOWN;
    }

    return this.serviceStatus;
  }

  /**
   * @return the webCamService
   */
  /*
   * public WebCamService getWebCamService() {
   *
   * if (this.webCamService == null) { this.webCamService = new WebCamService(); }
   *
   * return this.webCamService; }
   */

  /**
   * @return the webCamService
   */
  /*
   * public WebCamForPiService getWebCamForPiService() {
   *
   * if (this.webCamForPiService == null) { this.webCamForPiService = new WebCamForPiService(); }
   *
   * return this.webCamForPiService; }
   */

  /**
   * @return the webCamService
   */
  public UdpService getUdpService()
  {

    if (this.udpService == null)
    {
      this.udpService = new UdpService();
    }

    return this.udpService;
  }


  /**
   * @return the webCamService
   */
  public SerialService getSerialService()
  {

    if (this.serialService == null)
    {
      this.serialService = new SerialService();
    }

    return this.serialService;
  }

  /**
   * @return the webCamService
   */
  public Game2020StatusService getGame2020StatusService()
  {

    if (this.game2020StatusService == null)
    {
      this.game2020StatusService = new Game2020StatusService();
    }

    return this.game2020StatusService;
  }

  /**
   * @return the webCamService
   */
  public ImageTransformerService getImageTransformerService()
  {

    if (this.imageTransformerService == null)
    {
      this.imageTransformerService = new ImageTransformerService();
    }

    return this.imageTransformerService;
  }


  /**
   * @return the webCamService
   */
  public OpenCvCamService getOpenCvCamService()
  {

    if (this.openCvCamService == null)
    {
      this.openCvCamService = new OpenCvCamService();
    }

    return this.openCvCamService;
  }

  /** Init. */
  @Override
  public boolean init()
  {

    ConsoleHandler consoleHandler = new ConsoleHandler();
    consoleHandler.setLevel(Level.FINE);



    Logger.getGlobal().setLevel(Level.FINE);
    Logger.getGlobal().addHandler(consoleHandler);
    Logger.getAnonymousLogger().addHandler(consoleHandler);
    Logger.getAnonymousLogger().setLevel(Level.FINE);
    Logger.getLogger("").addHandler(consoleHandler);
    Logger.getLogger("").setLevel(Level.FINE);


    try
    {
      FileHandler fileHandler = new FileHandler("watchtower-%u.log", 10 * 1014 * 1024, 10, true);
      fileHandler.setLevel(Level.FINE);
      fileHandler.setFormatter(new SimpleFormatter());
      Logger.getGlobal().addHandler(fileHandler);
      Logger.getAnonymousLogger().addHandler(fileHandler);
      Logger.getLogger("").addHandler(fileHandler);
    }
    catch (SecurityException | IOException e1)
    {
      e1.printStackTrace();
    }

    if (this.getServiceStatus() != EnumServiceStatus.UNKNOWN)
    {
      return false;
    }

    LOG.info("[WatchTowerService] init, Thread.currentThread().getId()=" + Thread.currentThread().getId());

    this.serviceStatus = EnumServiceStatus.INITIALIZING;
    this.setName(WatchTowerService.class.getName());

    try
    {
      // this.getWebCamService().init();

      LOG.info("[WatchTowerService] init, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 060");

      this.getOpenCvCamService().init();
      // this.getWebCamForPiService().init();

      LOG.info("[WatchTowerService] init, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 070");

      this.getArucoDetectionService().init();

      LOG.info("[WatchTowerService] init, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 080");

      this.getGame2020StatusService().init();

      LOG.info("[WatchTowerService] init, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 090");
    }
    catch (Exception e)
    {
      this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
      e.printStackTrace();
      return false;

    }


    try
    {
      this.getSerialService().init();

      LOG.info("[WatchTowerService] init, Thread.currentThread().getId()=" + Thread.currentThread().getId() + ", checkpoint 110");
    }
    catch (Exception e)
    {
      this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
      e.printStackTrace();
      return false;

    }



    // try
    // {
    // this.getUdpService().init();
    // LOG.info("[WatchTowerService] init, Thread.currentThread().getId()="
    // + Thread.currentThread().getId() + ", checkpoint 120");
    // }
    // catch (Exception e)
    // {
    // this.serviceStatus = EnumServiceStatus.FULL_FAILURE;
    // e.printStackTrace();
    // return false;
    //
    // }
    // try
    // {
    // this.getRestService().init();
    // }
    // catch (Exception e)
    // {
    // this.serviceStatus = EnumServiceStatus.PARTIAL_FAILURE;
    // e.printStackTrace();
    // return false;
    //
    // }



    this.serviceStatus = EnumServiceStatus.READY;
    return true;

  }

  @Override
  public void run()
  {

    while (this.getServiceStatus() != EnumServiceStatus.STOPPING && this.getServiceStatus() != EnumServiceStatus.FULL_FAILURE)
    {

      Thread.yield();

      try
      {
        Thread.sleep(THREAD_SLEEPTIME);
      }
      catch (InterruptedException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }



    }

  }

  /**
   * @param serviceStatus the serviceStatus to set.
   */
  protected void setServiceStatus(EnumServiceStatus serverStatus)
  {
    this.serviceStatus = serverStatus;
  }



  @Override
  public boolean tearDown()
  {
    // TODO Auto-generated method stub
    return false;
  }



}
