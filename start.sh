#/bin/bash
javaExec="/usr/bin/java"
echo "javaExec=$javaExec"
ocelloidJar="/home/pi/ocelloid/target/ocelloid-0.0.2-SNAPSHOT.jar"
echo "ocelloidJar=$ocelloidJar"
watchTowerHome="/home/pi/ocelloid"
echo "watchTowerHome=$watchTowerHome"
pidFile="$watchTowerHome/ocelloid_pid"
echo "pidFile=$pidFile"
javaCmd="$javaExec -Xmx2000m -jar $ocelloidJar"
echo "javaCmd=$javaCmd"

umask 002

sleep 3
echo "Starting watchtower2020..."

cd $watchTowerHome
echo "working directory: $PWD"
if [ -f $ocelloidJar ]; then
    echo "$ocelloidJar found."
else
    echo "$ocelloidJar not found. proceeding with update."
    ./killCleanUpdate.sh
fi
chmod -R 775 *

v4l2-ctl -d /dev/video0 --set-priority 3
v4l2-ctl -d /dev/video2 --set-priority 3
v4l2-ctl -d /dev/video4 --set-priority 3


$javaCmd >/dev/null 2>/dev/null &
targetPid=$!
disown -a
echo "targetPid=$targetPid"
touch $pidFile
echo "$targetPid" >$pidFile
echo "Waiting 10s..."
sleep 10
ps -p $targetPid
result1=$?

if [ -f $pidFile ]; then
    echo "PID file exists..."
else
    echo "NO PID file !!!!"
fi

echo "Waiting 10s..."
sleep 10
ps -p $targetPid
result2=$?
if [ $result2 -eq 0 ]; then
    echo "Process still alive. seems OK."
    tail -n 20 watch*log*
    exit 0
else
    echo "He's dead jim."
    exit 1
fi