# Ocelloid / watchtower project




## Raspberry Pi4 setup


### System packages

We will need

*   Some tools for diagnostics: htop, iotop, lshw, dmidecode, tshark, wirshark, fswebcam
*   Some system tools and editors: synaptic, aptitude
*   Remote access tools: vnc4server
*   Java + Maven suite: openjdk-11-jdk, maven, git
*   Some required libraries: libjpeg8
*   Wifi access point kit: hostapd dnsmasq

So, using a terminal: 

	sudo apt update
	sudo apt upgrade
	sudo apt install install htop iotop lshw dmidecode tshark wirewhark synaptic aptitude openjdk-11-jdk maven git kwrite kate hostapd dnsmasq libjpeg8 fswebcam vnc4server scons
	
	


### Activation of required hardware / modules:

With raspi-config tool:
*   in "interfacing", Enable SSH (or add "/boot/ssh" file using touch).
*   in "interfacing", Disable TTY and kernel messages on serial device. 
*   in "interfacing", Enable SPI, I2C.
*   in "interfacing", Disable VNC (buggy and not very useful).
*   in "interfacing", DIsable the native camera support (thru the connector of the pi).


### Build rpi_ws281x for led strip control

Link: https://github.com/jgarff/rpi_ws281x

Setup:

	git clone https://github.com/jgarff/rpi_ws281x.git
	cd rpi_ws281x/
	sudo apt install scons
	scons

Test:

	./test -g 10


### Alternate to sole rpi_ws281x, the python lib

Link: https://github.com/rpi-ws281x/rpi-ws281x-python

Setup:

	sudo pip3 install rpi_ws281x

### Get, deploy and run the software:

Get the software:

	cd
	git clone "https://gitea.extra.booleanworks.com/mlecabellec/ocelloid.git"
	cd ocelloid
	
Build and run:

	killall -9 java ; git reset --hard ; git clean -d -q -f -f ; git pull https://gitlab.com/CybernetiqueEnNord/watchtower2020.git; mvn -P watchtower01 clean  compile package ; sync ; java -Xdebug -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=0.0.0.0:8000 -jar target/ocelloid-0.0.2-SNAPSHOT.jar
	