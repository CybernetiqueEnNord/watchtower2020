# Configuration using ocelloid.properties

## Location of configuration file

As implemented in class com.booleanworks.ocelloid.watchtower.Cfg, the watchtower is looking for configuration file in the following order:

* ~/ocelloid.properties
* ~/ocelloid/ocelloid.properties
* /etc/ocelloid.properties
* ocelloid.properties

At each lookup, the Cfg class will take the first available.

## write back configuration

By default, if a configuration is missing in the configuration file, if Cfg allows it and if the file is writable, the missing item will be added with its default value as specified when calling get**** methods.

## When configuration is read ?

The implementation of Cfg class is straight-forward and not optimal:
* Each time a value is requested, "last modified" attribute of the configration file is checked against last known value and the timestamp of the last load of the file.
* If file was updated, the file is loaded again and a new Properties object replace the old one. A "margin" of 2 seconds is used in order to be sure that the file is not currently edited.

## Meaning of items

### Aruco service

#### c1.services.aruco.cycle=1200

Duration of the Aruco detection loop in ms. At each iteration, a set of images are taken from the "heap of available images" with an "ugly check" ensuring that images from all available camera are taken.

#### c1.services.aruco.detector.pixelpercell=4

Number of pixels for each "cell" of an Aruco tag. An Aruco "4x4" with a pixelpercell value at 2 will give a binary image of 8x8 (simple multiplication. As the detector use a binary image as a template to detect internal patterns in the rectangle of the Aruco tag, the pixelpercell is a mean to obtain multiple measurement points for each cell of the Aruco and reduce errors.

A high value reduce error probabilities but reduce the distance of detection as we will need more pixels to detect it.

A low value will increase the detection distance but will increase the probability of bad detection.

#### c1.services.aruco.detector.stability.disturbance=0.25

When detected, BoofCv performs a stability check by testing if the result is consistent even if a proportion of pixels (here 25%) are outside the limits.


#### c1.services.aruco.detector.startIdx=0

In order to ensure robustness of the detection, multiple detectors are configured with indices from 0 to 5.

startIdx and stopIdx allow to choose used detectors in the 5 available configurations.

As implemented, more the index is high, more the detector is restrictive.

Using all five available detectors will consume a lot of CPU time. 

#### c1.services.aruco.detector.stopIdx=3

In order to ensure robustness of the detection, multiple detectors are configured with indices from 0 to 5.

startIdx and stopIdx allow to choose used detectors in the 5 available configurations.

As implemented, more the index is high, more the detector is restrictive.

Using all five available detectors will consume a lot of CPU time. 


#### c1.services.aruco.onlycentercamera=false

This parameter allow to retain only images from the center camera for Aruco detection. It's a mean to increase performances if detection isn't required on the whole table area.

### Game's status service

#### c1.services.gamestatus.cycle=600

Duration of the "game status" loop in ms. At each iteration:
* The position of the watchtower on the mast may be evaluated.
* The position of the weathervane  may be evaluated.
* The code of grounded buoys may be evaluated.
* The Aruco locations may be refined / analyzed / transmitted.



#### c1.services.gamestatus.debug5.fakegroundedbuoys=1

When c1.services.root.debug5=true, allows to report a fake value for grounded buoys.

#### c1.services.gamestatus.debug5.fakeweathervane=1

When c1.services.root.debug5=true, allows to report a fake value for grounded buoys.


#### c1.services.gamestatus.groundedbuoys.*side.*filter.*

When detecting grounded buoys, the side cameras (yellow side + blue side) apply the following process to detect buoys:
* Get a sub-image of the zone where the buoys are supposed to be.
* Apply a median blur filter on the image.
* Transform the image in a binary (back / white) image by using an algo looking if the Hue/saturation/Brightness of each pixel is included into a given range around a target value.
* Detect ellipses from binary images.
* Filter ellipses with inconsistent shapes.
* Try to guess out something from it (and it's really painful)

So:
* blur is the size in pixels of the median blur filter.
* saturation is the target value of saturation of each filter (green filter + red filter on each yellow/blue sides).
* radius is the tolerance range around the central target value composed by the pair (centerhue,saturation)

A good way to tune these parameter is to set c1.services.root.debug5=true. By the way, it will enable saving of images coming from the camera is the "workdir' of the watchtower (in fact, the root of the project).

Then, by using an image processing tool like Gimp, evaluate the mean Hue/saturation/Brightness values of green or red buoys on each side in order to tune (centerhue,saturation,radius,blur) values. When debug5 is enabled, the watchtower saves also the binary images obtained after the Hue/saturation filters for each side. If parameters are corrects, these images must show white shapes in the place of matching buoys. As Cfg class detects changes in the configuration file at runtime, the tuning can be achieved when the wacthtower is running.




``` 
c1.services.gamestatus.groundedbuoys.blueside.greenfilter.blur=7
c1.services.gamestatus.groundedbuoys.blueside.greenfilter.radius=0.35
c1.services.gamestatus.groundedbuoys.blueside.greenfilter.saturation=0.15
c1.services.gamestatus.groundedbuoys.blueside.redfilter.blur=7
c1.services.gamestatus.groundedbuoys.blueside.redfilter.radius=0.20
c1.services.gamestatus.groundedbuoys.blueside.redfilter.saturation=0.5
```

``` 
c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.blur=7
c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.radius=0.35
c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.saturation=0.15
c1.services.gamestatus.groundedbuoys.yellowside.redfilter.blur=7
c1.services.gamestatus.groundedbuoys.yellowside.redfilter.radius=0.20
c1.services.gamestatus.groundedbuoys.yellowside.redfilter.saturation=0.43
```


``` 
c1.services.gamestatus.groundedbuoys.greenfilter.centerhue=0.29
c1.services.gamestatus.groundedbuoys.redfilter.centerhue=0.97
```

#### c1.services.gamestatus.groundedbuoys.*side.window.*

These values are used during the grounded buoys detection process in order to get a sub-image to process. (as explained before).

The c1.services.root.debug5 parameter can the settled at true in order to obtain sub images at runtime for tuning.

When tuning these parameters, ensure that the width of images let enought margin to see all buoys in both watchtower positions (yellow side / blue side).

``` 
c1.services.gamestatus.groundedbuoys.blueside.window.height=120
c1.services.gamestatus.groundedbuoys.blueside.window.width=560
c1.services.gamestatus.groundedbuoys.blueside.window.x=75
c1.services.gamestatus.groundedbuoys.blueside.window.y=570
```

``` 
c1.services.gamestatus.groundedbuoys.yellowside.window.height=120
c1.services.gamestatus.groundedbuoys.yellowside.window.width=560
c1.services.gamestatus.groundedbuoys.yellowside.window.x=56
c1.services.gamestatus.groundedbuoys.yellowside.window.y=570
```


####c1.services.gamestatus.groundedbuoys.common.maxellipseelong=3.5

This parameter is a max ratio of major/minor semi-axis of ellipses used during grounded buoys detection process for removal of inconsistent ellipses.


#### c1.services.gamestatus.groundedbuoys.maxbuoyapparentsize=39.0

Used during the grounded buoys detection process as a max apparent size for a single buoy. (filtering based on the major semi-axis of ellipses).

#### c1.services.gamestatus.groundedbuoys.minbuoyapparentsize=18.7

Used during the grounded buoys detection process as a min apparent size for a single buoy. (filtering based on the major semi-axis of ellipses).

#### c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.*

These parameters are used when multiple same-colored buoys on a given rack are seen as a single long ellipse.

These parameters are threshold helping to decide, according to the major/minor semi-axis ratio of an ellipse, if it's covering one, two or three buoys.

Honnestly, it's not a reliable method....

``` 
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.far=1.53
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.near=1.53
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.neutral=1.53
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.far=2.05
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.near=2.05
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.neutral=2.05
```

#### c1.services.gamestatus.grounedbuoys.cyclemodulo=13

As grounded buoys detection can be time consuming, this parameter allows to do it only on one cycle took in n cycles (using modulo operator). Keep in mind that grounded buoys detection can take 8500 ms on a cycle who should stay below 900 ms.

#### c1.services.gamestatus.locaterobots.cyclemodulo=5

this parameter allows to do robot location only on one cycle took in n cycles (using modulo operator). 

#### c1.services.gamestatus.weathervane.blueside.detectionmargin=1.8

The weathervane orientation detection is implemented by taking a sub-image showing only the weathervane and by computing a mean value of the pixels in the image. The min/max/mean values of these computation are kepts at short and long terms.

The detection of the weathervane is achieved by comparing the long term mean with the short term mean assuming the fact that, if the weathervane was moving earlier, the mean value of the stabilized weathervane will be brighter or darker than the long term mean.

But, in order to avoid false detection, we need to have a "margin" in order to make the detection reliable.


### c1.services.gamestatus.weathervane.blueside.histosize=200

In the weathervane detection process, this parameter sets the number of long term means that are kept.

#### c1.services.gamestatus.weathervane.blueside.recentduration=20000

In the weathervane detection process, this parameter sets the number of ms that are taken from the history in order to compute the "short term mean".


#### c1.services.gamestatus.weathervane.*side.window.*


These parameters are used to set configure the part of the images used for weathervane detection.


``` 
c1.services.gamestatus.weathervane.blueside.window.height=180
c1.services.gamestatus.weathervane.blueside.window.width=470
c1.services.gamestatus.weathervane.blueside.window.x=0
c1.services.gamestatus.weathervane.blueside.window.y=480
```

``` 
c1.services.gamestatus.weathervane.yellowside.window.height=180
c1.services.gamestatus.weathervane.yellowside.window.width=470
c1.services.gamestatus.weathervane.yellowside.window.x=540
c1.services.gamestatus.weathervane.yellowside.window.y=480
```



#### c1.services.gamestatus.weathervane.yellowside.detectionmargin=1.8

The weathervane orientation detection is implemented by taking a sub-image showing only the weathervane and by computing a mean value of the pixels in the image. The min/max/mean values of these computation are kepts at short and long terms.

The detection of the weathervane is achieved by comparing the long term mean with the short term mean assuming the fact that, if the weathervane was moving earlier, the mean value of the stabilized weathervane will be brighter or darker than the long term mean.

But, in order to avoid false detection, we need to have a "margin" in order to make the detection reliable.




#### c1.services.gamestatus.weathervane.yellowside.histosize=200

In the weathervane detection process, this parameter sets the number of long term means that are kept.

#### c1.services.gamestatus.weathervane.yellowside.recentduration=20000

n the weathervane detection process, this parameter sets the number of ms that are taken from the history in order to compute the "short term mean".

#### cc1.services.gamestatus.groundedbuoys.*side.disableellipsefilter

These are debugging parameters used to disable "ellipse filter" in the grounded buoys detection process. They are only useful with c1.services.root.debug5=true.

``` 
cc1.services.gamestatus.groundedbuoys.blueside.disableellipsefilter=false
cc1.services.gamestatus.groundedbuoys.yellowside.disableellipsefilter=false
```



### debug

#### c1.services.root.debug*

These parameters control if debug modes are activated. Up to now, only debug5 is really used and allow:
* To save images took from various processes of the watchtower.
* To issue fake grounded buoys codes and fake weathervane positions. 

Keep in mind that saving images from processes take a huge amount of CPU time and can output gigabytes of image in space of few minutes.

``` 
c1.services.root.debug2=false
c1.services.root.debug3=false
c1.services.root.debug4=false
c1.services.root.debug5=true
```

### The Pi webcam service.

#### c1.services.webcampi.maxstoredframe=50

The "Pi webcam" service is in charge of providing and storing images from camera. This parameter controls the amount of images that are kept in memory.

#### c1.services.webcampi.mindelaybetweenframes=200

The "Pi webcam" service is in charge of providing and storing images from camera. This parameter controls the minimal delay in ms between two images from a given webam.



## Sample configuration #1

This sample configuration was used the 17th of June for test.


``` 
#Ocelloid writeback at 1592324462928
#Tue Jun 16 17:21:02 BST 2020
c1.services.aruco.cycle=1200
c1.services.aruco.detector.pixelpercell=4
c1.services.aruco.detector.stability.disturbance=0.25
c1.services.aruco.detector.startIdx=0
c1.services.aruco.detector.stopIdx=3
c1.services.aruco.onlycentercamera=false
c1.services.gamestatus.cycle=600
c1.services.gamestatus.debug5.fakegroundedbuoys=1
c1.services.gamestatus.debug5.fakeweathervane=1
c1.services.gamestatus.groundedbuoys.blueside.greenfilter.blur=7
c1.services.gamestatus.groundedbuoys.blueside.greenfilter.radius=0.35
c1.services.gamestatus.groundedbuoys.blueside.greenfilter.saturation=0.15
c1.services.gamestatus.groundedbuoys.blueside.redfilter.blur=7
c1.services.gamestatus.groundedbuoys.blueside.redfilter.radius=0.20
c1.services.gamestatus.groundedbuoys.blueside.redfilter.saturation=0.5
c1.services.gamestatus.groundedbuoys.blueside.window.height=120
c1.services.gamestatus.groundedbuoys.blueside.window.width=560
c1.services.gamestatus.groundedbuoys.blueside.window.x=75
c1.services.gamestatus.groundedbuoys.blueside.window.y=570
c1.services.gamestatus.groundedbuoys.common.maxellipseelong=3.5
c1.services.gamestatus.groundedbuoys.greenfilter.centerhue=0.29
c1.services.gamestatus.groundedbuoys.maxbuoyapparentsize=39.0
c1.services.gamestatus.groundedbuoys.minbuoyapparentsize=18.7
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.far=1.53
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.near=1.53
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.double.neutral=1.53
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.far=2.05
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.near=2.05
c1.services.gamestatus.groundedbuoys.multiplebuoys.threshold.triple.neutral=2.05
c1.services.gamestatus.groundedbuoys.redfilter.centerhue=0.97
c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.blur=7
c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.radius=0.35
c1.services.gamestatus.groundedbuoys.yellowside.greenfilter.saturation=0.15
c1.services.gamestatus.groundedbuoys.yellowside.redfilter.blur=7
c1.services.gamestatus.groundedbuoys.yellowside.redfilter.radius=0.20
c1.services.gamestatus.groundedbuoys.yellowside.redfilter.saturation=0.43
c1.services.gamestatus.groundedbuoys.yellowside.window.height=120
c1.services.gamestatus.groundedbuoys.yellowside.window.width=560
c1.services.gamestatus.groundedbuoys.yellowside.window.x=56
c1.services.gamestatus.groundedbuoys.yellowside.window.y=570
c1.services.gamestatus.grounedbuoys.cyclemodulo=13
c1.services.gamestatus.locaterobots.cyclemodulo=5
c1.services.gamestatus.weathervane.blueside.detectionmargin=1.8
c1.services.gamestatus.weathervane.blueside.histosize=200
c1.services.gamestatus.weathervane.blueside.recentduration=20000
c1.services.gamestatus.weathervane.blueside.window.height=180
c1.services.gamestatus.weathervane.blueside.window.width=470
c1.services.gamestatus.weathervane.blueside.window.x=0
c1.services.gamestatus.weathervane.blueside.window.y=480
c1.services.gamestatus.weathervane.yellowside.detectionmargin=1.8
c1.services.gamestatus.weathervane.yellowside.histosize=200
c1.services.gamestatus.weathervane.yellowside.recentduration=20000
c1.services.gamestatus.weathervane.yellowside.window.height=180
c1.services.gamestatus.weathervane.yellowside.window.width=470
c1.services.gamestatus.weathervane.yellowside.window.x=540
c1.services.gamestatus.weathervane.yellowside.window.y=480
c1.services.root.debug2=false
c1.services.root.debug3=false
c1.services.root.debug4=false
c1.services.root.debug5=true
c1.services.webcampi.maxstoredframe=50
c1.services.webcampi.mindelaybetweenframes=200
cc1.services.gamestatus.groundedbuoys.blueside.disableellipsefilter=false
cc1.services.gamestatus.groundedbuoys.yellowside.disableellipsefilter=false
#Ocelloid writeback at 1592324462928
#Tue Jun 16 17:21:02 BST 2020


```

