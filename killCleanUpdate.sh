#/bin/bash
javaExec="/usr/bin/java"
ocelloidJar="/home/pi/ocelloid/target/ocelloid-0.0.2-SNAPSHOT.jar"
watchTowerHome="/home/pi/ocelloid"
pidFile="$watchTowerHome/ocelloid.pid"
javaCmd="$javaExec -Xmx2000m -jar $ocelloidJar"

cd $watchTowerHome

echo "Step 1: Killing all java processes."
killall -9 java
result1=$?

echo "Step 2: Making a hard git reset."
git reset --hard
result2=$?

echo "Step 3: Cleaning foreign (non git) files."
git clean -d -q -f -f
result3=$?

echo "Step 4: Pulling new version from git"
git pull https://gitlab.com/CybernetiqueEnNord/watchtower2020.git
result4=$?

echo "Step 5: Making a new version."
mvn -P watchtower01 clean  compile package
result5=$?

echo "Step 6: Putting refconf as main conf."
cp ocelloid.properties.refconf ocelloid.properties

sync

