#/bin/bash
javaExec="/usr/bin/java"
echo "javaExec=$javaExec"
ocelloidJar="/home/pi/ocelloid/target/ocelloid-0.0.2-SNAPSHOT.jar"
echo "ocelloidJar=$ocelloidJar"
watchTowerHome="/home/pi/ocelloid"
echo "watchTowerHome=$watchTowerHome"
pidFile="$watchTowerHome/ocelloid_pid"
echo "pidFile=$pidFile"
javaCmd="$javaExec -Xmx2000m -jar $ocelloidJar"
echo "javaCmd=$javaCmd"

if [ -f pidFile ]; then
    echo "$pidFile exist."
    targetPid=$(cat $pidFile)
    echo "Target PID = $targetPid"
    ps -p $targetPid
    result1=$?
    if [ $result1 -eq 0 ]; then
    	echo "Matching process found. Killing it."
    	kill $targetPid
    	result2=$?
    	if [ $result2 -eq 0 ]; then
    	    echo "Process killed properly."
    	    exit 0
    	else
    	    echo "Failed to kill the process."
    	    exit 1
    	fi
    else
        echo "No matching process found."
        exit 1
    fi
else 
    echo "$pidFile does not exist"
    echo "Killing all java processes in a bloody rage !!!!"
    killall -r .*java.*
    result10=$?
        if [ $result10 -ne 0 ]; then
            echo "No process killed"
            exit 1
        else
            echo "At least one process killed."
            exit 0
        fi
    exit 1
fi